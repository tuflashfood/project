# Project Tu Flash Food 

* Front End Android 
* Language Java , Xml 
* Library Retrofit
* API Firebase Cloud Message

## Install 

### Clone 
* Clone this repo to your local machine using https://gitlab.com/tuflashfood/project.git

### Setup

   #### 1. Code
   * Open file in Android Studio

   #### 2. Firebase Cloud Message
   * **Step 1** register firebase in android studio
        * link <a href="https://medium.com/firebasethailand/%E0%B8%A3%E0%B8%B9%E0%B9%89%E0%B8%88%E0%B8%B1%E0%B8%81-firebase-%E0%B9%81%E0%B8%A5%E0%B8%B0%E0%B8%A7%E0%B8%B4%E0%B8%98%E0%B8%B5%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B9%80%E0%B8%9E%E0%B8%B4%E0%B9%88%E0%B8%A1%E0%B8%A1%E0%B8%B1%E0%B8%99%E0%B9%80%E0%B8%82%E0%B9%89%E0%B8%B2%E0%B9%84%E0%B8%9B%E0%B9%83%E0%B8%99-android-project-e51e38bccbfa" target="_blank">`register firebase in android studio`</a>.
   * **Step 2** how to use firebase cloud message 
        * link <a href="https://medium.com/firebasethailand/%E0%B8%A3%E0%B8%B9%E0%B9%89%E0%B8%88%E0%B8%B1%E0%B8%81-firebase-cloud-messaging-fcm-%E0%B8%95%E0%B8%B1%E0%B9%89%E0%B8%87%E0%B9%81%E0%B8%95%E0%B9%88-zero-%E0%B8%88%E0%B8%99%E0%B9%80%E0%B8%9B%E0%B9%87%E0%B8%99-hero-fb7900af92cd" target="_blank">`how to use firebase cloud message`</a>.

   
## Directory Tree

```
├── app
│   ├── app.iml
│   ├── build.gradle
│   ├── google-services.json
│   ├── proguard-rules.pro
│   └── src
│       └── main
│           ├── AndroidManifest.xml
│           ├── java
│           │   └── com
│           │       └── example
│           │           └── project
│           │               ├── adapter
│           │               │   ├── MainStatusOrderAdapter.java
│           │               │   ├── MenuRestaurantAdapter.java
│           │               │   ├── OrderRestaurantMainAdapter.java
│           │               │   ├── SelectRestaurantAdapter.java
│           │               │   ├── SenderMainAdapter.java
│           │               │   └── ShoppingBasketAdapter.java
│           │               ├── interfaces
│           │               │   ├── Api.java
│           │               │   ├── Callbacklogin.java
│           │               │   └── TalkMainActivity.java
│           │               ├── LoginActivity.java
│           │               ├── MainActivity.java
│           │               ├── model
│           │               │   ├── DetailOrderRepository.java
│           │               │   ├── json
│           │               │   │   ├── FoodOrder.java
│           │               │   │   ├── Location.java
│           │               │   │   ├── Menu.java
│           │               │   │   ├── Menus.java
│           │               │   │   ├── OptionItem.java
│           │               │   │   ├── OrderBuy.java
│           │               │   │   ├── Order.java
│           │               │   │   ├── OrderStatus.java
│           │               │   │   ├── PurchaseOrder.java
│           │               │   │   ├── ResponseBody.java
│           │               │   │   ├── Restaurant.java
│           │               │   │   └── User.java
│           │               │   ├── MainOrderRestaurantRepository.java
│           │               │   ├── MainUserOrderStatusRepository.java
│           │               │   ├── NotificationHelper.java
│           │               │   ├── RestaurantRepository.java
│           │               │   ├── SelectRestaurantRepository.java
│           │               │   ├── SenderOrderResponse.java
│           │               │   └── ShoppingBasketRepository.java
│           │               ├── RestaurantActivity.java
│           │               ├── SenderActivity.java
│           │               ├── service
│           │               │   ├── AlarmReceiver.java
│           │               │   ├── HttpManager.java
│           │               │   ├── LocationService.java
│           │               │   └── MyFirebaseMessagingService.java
│           │               └── ui
│           │                   ├── basket
│           │                   │   ├── ShoppingBasketFragment.java
│           │                   │   └── ShoppingBasketViewModel.java
│           │                   ├── detail_order
│           │                   │   ├── DetailOrderFragment.java
│           │                   │   └── DetailOrderViewModel.java
│           │                   ├── login
│           │                   │   ├── LoginFragment.java
│           │                   │   └── LoginViewModel.java
│           │                   ├── main_order_restaurant
│           │                   │   ├── MainOrderRestaurantFragment.java
│           │                   │   └── MainOrderRestaurantViewModel.java
│           │                   ├── main_restaurant
│           │                   │   ├── MainRestaurantFragment.java
│           │                   │   ├── MainRestaurantViewModel.java
│           │                   │   └── SectionsPagerResAdapter.java
│           │                   ├── main_statusorder
│           │                   │   ├── MainUserStatusOrderFragment.java
│           │                   │   └── MainUserStatusOrderViewModel.java
│           │                   ├── menu_retaurant
│           │                   │   ├── MenuRestaurantFragment.java
│           │                   │   ├── MenuRestaurantViewModel.java
│           │                   │   └── SectionsPagerAdapter.java
│           │                   ├── option_menu
│           │                   │   ├── OptionMenuFragment.java
│           │                   │   └── OptionMenuViewModel.java
│           │                   ├── register
│           │                   │   ├── RegisterFragment.java
│           │                   │   └── RegisterViewModel.java
│           │                   ├── restaurant
│           │                   │   ├── RestaurantFragment.java
│           │                   │   └── RestaurantViewModel.java
│           │                   ├── select_restaurant
│           │                   │   ├── SelectRestaurantFragment.java
│           │                   │   └── SelectRestaurantViewModel.java
│           │                   ├── sender
│           │                   │   ├── SenderFragment.java
│           │                   │   └── SenderViewModel.java
│           │                   └── statusorder
│           │                       ├── UserStatusOrderFragment.java
│           │                       └── UserStatusOrderViewModel.java
│           └── res
│               ├── drawable-v24
│               │   └── ic_launcher_foreground.xml
│               ├── font
│               │   └── layijimahaniyom.ttf
│               ├── layout
│               │   ├── activity_login.xml
│               │   ├── activity_main.xml
│               │   ├── app_bar_main.xml
│               │   ├── cardview_main_restaurant.xml
│               │   ├── cardview_main_sender.xml
│               │   ├── cardview_menu_restuarant_fragment.xml
│               │   ├── cardview_select_restaurant_fragment.xml
│               │   ├── content_main.xml
│               │   ├── detail_order_fragment.xml
│               │   ├── login_fragments.xml
│               │   ├── main_order_restaurant_fragment.xml
│               │   ├── main_restaurant_fragment.xml
│               │   ├── main_user_status_order_fragment.xml
│               │   ├── nav_header_main.xml
│               │   ├── option_menu_fragment.xml
│               │   ├── recycler_menu_restaurant_fragment.xml
│               │   ├── recycler_select_restaurant_fragment.xml
│               │   ├── register_fragment.xml
│               │   ├── restaurant_fragment.xml
│               │   ├── select_time.xml
│               │   ├── sender_fragment.xml
│               │   ├── shoppingbasket_fragment.xml
│               │   ├── shoppingbasket_menu_fragment.xml
│               │   └── user_status_order_fragment.xml
│               ├── menu
│               │   ├── activity_main_drawer_restaurant.xml
│               │   ├── activity_main_drawer_sender.xml
│               │   ├── activity_main_drawer_user.xml
│               │   └── main.xml
│               ├── mipmap-anydpi-v26
│               │   ├── ic_launcher_round.xml
│               │   ├── ic_launcher.xml
│               │   └── logotu.png
│               ├── mipmap-hdpi
│               │   ├── ic_launcher.png
│               │   └── ic_launcher_round.png
│               ├── mipmap-mdpi
│               │   ├── ic_launcher.png
│               │   └── ic_launcher_round.png
│               ├── mipmap-xhdpi
│               │   ├── ic_launcher.png
│               │   └── ic_launcher_round.png
│               ├── mipmap-xxhdpi
│               │   ├── ic_launcher.png
│               │   └── ic_launcher_round.png
│               ├── mipmap-xxxhdpi
│               │   ├── ic_launcher.png
│               │   └── ic_launcher_round.png
│               ├── navigation
│               │   ├── restaurant_navigation.xml
│               │   ├── sender_navigation.xml
│               │   └── user_navigation.xml
│               ├── values
│               │   ├── colors.xml
│               │   ├── dimen.xml
│               │   ├── strings.xml
│               │   └── styles.xml
│               ├── values-v21
│               │   └── styles.xml
│               └── xml
│                   └── network_security_config.xml
├── build.gradle
├── gradle
│   └── wrapper
│       ├── gradle-wrapper.jar
│       └── gradle-wrapper.properties
├── gradle.properties
├── gradlew
├── gradlew.bat
├── local.properties
├── project.iml
├── README.md
└── settings.gradle
```

