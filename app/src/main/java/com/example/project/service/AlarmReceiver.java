package com.example.project.service;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.project.MainActivity;
import com.example.project.R;
import com.example.project.model.json.ResponseBody;
import com.example.project.ui.login.LoginFragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlarmReceiver extends BroadcastReceiver {

  private static final String TAG = "AlarmReceiver";

  @Override
  public void onReceive(Context context, Intent intent) {
    int requestID = (int) System.currentTimeMillis();
    Intent intentOnReceive = new Intent(context, LoginFragment.class) ;
    String title = intent.getStringExtra("title");
    String contentText = intent.getStringExtra("contentText");
    String order_id = intent.getStringExtra("order_id");
    sendSearchSender(order_id);
    intentOnReceive.putExtra("action",order_id) ;
    intentOnReceive.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    PendingIntent pendingIntent =
        PendingIntent.getActivity(context, requestID, intentOnReceive, PendingIntent.FLAG_UPDATE_CURRENT );
    NotificationCompat.Builder builder = new NotificationCompat.Builder(context,"Receiver")
        .setSmallIcon(R.drawable.logotu)
        .setContentTitle(title)
        .setContentText(contentText)
        .setContentIntent(pendingIntent)
        .setAutoCancel(true);
    NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context) ;
    notificationManagerCompat.notify(200,builder.build());
  }

  public void sendSearchSender(String order_id) {
    Call<ResponseBody> responseBodySearchSender ;
    responseBodySearchSender = HttpManager.getInstance().getService().sendSearchSender(order_id);
    responseBodySearchSender.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if(response.isSuccessful()){
          Log.i(TAG, "onResponse: " + response.body());
        }else {
          Log.i(TAG, "onResponese: " + response.errorBody().toString());
        }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.i(TAG, "onFailure: " + t.toString());
      }
    });
  }
}
