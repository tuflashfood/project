package com.example.project.service;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static okhttp3.internal.Internal.instance;

public class LocationService implements LocationListener {
  private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 1; // in Meters
  private static final long MINIMUM_TIME_BETWEEN_UPDATES = 1000; // in Milliseconds
  private static final String TAG = "LocationService";
  private LocationManager locationManager;
  private static LocationService instance = null;
  private Location location;
  private Context context;
  private double longitude;
  private double latitude;
  private boolean isGPSEnabled;
  private boolean isNetworkEnabled;

  public static LocationService getLocationManager(Context context) {
    if (instance == null) {
      instance = new LocationService(context);
    }
    return instance;
  }

  private LocationService(Context context) {
    this.context = context;
    this.longitude = 0.0;
    this.latitude = 0.0;
    this.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    this.isGPSEnabled = false;
    this.isNetworkEnabled = false;
    initLocationService();
  }

  private void initLocationService() {
    if (!checkPermission()) {
      //check permission
      Toast.makeText(context,"please allow permission GPS",Toast.LENGTH_LONG).show();
      return;
    }
    this.isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    this.isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    if (!isNetworkEnabled && !isGPSEnabled) {
      // cannot get location
      Toast.makeText(context,"cannot open isGPSEnabled or isNetworkEnabled",Toast.LENGTH_LONG).show();
      return;
    } else {
      onStartLocation();
    }
  }

  @Override
  public void onLocationChanged(Location location) {
    if (location == null) return;
    this.location = location;
    this.longitude = location.getLongitude();
    this.latitude = location.getLatitude();
  }

  @Override
  public void onStatusChanged(String provider, int status, Bundle extras) {

  }

  @Override
  public void onProviderEnabled(String provider) {

  }

  @Override
  public void onProviderDisabled(String provider) {

  }

  private boolean checkPermission() {
    if (Build.VERSION.SDK_INT >= 23 &&
        ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
        ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
      //check permission
      return false;
    }
    return true;
  }

  public void onStartLocation() {
    if (checkPermission()) {
    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MINIMUM_TIME_BETWEEN_UPDATES,
        MINIMUM_DISTANCE_CHANGE_FOR_UPDATES, this);
      location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
      if (location != null) {
        Log.i(TAG, "onStartLocation: " + location);
        this.longitude = location.getLongitude();
        this.latitude = location.getLatitude();
      }
    }
  }

  public void onStopLocation() {
    if (locationManager != null) {
      if (Build.VERSION.SDK_INT >= 23 &&
          ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
          ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        locationManager.removeUpdates(this);
      }
      locationManager.removeUpdates(this);
    }
  }

  public Location getLocation() {
    return location;
  }

  public double getLatitude() {
    return latitude;
  }

  public double getLongitude() {
    return longitude;
  }
}
