package com.example.project.service;

import android.util.Log;

import com.example.project.interfaces.Api;

import java.net.SocketException;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpManager {
  private static HttpManager instances;
  private static final String TAG = "HttpManager" ;

  public static HttpManager getInstance() {
    if (instances == null)
      instances = new HttpManager();
    return instances;
  }
  private Api service;

  private HttpManager() {
//            .baseUrl("http://13.229.73.236:4000/")
//    192.168.1.49 moserper
//    192.168.2.151 bond
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl("http://192.168.1.108:4000/")
        .addConverterFactory(GsonConverterFactory.create())
        .build();
    service = retrofit.create(Api.class);
  }

  public Api getService() {
    return service;
  }
}
