package com.example.project.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.Navigation;

import com.example.project.LoginActivity;
import com.example.project.R;
import com.example.project.model.json.ResponseBody;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
  private static final String TAG = "MyFirebaseMessagingService";
  private static int count = 0;

  @Override
  public void onNewToken(@NonNull String s) {
    super.onNewToken(s);
    Log.i(TAG, "onNewToken: " + s);
    //new token send token to server
  }

  private void sendRegistrationToServer(String token) {
    // TODO: Implement this method to send token to your app server.

  }

  @Override
  public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
    super.onMessageReceived(remoteMessage);
    Log.i(TAG, "From: " + remoteMessage.getFrom());
    Log.i(TAG, "From: " + remoteMessage);
    if (remoteMessage.getNotification() != null) {
      // notification title , body
      RemoteMessage.Notification notification = remoteMessage.getNotification();
      // data
      Map<String, String> data = remoteMessage.getData();
      sendNotification(notification, data);
    }
  }

//  private PendingIntent setIntent(String data,Class aClass){
//    Intent resultIntent = new Intent(this, aClass);
//    resultIntent.putExtra("MESSGE","eiei");
//// Create the TaskStackBuilder and add the intent, which inflates the back stack
//    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//    stackBuilder.addNextIntentWithParentStack(resultIntent);
//    return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//  }

  private void sendNotification(RemoteMessage.Notification notification, Map<String, String> data) {
    for (String key : data.keySet()) {
      Log.i(TAG, "sendNotification: " + data.get(key));
    }
    Intent intent = new Intent(this, LoginActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    if(data.containsKey("data")){
      intent.setAction("messageFormFCM");
      intent.putExtra("dataFromForeGround", data.get("data"));
      if (data.get("phone") != null) {
        intent.putExtra("dataFromForeGroundPhone", data.get("phone"));
      }
      if (data.get("time") != null) {
        intent.putExtra("dataFromForeGroundTime", data.get("time"));
      }
//      LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
//      localBroadcastManager.sendBroadcast(intent);
    }else if(data.containsKey("messageResCancel")){
      intent.setAction("messageFormFCM");
      intent.putExtra("messageResCancel", data.get("messageResCancel"));
    }else if(data.containsKey("messageCancelQueueSender")){
      intent.setAction("messageFormFCM");
      intent.putExtra("messageCancelQueueSender", data.get("messageCancelQueueSender"));
    }
    LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
    localBroadcastManager.sendBroadcast(intent);
    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
    Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.logotu);

    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "FirebaseCloudMessage")
        .setContentTitle(notification.getTitle())
        .setContentText(notification.getBody())
        .setAutoCancel(true)
//        .setSound(urls, AudioManager.STREAM_RING)
        .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(), 0))
        .setContentInfo(notification.getTitle())
        .setLargeIcon(icon)
        .setColor(Color.RED)
        .setLights(Color.RED, 1000, 300)
        .setDefaults(Notification.DEFAULT_VIBRATE)
        .setSmallIcon(R.drawable.logotu);

    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    // Notification Channel is required for Android O and above
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      NotificationChannel channel = new NotificationChannel("FirebaseCloudMessage", "firebaseCloudMessage", NotificationManager.IMPORTANCE_DEFAULT);
      channel.setDescription("channel FirebaseCloudMessage");
      channel.setShowBadge(true);
      channel.canShowBadge();
      channel.enableLights(true);
      channel.setLightColor(Color.BLACK);
      channel.enableVibration(true);
      notificationManager.createNotificationChannel(channel);
//      channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});
      NotificationChannel channelReceiver = new NotificationChannel("Receiver", "receiver", NotificationManager.IMPORTANCE_DEFAULT);
      channelReceiver.setDescription("channel Receiver");
      channelReceiver.setShowBadge(true);
      channelReceiver.canShowBadge();
      channelReceiver.enableLights(true);
      channelReceiver.setLightColor(Color.BLACK);
      channelReceiver.enableVibration(true);
      notificationManager.createNotificationChannel(channelReceiver);
    }

    notificationManager.notify(0, notificationBuilder.build());
//    count++;
  }

}
