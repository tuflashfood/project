package com.example.project.adapter;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.model.json.OrderBuy;
import com.example.project.R;
import com.example.project.model.json.Menu;

import java.util.List;

public class MenuRestaurantAdapter extends RecyclerView.Adapter<MenuRestaurantAdapter.RecyclerViewHolder> {

  private List<Menu> dataList ;
  private MyClickListener mCallback;
  private OrderBuy orderBuy ;
  private int id_res = 0 ;
  public MenuRestaurantAdapter(List<Menu> dataMenu) {
    this.dataList = dataMenu;
  }

  public void setOrder(OrderBuy order){
    this.orderBuy = order ;
  }

//  private void setId_res(int id_res){
//    this.id_res = id_res ;
//  }
  @NonNull
  @Override
  public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_menu_restuarant_fragment, parent, false);
    return new RecyclerViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerViewHolder holder, final int position) {
    Menu menu = dataList.get(position) ;
    holder.menu_image.setImageURI(Uri.parse("android.resource://"+holder.itemView.getContext().getPackageName()+"/drawable/"+menu.getPictureSmall()));
    holder.menu_name.setText(menu.getName());
    holder.menu_description.setText(menu.getDescription());
    holder.menu_price.setText(menu.getPrice() + " ฿");
    if(orderBuy != null){
      int result = orderBuy.getAmountProduct(menu.getName()) ;
      if(result != -1){
        holder.frameShowAmount_product.setVisibility(View.VISIBLE);
        holder.amount_product.setText(result + "");
      }
    }
  }

  @Override
  public int getItemCount() {
    return dataList.size();
  }

  public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    private ImageView menu_image ;
    private TextView menu_name ;
    private TextView menu_description ;
    private TextView menu_price ;
    private TextView amount_product ;
    private FrameLayout frameShowAmount_product ;
    private LinearLayout linearLayout ;

    public RecyclerViewHolder(@NonNull View itemView) {
      super(itemView);
      this.menu_image = (ImageView) itemView.findViewById(R.id.menu_image);
      this.menu_name = (TextView) itemView.findViewById(R.id.menu_name);
      this.menu_description = (TextView) itemView.findViewById(R.id.menu_description);
      this.menu_price = (TextView) itemView.findViewById(R.id.menu_price);
      this.amount_product = (TextView) itemView.findViewById(R.id.amount_product);
      this.frameShowAmount_product = (FrameLayout) itemView.findViewById(R.id.frameShowAmount_product);
      itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
      mCallback.onItemClick(dataList.get(getAdapterPosition()), v ,getAdapterPosition());
    }
  }
  public void setOnItemClickListener(MyClickListener mCallback){
    this.mCallback = mCallback;
  }

  public interface MyClickListener {
    public void onItemClick(Menu menus, View v, int position);
  }
}
