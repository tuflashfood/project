package com.example.project.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.model.json.Menu;
import com.example.project.model.json.PurchaseOrder;

import java.util.List;

public class SenderMainAdapter extends RecyclerView.Adapter<SenderMainAdapter.RecyclerViewHolder> {

  private List<PurchaseOrder> purchaseOrder ;
  private MyClickListener mCallback;
  @NonNull
  @Override
  public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_main_sender, parent, false);
    return new RecyclerViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
    PurchaseOrder purchase = this.purchaseOrder.get(position);
    holder.textView_sender_location.setText("ส่งที่: " + purchase.getOrder().getDestinationLocationName());
    if(purchase.getOrder().isOrder_complete()){
      holder.cardview_sender.setEnabled(false);
      holder.cardview_sender.setBackgroundResource(R.color.colorGray);
      holder.linear_sender_success.setVisibility(View.VISIBLE);
      holder.linear_sender_not_success.setVisibility(View.GONE);
    }else {
      holder.textView_sender_restaurant_name.setText("รับอาหารที่ร้าน: " + purchase.getRes_name());
      holder.textView_sender_total_price.setText("ยอดเงิน " + purchase.getOrder().getBillingCost() + " ฿");
    }
  }
  @Override
  public int getItemCount() {
    if(purchaseOrder.size() != 0)
      return purchaseOrder.size();
    return 0 ;
  }

  public void setPurchaseOrder(List<PurchaseOrder> purchaseOrder) {
    this.purchaseOrder = purchaseOrder;
  }

  public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView textView_sender_location ;
    private TextView textView_sender_restaurant_name ;
    private TextView textView_sender_total_price ;
    private LinearLayout linear_sender_success ;
    private LinearLayout linear_sender_not_success ;
    private CardView cardview_sender ;
    public RecyclerViewHolder(@NonNull View itemView) {
      super(itemView);
      textView_sender_location = itemView.findViewById(R.id.textView_sender_location);
      textView_sender_restaurant_name = itemView.findViewById(R.id.textView_sender_restaurant_name);
      textView_sender_total_price = itemView.findViewById(R.id.textView_sender_total_price);
      linear_sender_success = itemView.findViewById(R.id.linear_sender_success);
      linear_sender_not_success = itemView.findViewById(R.id.linear_sender_not_success);
      cardview_sender = itemView.findViewById(R.id.cardview_sender);
      cardview_sender.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
      mCallback.onItemClick(getAdapterPosition(), v);
    }
  }
  public void setOnItemClickListener(MyClickListener mCallback) {
    this.mCallback = mCallback;
  }

  public interface MyClickListener {
    public void onItemClick(int position, View v);
  }

}
