package com.example.project.adapter;

import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.model.json.Restaurant;

import java.util.List;

public class SelectRestaurantAdapter extends RecyclerView.Adapter<SelectRestaurantAdapter.RecyclerViewHolder> {
  private static final String TAG = "SelectRestaurantAdapter" ;
  private List<Restaurant> dataList;
  private MyClickListener mCallback;
  public SelectRestaurantAdapter(List<Restaurant> dataRes) {
    this.dataList = dataRes;
  }

  public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView name_res;
    TextView detail_res;
    TextView timeSender_res;
    TextView costSender_res;
    ImageView image_res;

    public RecyclerViewHolder(View itemView) {
      super(itemView);
      this.name_res = (TextView) itemView.findViewById(R.id.name_res);
      this.detail_res = (TextView) itemView.findViewById(R.id.detail_res);
      this.timeSender_res = (TextView) itemView.findViewById(R.id.timeSender_res);
      this.costSender_res = (TextView) itemView.findViewById(R.id.costSender_res);
      this.image_res = (ImageView) itemView.findViewById(R.id.image_res);
      itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
      mCallback.onItemClick(getAdapterPosition(), v);
    }
  }

  @NonNull
  @Override
  public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_select_restaurant_fragment, parent, false);
    return new RecyclerViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerViewHolder holder, final int position) {
    Restaurant restaurant = dataList.get(position) ;
    holder.image_res.setImageURI(Uri.parse("android.resource://"+holder.itemView.getContext().getPackageName()+"/drawable/"+restaurant.getMenus().getImage()));
    holder.name_res.setText("ร้าน " + restaurant.getResName());
    holder.detail_res.setText(restaurant.getMenus().getDetail());
    holder.timeSender_res.setText(restaurant.getCookTime() + " นาที");
    holder.costSender_res.setText("ค่าส่ง " + restaurant.getDeliveryCost() + " ฿");
  }

  public void setOnItemClickListener(MyClickListener mCallback) {
    this.mCallback = mCallback;
  }

  @Override
  public int getItemCount() {
    return dataList.size();
  }

  public interface MyClickListener {
    public void onItemClick(int position, View v);
  }
}
