package com.example.project.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.model.json.FoodOrder;
import com.example.project.model.json.Menu;
import com.example.project.model.json.OrderStatus;
import com.example.project.model.json.PurchaseOrder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class MainStatusOrderAdapter extends RecyclerView.Adapter<MainStatusOrderAdapter.RecyclerViewHolder> {
  private MyClickListener mCallback;
  private List<PurchaseOrder> purchaseOrders = new ArrayList<>();
  private static final String TAG = "MainStatusOrderAdapter" ;
  @NonNull
  @Override
  public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_main_restaurant, parent, false);
    return new RecyclerViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
    PurchaseOrder purchaseOrder = purchaseOrders.get(position) ;
    holder.textView_tracking_number.setText(" ใบสั่งซื้อ #"+String.format("%05d", purchaseOrder.getOrder().getOrderId())) ;
    holder.textView_resNameMenu.setText("ร้าน: "+ purchaseOrder.getRes_name() + "เมนู " + getNameMenu(purchaseOrder.getOrder().getFood_order())) ;
    holder.textView_res_OrderTime.setText(changeDateToString(purchaseOrder.getOrder().getOrder_status().get("order").getTime())) ;
    holder.textView_res_total_price.setText("ยอดเงิน " + purchaseOrder.getOrder().getBillingCost() + " ฿") ;
  }

  public void setPurchaseOrder(List<PurchaseOrder> purchaseOrder) {
    this.purchaseOrders = purchaseOrder;
  }

  private String changeDateToString(String timeParse) {
    Locale locale = new Locale("th");
    Date date = null;
    try {
      date = new SimpleDateFormat("dd/MM/yyyy - HH:mm", locale).parse(timeParse);
      String sDate = new SimpleDateFormat("dd MMMM yyyy", locale).format(date);
      String sTime = new SimpleDateFormat("HH:mm", locale).format(date);
      String timeOrder = "สั่งออเดอร์: " + sDate + ", " + "เวลา " + sTime + " น.";
//      Log.i(TAG, "onActivityCreated: " + timeOrder);
      if (!timeOrder.equals("")) return timeOrder;
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  }

  private String getNameMenu(List<FoodOrder> foodOrders) {
    String nameFoodOrders = "";
    for (FoodOrder food : foodOrders) {
      nameFoodOrders = nameFoodOrders.concat(food.getKey() + ", ") ;
    }
    nameFoodOrders = nameFoodOrders.substring(0,nameFoodOrders.length()-2);
    Log.i(TAG, "getNameMenu: " + nameFoodOrders);
    return nameFoodOrders ;
  }

  @Override
  public int getItemCount() {
    return purchaseOrders.size();
  }

  public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    private TextView textView_res_OrderTime ;
    private TextView textView_tracking_number ;
    private TextView textView_resNameMenu ;
    private TextView textView_res_total_price ;
    private LinearLayout linearLayout ;

    public RecyclerViewHolder(@NonNull View itemView) {
      super(itemView);
      this.textView_res_OrderTime = (TextView) itemView.findViewById(R.id.textView_res_OrderTime);
      this.textView_tracking_number = (TextView) itemView.findViewById(R.id.textView_tracking_number);
      this.textView_resNameMenu = (TextView) itemView.findViewById(R.id.textView_resNameMenu);
      this.textView_res_total_price = (TextView) itemView.findViewById(R.id.textView_res_total_price);
      itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
      mCallback.onItemClick(purchaseOrders.get(getAdapterPosition()).getOrder().getOrder_status(),purchaseOrders.get(getAdapterPosition()).getOrder().getOrderId()+"");
    }
  }
  public void setOnItemClickListener(MyClickListener mCallback) {
    this.mCallback = mCallback;
  }
  public interface MyClickListener {
    public void onItemClick(HashMap<String, OrderStatus> order_status , String statusOrderId);
  }
}
