package com.example.project.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.model.json.OrderBuy;
import com.example.project.ui.select_restaurant.SelectRestaurantViewModel;

import org.parceler.Parcels;

import java.util.HashMap;
import java.util.List;

public class ShoppingBasketAdapter extends RecyclerView.Adapter<ShoppingBasketAdapter.RecyclerViewHolder> {
  public static String TAG = "ShoppingBasketAdapter" ;
  private static final String SEND_BASKET_ARG_BUNDLE = "basket";

  private static final int MIN_DISTANCE = 0;
  private float x1, x2;
  private SelectRestaurantViewModel selectRestaurantViewModel ;
  private List<HashMap<String,String>> menuList ;

  private OrderBuy orderBuy ;
  private Context context;
  public ShoppingBasketAdapter(Context context,List<HashMap<String,String>> menuList){
    this.context = context ;
    this.menuList = menuList ;
    this.selectRestaurantViewModel = ViewModelProviders.of((FragmentActivity) context).get(SelectRestaurantViewModel.class);
  }

  public void setOrderBuy(OrderBuy orderBuy){
    this.orderBuy = orderBuy ;
  }

  @NonNull
  @Override
  public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.shoppingbasket_menu_fragment, parent, false);
    return new RecyclerViewHolder(view);
  }

  @SuppressLint("ClickableViewAccessibility")
  @Override
  public void onBindViewHolder(@NonNull final RecyclerViewHolder holder, final int position) {
    holder.currentMenu = menuList.get(position);
    holder.recycle_amount.setText(menuList.get(position).get("amount"));
    holder.recycle_name_menu.setText(menuList.get(position).get("menu_name"));
    holder.recycle_total_price.setText(menuList.get(position).get("total_price") + " ฿");
    holder.recycle_negare.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if(Integer.parseInt(menuList.get(position).get("amount")) == 1){
          holder.main_menu_slide.animate().translationXBy(-holder.linear_recycle_remove_menu.getWidth() - holder.main_menu_slide.getTranslationX()).setDuration(500);
        }else {
          orderBuy.changeAmountMenu(menuList.get(position).get("key"),"-1");
          selectRestaurantViewModel.setOrderBuy(orderBuy);
          notifyDataSetChanged();
        }
      }
    });
    holder.recycle_plus.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        orderBuy.changeAmountMenu(menuList.get(position).get("key"),"1");
        Log.i(TAG, "onClick: " + orderBuy.toString());
        selectRestaurantViewModel.setOrderBuy(orderBuy);
        notifyDataSetChanged();
      }
    });
    holder.recycle_remove_menu.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        orderBuy.removeMenuByKey(menuList.get(position).get("key"));
        selectRestaurantViewModel.setOrderBuy(orderBuy);
        //fix bug  เมื่อลบสินค้าแล้วตัวมันจะไปแทนที่ ทำให้ตัวปัจจุบันจะลบด้วย
        holder.main_menu_slide.animate().translationXBy(-holder.linear_recycle_remove_menu.getWidth() - holder.main_menu_slide.getTranslationX()).setDuration(0);
        notifyDataSetChanged();
      }
    });
  }

  @Override
  public int getItemCount() {
    return menuList.size();
  }

  public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnTouchListener {
    private TextView recycle_amount ;
    private TextView recycle_name_menu ;
    private TextView recycle_total_price ;
    private Button recycle_negare ;
    private Button recycle_plus ;
    private Button recycle_remove_menu ;
    private LinearLayout main_menu_slide ;
    private LinearLayout linear_recycle_remove_menu ;
    private HashMap<String,String> currentMenu ;


    @SuppressLint("ClickableViewAccessibility")
    RecyclerViewHolder(@NonNull View itemView) {
      super(itemView);
      this.recycle_amount = (TextView) itemView.findViewById(R.id.recycle_amount);
      this.recycle_name_menu = (TextView) itemView.findViewById(R.id.recycle_name_menu);
      this.recycle_total_price = (TextView) itemView.findViewById(R.id.recycle_total_price);
      this.recycle_negare = (Button) itemView.findViewById(R.id.recycle_negare);
      this.recycle_plus = (Button) itemView.findViewById(R.id.recycle_plus);
      this.recycle_remove_menu = (Button) itemView.findViewById(R.id.recycle_remove_menu);
      this.main_menu_slide = (LinearLayout) itemView.findViewById(R.id.main_menu_slide);
      this.linear_recycle_remove_menu = (LinearLayout) itemView.findViewById(R.id.linear_recycle_remove_menu);
      this.main_menu_slide.setOnTouchListener(this);
    }

    private void attemptClaimDrag() {
      //fix bug action=ACTION_CANCEL
      if (main_menu_slide  != null) {
        main_menu_slide.requestDisallowInterceptTouchEvent(true);
      }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
      switch (event.getAction()) {
        case MotionEvent.ACTION_UP:
          double x3 = event.getX() - x1;
          if(Math.abs(main_menu_slide.getTranslationX()) == MIN_DISTANCE && (x3 >= 0 && x3 <= 5)){
            Bundle bundle = new Bundle();
            bundle.putParcelable(SEND_BASKET_ARG_BUNDLE, Parcels.wrap(currentMenu));
            Navigation.findNavController(v).navigate(R.id.nav_menu_option,bundle);
          } else if (Math.abs(main_menu_slide.getTranslationX()) > Math.abs(linear_recycle_remove_menu.getWidth() / 2)) {
            main_menu_slide.animate().translationXBy(-linear_recycle_remove_menu.getWidth() - main_menu_slide.getTranslationX()).setDuration(500);
          } else if(Math.abs(main_menu_slide.getTranslationX()) < Math.abs(linear_recycle_remove_menu.getWidth() / 2)){
            main_menu_slide.animate().translationXBy(Math.abs(main_menu_slide.getTranslationX())).setDuration(500);
          }
          break;
        case MotionEvent.ACTION_DOWN:
          x1 = event.getX();
          attemptClaimDrag();
          break;
        case MotionEvent.ACTION_MOVE:
          x2 = event.getX();
          float deltaX = x2 - x1;
          //deltaX > 0 is left to right
          // deltaX < 0 right to left
          if (main_menu_slide.getTranslationX() + deltaX > 0) {
            deltaX = Math.abs(main_menu_slide.getTranslationX());
          }else if (main_menu_slide.getTranslationX() + deltaX < -linear_recycle_remove_menu.getWidth()) {
            deltaX = -linear_recycle_remove_menu.getWidth() - main_menu_slide.getTranslationX();
          }
          // not move
          if(deltaX == 0) break ;
          main_menu_slide.animate().translationXBy(deltaX).setDuration(0);
          break;
      }
      return true;
    }
  }

}
