package com.example.project.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.model.json.FoodOrder;
import com.example.project.model.json.Menu;
import com.example.project.model.json.PurchaseOrder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class OrderRestaurantMainAdapter extends RecyclerView.Adapter<OrderRestaurantMainAdapter.RecyclerViewHolder> {

  private MyClickListener mCallback;
  private List<PurchaseOrder> purchaseOrder;
  private static final String TAG = "OrderRestaurantMainAdapter";

  @NonNull
  @Override
  public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_main_restaurant, parent, false);
    return new RecyclerViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
    PurchaseOrder p = purchaseOrder.get(position);
    Log.i(TAG, "onBindViewHolder: " + p);
    holder.textView_tracking_number.setText(" ใบสั่งซื้อ #" + String.format("%05d", p.getOrder().getOrderId()));
    holder.textView_res_OrderTime.setText(changeDateToString(p.getOrder().getOrder_status().get("order").getTime()));
    holder.textView_resNameMenu.setText("เมนูสั่งซื้อ: " + getNameMenu(p.getOrder().getFood_order()));
    holder.textView_res_total_price.setText("ยอดเงิน " + p.getOrder().getBillingCost() + " ฿");
  }

  public void setPurchaseOrder(List<PurchaseOrder> purchaseOrder) {
    this.purchaseOrder = purchaseOrder;
  }

  private String getNameMenu(List<FoodOrder> foodOrders) {
    String nameFoodOrders = "";
    for (FoodOrder food : foodOrders) {
      nameFoodOrders = nameFoodOrders.concat(food.getKey() + ", ");
    }
    nameFoodOrders = nameFoodOrders.substring(0, nameFoodOrders.length() - 2);
    return nameFoodOrders;
  }

  private String changeDateToString(String timeParse) {
    Locale locale = new Locale("th");
    Date date = null;
    try {
      date = new SimpleDateFormat("dd/MM/yyyy - HH:mm", locale).parse(timeParse);
      String sDate = new SimpleDateFormat("dd MMMM yyyy", locale).format(date);
      String sTime = new SimpleDateFormat("HH:mm", locale).format(date);
      String timeOrder = "สั่งออเดอร์: " + sDate + ", " + "เวลา " + sTime + " น.";
//      Log.i(TAG, "onActivityCreated: " + timeOrder);
      if (!timeOrder.equals("")) return timeOrder;
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public int getItemCount() {
    if(purchaseOrder == null) return 0 ;
    return purchaseOrder.size();
  }

  public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView textView_tracking_number;
    private TextView textView_res_OrderTime;
    private TextView textView_resNameMenu;
    private TextView textView_res_total_price;
//    private LinearLayout linear_res_not_success ;

    public RecyclerViewHolder(@NonNull View itemView) {
      super(itemView);
      textView_tracking_number = itemView.findViewById(R.id.textView_tracking_number);
      textView_res_OrderTime = itemView.findViewById(R.id.textView_res_OrderTime);
      textView_resNameMenu = itemView.findViewById(R.id.textView_resNameMenu);
      textView_res_total_price = itemView.findViewById(R.id.textView_res_total_price);
      itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
      mCallback.onItemClick(v, getAdapterPosition());
    }
  }

  public void setOnItemClickListener(MyClickListener mCallback) {
    this.mCallback = mCallback;
  }

  public interface MyClickListener {
    public void onItemClick(View v, int position);
  }


}
