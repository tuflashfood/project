package com.example.project.model;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.project.model.json.OrderBuy;
import com.example.project.model.json.ResponseBody;
import com.example.project.service.HttpManager;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectRestaurantRepository {
  private Call<ResponseBody> callOrder;
  private MutableLiveData<ResponseBody> responseOrder = new MutableLiveData<>();
  private String message ;
  private String TAG = "SelectRestaurantRepository" ;

  public MutableLiveData<ResponseBody> getSendDataOrderToServer(OrderBuy orderBuy,String time){
    sendDataOrderToServer(orderBuy,time);
    return responseOrder ;
  }

  public void sendDataOrderToServer(OrderBuy orderBuy,String time){
    Log.i(TAG, "sendDataOrderToServer: " + orderBuy);
    callOrder = HttpManager.getInstance().getService().sendOrder(orderBuy.getRes_id(),orderBuy.getBuy_type_user_id(),orderBuy.getMenuSendServer(),
        orderBuy.getBillingCost() , orderBuy.getDestination_location() ,orderBuy.getDestination_location_name(), time);
    callOrder.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if(response.body() != null) {
          responseOrder.setValue(response.body());
        }else {
          try {
            Gson gson = new Gson();
            ResponseBody error = gson.fromJson(response.errorBody().string(),ResponseBody.class) ;
            responseOrder.setValue(error);
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e(TAG, "onFailure: "  + t.toString());
        message = t.toString() ;
      }
    });
//    return message ;
  }

}
