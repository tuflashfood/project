package com.example.project.model.json;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class FoodOrder implements Parcelable {

  @SerializedName("amount")
  private String amount;
  @SerializedName("senderTypeId")
  private int senderTypeId;
  @SerializedName("total_price")
  private String total_price;
  @SerializedName("product_notes")
  private String product_notes;
  @SerializedName("price")
  private String price;
  @SerializedName("menu_name")
  private String menu_name;
  @SerializedName("key")
  private String key;

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public String getTotal_price() {
    return total_price;
  }

  public void setTotal_price(String total_price) {
    this.total_price = total_price;
  }

  public String getProduct_notes() {
    return product_notes;
  }

  public void setProduct_notes(String product_notes) {
    this.product_notes = product_notes;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getMenu_name() {
    return menu_name;
  }

  public void setMenu_name(String menu_name) {
    this.menu_name = menu_name;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  @Override
  public String toString() {
    return "FoodOrder{" +
        "amount='" + amount + '\'' +
        ", senderTypeId=" + senderTypeId +
        ", total_price='" + total_price + '\'' +
        ", product_notes='" + product_notes + '\'' +
        ", price='" + price + '\'' +
        ", menu_name='" + menu_name + '\'' +
        ", key='" + key + '\'' +
        '}';
  }


  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.amount);
    dest.writeInt(this.senderTypeId);
    dest.writeString(this.total_price);
    dest.writeString(this.product_notes);
    dest.writeString(this.price);
    dest.writeString(this.menu_name);
    dest.writeString(this.key);
  }

  public FoodOrder() {
  }

  protected FoodOrder(Parcel in) {
    this.amount = in.readString();
    this.senderTypeId = in.readInt();
    this.total_price = in.readString();
    this.product_notes = in.readString();
    this.price = in.readString();
    this.menu_name = in.readString();
    this.key = in.readString();
  }

  public static final Parcelable.Creator<FoodOrder> CREATOR = new Parcelable.Creator<FoodOrder>() {
    @Override
    public FoodOrder createFromParcel(Parcel source) {
      return new FoodOrder(source);
    }

    @Override
    public FoodOrder[] newArray(int size) {
      return new FoodOrder[size];
    }
  };
}
