package com.example.project.model.json;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Restaurant implements Parcelable {

	@SerializedName("res_type_user_id")
	private int resTypeUserId;

	@SerializedName("cook_time")
	private int cookTime;

	@SerializedName("delivery_cost")
	private int deliveryCost;

	@SerializedName("res_id")
	private int resId;

	@SerializedName("res_status")
	private boolean resStatus;

	@SerializedName("location")
	private Location location;

	@SerializedName("menus")
	private Menus menus;

	@SerializedName("res_name")
	private String resName;

	public void setResTypeUserId(int resTypeUserId){
		this.resTypeUserId = resTypeUserId;
	}

	public int getResTypeUserId(){
		return resTypeUserId;
	}

	public void setCookTime(int cookTime){
		this.cookTime = cookTime;
	}

	public int getCookTime(){
		return cookTime;
	}

	public void setDeliveryCost(int deliveryCost){
		this.deliveryCost = deliveryCost;
	}

	public int getDeliveryCost(){
		return deliveryCost;
	}

	public void setResId(int resId){
		this.resId = resId;
	}

	public int getResId(){
		return resId;
	}

	public void setResStatus(boolean resStatus){
		this.resStatus = resStatus;
	}

	public boolean isResStatus(){
		return resStatus;
	}

	public void setLocation(Location location){
		this.location = location;
	}

	public Location getLocation(){
		return location;
	}

	public void setMenus(Menus menus){
		this.menus = menus;
	}

	public Menus getMenus(){
		return menus;
	}

	public void setResName(String resName){
		this.resName = resName;
	}

	public String getResName(){
		return resName;
	}

	@Override
 	public String toString(){
		return 
			"Restaurant{" +
			"res_type_user_id = '" + resTypeUserId + '\'' + 
			",cook_time = '" + cookTime + '\'' + 
			",delivery_cost = '" + deliveryCost + '\'' + 
			",res_id = '" + resId + '\'' + 
			",res_status = '" + resStatus + '\'' + 
			",location = '" + location + '\'' + 
			",menus = '" + menus + '\'' + 
			",res_name = '" + resName + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.resTypeUserId);
		dest.writeInt(this.cookTime);
		dest.writeInt(this.deliveryCost);
		dest.writeInt(this.resId);
		dest.writeByte(this.resStatus ? (byte) 1 : (byte) 0);
		dest.writeParcelable(this.location, flags);
		dest.writeParcelable(this.menus, flags);
		dest.writeString(this.resName);
	}

	public Restaurant() {
	}

	protected Restaurant(Parcel in) {
		this.resTypeUserId = in.readInt();
		this.cookTime = in.readInt();
		this.deliveryCost = in.readInt();
		this.resId = in.readInt();
		this.resStatus = in.readByte() != 0;
		this.location = in.readParcelable(Location.class.getClassLoader());
		this.menus = in.readParcelable(Menus.class.getClassLoader());
		this.resName = in.readString();
	}

	public static final Parcelable.Creator<Restaurant> CREATOR = new Parcelable.Creator<Restaurant>() {
		@Override
		public Restaurant createFromParcel(Parcel source) {
			return new Restaurant(source);
		}

		@Override
		public Restaurant[] newArray(int size) {
			return new Restaurant[size];
		}
	};
}