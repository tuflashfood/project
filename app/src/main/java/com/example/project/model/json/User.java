package com.example.project.model.json;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class User implements Parcelable {

  @SerializedName("username")
  private String username;
  @SerializedName("user_id")
  private String user_id;
  @SerializedName("name")
  private String name;
  @SerializedName("type")
  private String type;
  @SerializedName("phone")
  private String phone;
  @SerializedName("firebase_token")
  private String firebase_token;
  @SerializedName("errors")
  private String errors;

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getFirebase_token() {
    return firebase_token;
  }

  public void setFirebase_token(String firebase_token) {
    this.firebase_token = firebase_token;
  }

  public String getErrors() {
    return errors;
  }

  public void setErrors(String errors) {
    this.errors = errors;
  }

  @Override
  public String toString() {
    return "User{" +
        "username='" + username + '\'' +
        ", user_id=" + user_id +
        ", name='" + name + '\'' +
        ", type='" + type + '\'' +
        ", phone='" + phone + '\'' +
        ", firebase_token=" + firebase_token +
        ", errors='" + errors + '\'' +
        '}';
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.username);
    dest.writeString(this.user_id);
    dest.writeString(this.name);
    dest.writeString(this.type);
    dest.writeString(this.phone);
    dest.writeString(this.firebase_token);
    dest.writeString(this.errors);
  }

  public User() {
  }

  protected User(Parcel in) {
    this.username = in.readString();
    this.user_id = in.readString();
    this.name = in.readString();
    this.type = in.readString();
    this.phone = in.readString();
    this.firebase_token = in.readString();
    this.errors = in.readString();
  }

  public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
    @Override
    public User createFromParcel(Parcel source) {
      return new User(source);
    }

    @Override
    public User[] newArray(int size) {
      return new User[size];
    }
  };
}
