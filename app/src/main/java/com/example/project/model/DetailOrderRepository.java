package com.example.project.model;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.project.model.json.ResponseBody;
import com.example.project.service.HttpManager;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailOrderRepository {
  private static final String TAG = "DetailOrderRepository" ;
  private MutableLiveData<ResponseBody> responseMessageRestaurantSuccess = new MutableLiveData<>();
  private MutableLiveData<ResponseBody> responseMessageUpdateOrderStatus = new MutableLiveData<>();
  private MutableLiveData<ResponseBody> responseMessageSearchSender = new MutableLiveData<>() ;
  private MutableLiveData<ResponseBody> responseMessageUpdateSenderIdOrder = new MutableLiveData<>() ;
  private MutableLiveData<ResponseBody> responseMessageRemoveOrder = new MutableLiveData<>() ;
  private MutableLiveData<ResponseBody> responseMessageUpdateOrderRestaurant = new MutableLiveData<>() ;
  private MutableLiveData<ResponseBody> responseMessageUpdateOrderComplete = new MutableLiveData<>() ;
  private MutableLiveData<ResponseBody> responseMessageAnswerToSender = new MutableLiveData<>() ;
  private Call<ResponseBody> callResponseMessageAnswerToSender ;
  private Call<ResponseBody> callResponseUpdateOrderComplete ;
  private Call<ResponseBody> callResponseUpdateOrderRestaurant ;
  private Call<ResponseBody> callResponseSearchSender ;
  private Call<ResponseBody> callResponseUpdateSenderIdOrder ;
//  private Call<ResponseBody> callResponseMRS;
  private Call<ResponseBody> callResponseUpdateStatus ;
  private Call<ResponseBody> callResponseRemoveOrder ;


  public MutableLiveData<ResponseBody> getSendAnswerToSender(String sender_id,String order_id,String value){
    sendAnswerToSender(sender_id, order_id, value);
    return responseMessageAnswerToSender;
  }

  private void sendAnswerToSender(String sender_id,String order_id,String value){
    callResponseMessageAnswerToSender = HttpManager.getInstance().getService().sendAnswerToSender(sender_id, order_id, value);
    callResponseMessageAnswerToSender.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if(response.isSuccessful()){
          responseMessageAnswerToSender.setValue(response.body());
        }else {
          try {
            Gson gson = new Gson();
            responseMessageAnswerToSender.setValue(gson.fromJson(response.errorBody().string(),ResponseBody.class));
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e(TAG, "onFailure: 1" + t.toString());
      }
    });
  }

  public MutableLiveData<ResponseBody> getSendUpdateOrderComplete(String order_id,String value) {
    sendUpdateOrderComplete(order_id,value);
    return responseMessageUpdateOrderComplete ;
  }

  private void sendUpdateOrderComplete(String order_id, String value){
    callResponseUpdateOrderComplete = HttpManager.getInstance().getService().sendUpdateOrderComplete(order_id, value);
    callResponseUpdateOrderComplete.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if(response.isSuccessful()){
          responseMessageUpdateOrderComplete.setValue(response.body());
        }else {
          try {
            Gson gson = new Gson() ;
            responseMessageUpdateOrderComplete.setValue(gson.fromJson(response.errorBody().string(),ResponseBody.class));
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e(TAG, "onFailure: 2" + t.toString());
      }
    });
  }

  public MutableLiveData<ResponseBody> getSendUpdateOrderRestaurant(String order_id,String value) {
    sendUpdateOrderRestaurant(order_id,value);
    return responseMessageUpdateOrderRestaurant ;
  }

  private void sendUpdateOrderRestaurant(String order_id, String value){
    callResponseUpdateOrderRestaurant = HttpManager.getInstance().getService().sendUpdateOrderRestaurant(order_id,value);
    callResponseUpdateOrderRestaurant.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if(response.isSuccessful()){
          responseMessageUpdateOrderRestaurant.setValue(response.body());
        }else {
          try {
            Gson gson = new Gson();
            ResponseBody responseBody = gson.fromJson(response.errorBody().string(),ResponseBody.class);
            responseMessageUpdateOrderRestaurant.setValue(responseBody);
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e(TAG, "onFailure: 3" + t.toString());
      }
    });
  }

  public MutableLiveData<ResponseBody> getSendRemoveOrderSender(String order_id) {
    sendRemoveOrderSender(order_id);
    return responseMessageRemoveOrder ;
  }

  private void sendRemoveOrderSender(String order_id){
    callResponseRemoveOrder = HttpManager.getInstance().getService().getRemoveOrderSender(order_id);
    callResponseRemoveOrder.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if(response.isSuccessful()) {
          responseMessageRemoveOrder.setValue(response.body());
        }else {
          try {
            Gson gson = new Gson();
            ResponseBody responseBody = gson.fromJson(response.errorBody().string(),ResponseBody.class);
            responseMessageRemoveOrder.setValue(responseBody);
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e(TAG, "onFailure: 4" + t.toString());
      }
    });
  }

  public MutableLiveData<ResponseBody> getSendUpdateOrderSenderIdOrder(String order_id, String sender_id) {
    sendUpdateOrderIdOrder(order_id, sender_id);
    return responseMessageUpdateSenderIdOrder ;
  }

  private void sendUpdateOrderIdOrder(String order_id,String sender_id) {
    callResponseUpdateSenderIdOrder = HttpManager.getInstance().getService().sendUpdateSenderIdOrder(order_id, sender_id);
    callResponseUpdateSenderIdOrder.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if(response.isSuccessful()) {
          responseMessageUpdateSenderIdOrder.setValue(response.body());
        }else {
          Log.e(TAG, "onResponse: " + response.errorBody().toString());
        }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e(TAG, "onFailure: 5" + t.toString());
      }
    });
  }

//  public MutableLiveData<ResponseBody> getCheckOrderRestaurantOrderSuccess(String order_id) {
//    fetchCheckOrderRestaurantOrderSuccess(order_id) ;
//    Log.i(TAG, "getCheckOrderRestaurantOrderSuccess: " + responseMessageRestaurantSuccess);
//    return responseMessageRestaurantSuccess;
//  }
//
//  public MutableLiveData<ResponseBody> getResponseMessageRestaurantSuccess() {
//    return responseMessageRestaurantSuccess;
//  }

  private void fetchCheckOrderRestaurantOrderSuccess(String order_id) {
//    callResponseMRS = HttpManager.getInstance().getService().checkOrderRestaurantOrderSuccess(order_id);
//    callResponseMRS.enqueue(new Callback<ResponseBody>() {
//      @Override
//      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//        Gson gResponseBody = new Gson();
//        if (response.isSuccessful()) {
//          Log.i(TAG, "onResponse: " + response.body());
//          responseMessageRestaurantSuccess.setValue(response.body());
//        }
//        else {
//          try {
//            Log.i(TAG, "onResponse: " + response.errorBody().string());
//          } catch (IOException e) {
//            e.printStackTrace();
//          }
//        }
//      }
//
//      @Override
//      public void onFailure(Call<ResponseBody> call, Throwable t) {
//        Log.e(TAG, "onFailure: 6" + t.toString());
//      }
//    });
  }

  public MutableLiveData<ResponseBody> getUpdateStatusSender(String order_id, String status){
    sendUpdateStatusOrder(order_id,status);
    return responseMessageUpdateOrderStatus ;
  }

  private void sendUpdateStatusOrder(String order_id ,String status) {
    callResponseUpdateStatus = HttpManager.getInstance().getService().sendUpdateStatusOrder(order_id,status) ;
    callResponseUpdateStatus.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if (response.isSuccessful())
          responseMessageUpdateOrderStatus.setValue(response.body());
        else
          Log.i(TAG, "onResponse: " + response.errorBody().toString());
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e(TAG, "onFailure: 7" + t.toString());
      }
    });
  }

  public MutableLiveData<ResponseBody> getSearchSender(String order_id) {
    sendSearchSender(order_id);
    return responseMessageSearchSender ;
  }

  private void sendSearchSender(String order_id){
    callResponseSearchSender = HttpManager.getInstance().getService().sendSearchSender(order_id) ;
    callResponseSearchSender.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if(response.isSuccessful()) {
          responseMessageSearchSender.setValue(response.body());
        }else {
          Log.i(TAG, "onResponse: " + response.errorBody().toString());
        }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e(TAG, "onFailure: 8" + t.toString());
      }
    });
  }
}
