package com.example.project.model.json;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class OrderStatus implements Parcelable {

  @SerializedName("value")
  private boolean value;
  @SerializedName("time")
  private String time;

  public boolean isValue() {
    return value;
  }

  public void setValue(boolean value) {
    this.value = value;
  }

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  @Override
  public String toString() {
    return "OrderStatus{" +
        "value=" + value +
        ", time='" + time + '\'' +
        '}';
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeByte(this.value ? (byte) 1 : (byte) 0);
    dest.writeString(this.time);
  }

  public OrderStatus() {
  }

  protected OrderStatus(Parcel in) {
    this.value = in.readByte() != 0;
    this.time = in.readString();
  }

  public static final Parcelable.Creator<OrderStatus> CREATOR = new Parcelable.Creator<OrderStatus>() {
    @Override
    public OrderStatus createFromParcel(Parcel source) {
      return new OrderStatus(source);
    }

    @Override
    public OrderStatus[] newArray(int size) {
      return new OrderStatus[size];
    }
  };
}
