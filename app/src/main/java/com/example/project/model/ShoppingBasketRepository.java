package com.example.project.model;


import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.project.model.json.ResponseBody;
import com.example.project.service.HttpManager;

import org.parceler.Parcels;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShoppingBasketRepository {
  private static final String TAG = "ShoppingBasketRepository" ;
  private MutableLiveData<ResponseBody> currentLocation = new MutableLiveData<>() ;
  private Call<ResponseBody> callCurrentLocation ;

  public MutableLiveData<ResponseBody> getCurrentLocation() {
    return currentLocation;
  }

  public MutableLiveData<ResponseBody> getFetchCurrentLocation(String latlong) {
    fetchCurrentLocation(latlong);
    return currentLocation ;
  }
  private void fetchCurrentLocation(String latlong) {
    callCurrentLocation = HttpManager.getInstance().getService().getCurrentLocation(latlong) ;
    callCurrentLocation.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
       if (response.isSuccessful()) {
         currentLocation.setValue(response.body());
       }else {
         Log.i(TAG, "onResponse: " + response.errorBody().toString());
       }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e(TAG, "onFailure: " + t.toString());
      }
    });
  }
}
