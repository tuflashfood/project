package com.example.project.model.json;

import com.google.gson.annotations.SerializedName;

public class ResponseBody {
  @SerializedName("message")
  private String message ;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public String toString() {
    return "ResponseBody{" +
        "message='" + message + '\'' +
        '}';
  }
}
