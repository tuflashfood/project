package com.example.project.model;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.project.model.json.Restaurant;
import com.example.project.service.HttpManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RestaurantRepository {
  private String TAG = "RestaurantRepository";
  private Call<List<Restaurant>> callRestaurant;
  private MutableLiveData<List<Restaurant>> restaurantMutableLiveData ;
  private List<Restaurant> restaurantList;

  public RestaurantRepository() {
    this.restaurantMutableLiveData = new MutableLiveData<>();
    this.restaurantList = new ArrayList<>();
  }

  public void fetchDataRestaurantOpen() {
    callRestaurant = HttpManager.getInstance().getService().getDataRestaurantOpen();
    callRestaurant.enqueue(new Callback<List<Restaurant>>() {
      @Override
      public void onResponse(Call<List<Restaurant>> call, Response<List<Restaurant>> response) {
        if (response.isSuccessful()) {
          restaurantMutableLiveData.setValue(response.body());
          Log.i(TAG, response.body() + "");
        } else {
          Log.i(TAG, response.errorBody().toString());
        }
      }

      @Override
      public void onFailure(Call<List<Restaurant>> call, Throwable t) {
        Log.e(TAG, "onFailure: " + t.toString());
        Log.i(TAG, t.toString());
        Log.i(TAG, "------------------------");
      }
    });
  }


  public MutableLiveData<List<Restaurant>> getRestaurantMutableLiveData() {
    fetchDataRestaurantOpen();
    return restaurantMutableLiveData;
  }

  public MutableLiveData<Restaurant> getRestaurantByIndex(int index) {
    Restaurant restaurant = restaurantMutableLiveData.getValue().get(index) ;
    return new MutableLiveData<>(restaurant);
  }


}
