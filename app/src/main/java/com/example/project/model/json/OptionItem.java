package com.example.project.model.json;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.List;
import com.google.gson.annotations.SerializedName;


public class OptionItem implements Parcelable {

	@SerializedName("select")
	private String select;

	@SerializedName("data")
	private List<HashMap<String,String>> data;

	@SerializedName("detail")
	private String detail;

	public void setSelect(String select){
		this.select = select;
	}

	public String getSelect(){
		return select;
	}

	public List<HashMap<String, String>> getData() {
		return data;
	}

	public void setData(List<HashMap<String, String>> data) {
		this.data = data;
	}

	public void setDetail(String detail){
		this.detail = detail;
	}

	public String getDetail(){
		return detail;
	}

	@Override
 	public String toString(){
		return
			"OptionItem{" +
			"select = '" + select + '\'' +
			",data = '" + data + '\'' +
			",detail = '" + detail + '\'' +
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.select);
		dest.writeList(this.data);
		dest.writeString(this.detail);
	}

	public OptionItem() {
	}

	protected OptionItem(Parcel in) {
		this.select = in.readString();
		this.data = (List<HashMap<String, String>>) in.readSerializable();
		this.detail = in.readString();
	}

	public static final Parcelable.Creator<OptionItem> CREATOR = new Parcelable.Creator<OptionItem>() {
		@Override
		public OptionItem createFromParcel(Parcel source) {
			return new OptionItem(source);
		}

		@Override
		public OptionItem[] newArray(int size) {
			return new OptionItem[size];
		}
	};
}