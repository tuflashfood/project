package com.example.project.model;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.util.Log;

import com.example.project.service.AlarmReceiver;

import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.logging.SimpleFormatter;

import static android.content.Context.ALARM_SERVICE;

public class NotificationHelper {
  private static int ALARM_TYPE_RTC = 100;
  private static AlarmManager alarmManagerRTC;
  private static PendingIntent alarmIntentRTC;
  private static String order_id = "";
  private static String TAG = "NotificationHelper";
  private static SimpleDateFormat simpleDateFormat;
  private static NotificationManager mNotificationManager;

  public static void scheduleRepeatingRTCNotification(Context context, String timeOrderStart, String order) {
    simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm", new Locale("th"));
    long currentTimeMillis = System.currentTimeMillis();
    long time15ms = 15 * 60 * 1000;
    long timeDelay = 0;
    Date dateOrderStar = null;
    try {
      dateOrderStar = simpleDateFormat.parse(timeOrderStart);
      timeDelay = dateOrderStar.getTime() - currentTimeMillis - time15ms;
      Log.i(TAG, "scheduleRepeatingRTCNotification: " + timeDelay);
      int requestID = (int) System.currentTimeMillis();
      order_id = order;
      Intent intent = new Intent(context, AlarmReceiver.class);
      intent.putExtra("order_id", order_id);
      intent.putExtra("title", "ใบออเดอร์ที่" + order_id);
      intent.putExtra("contentText", "เริ่มทำอาหาร");
      alarmIntentRTC = PendingIntent.getBroadcast(context, ALARM_TYPE_RTC, intent, PendingIntent.FLAG_UPDATE_CURRENT);
      alarmManagerRTC = (AlarmManager) context.getSystemService(ALARM_SERVICE);
//      alarmManagerRTC.set(AlarmManager.RTC_WAKEUP, currentTimeMillis + timeDelay, alarmIntentRTC);
      //fix time 10 ms
      alarmManagerRTC.set(AlarmManager.RTC_WAKEUP, currentTimeMillis + (1000 * 10), alarmIntentRTC);
    } catch (ParseException e) {
      e.printStackTrace();
    }

  }

  public void setOrder_id(String order_id) {
    this.order_id = order_id;
  }

  public String getOrder_id() {
    return order_id;
  }

  public void cancelAlarmRTC() {
    if (alarmManagerRTC != null) {
      Log.i(TAG, "cancelAlarmRTC: " + alarmIntentRTC);
      alarmManagerRTC.cancel(alarmIntentRTC);
      alarmIntentRTC.cancel();
    }
  }

  public static NotificationManager getNotificationManager(Context context) {
    return (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
  }

  @Override
  public String toString() {
    return "NotificationHelper{" +
        "alarmManagerRTC=" + alarmManagerRTC +
        ", alarmIntentRTC=" + alarmIntentRTC +
        ", order_id='" + order_id + '\'' +
        '}';
  }
}
