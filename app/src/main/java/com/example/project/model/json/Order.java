package com.example.project.model.json;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.List;

public class Order implements Parcelable {

  @SerializedName("order_id")
  private int orderId;

  @SerializedName("res_id")
  private int resId;

  @SerializedName("buy_type_user_id")
  private int buyTypeUserId;

  @SerializedName("order_status")
  private HashMap<String, OrderStatus> order_status;

  @SerializedName("food_order")
  private List<FoodOrder> food_order;

  @SerializedName("destination_location")
  private Location destination_location;

  @SerializedName("destination_location_name")
  private String destinationLocationName;

  @SerializedName("billing_cost")
  private int billingCost;

  @SerializedName("sender_type_id")
  private int senderTypeId;

  @SerializedName("order_complete")
  private boolean order_complete ;

  @SerializedName("order_restaurant")
  private boolean order_restaurant ;

  public int getOrderId() {
    return orderId;
  }

  public void setOrderId(int orderId) {
    this.orderId = orderId;
  }

  public int getResId() {
    return resId;
  }

  public void setResId(int resId) {
    this.resId = resId;
  }

  public int getBuyTypeUserId() {
    return buyTypeUserId;
  }

  public void setBuyTypeUserId(int buyTypeUserId) {
    this.buyTypeUserId = buyTypeUserId;
  }

  public HashMap<String, OrderStatus> getOrder_status() {
    return order_status;
  }

  public void setOrder_status(HashMap<String, OrderStatus> order_status) {
    this.order_status = order_status;
  }

  public List<FoodOrder> getFood_order() {
    return food_order;
  }

  public void setFood_order(List<FoodOrder> food_order) {
    this.food_order = food_order;
  }

  public Location getDestination_location() {
    return destination_location;
  }

  public void setDestination_location(Location destination_location) {
    this.destination_location = destination_location;
  }

  public String getDestinationLocationName() {
    return destinationLocationName;
  }

  public void setDestinationLocationName(String destinationLocationName) {
    this.destinationLocationName = destinationLocationName;
  }

  public int getBillingCost() {
    return billingCost;
  }

  public void setBillingCost(int billingCost) {
    this.billingCost = billingCost;
  }

  public int getSenderTypeId() {
    return senderTypeId;
  }

  public void setSenderTypeId(int senderTypeId) {
    this.senderTypeId = senderTypeId;
  }

  public boolean isOrder_complete() {
    return order_complete;
  }

  public void setOrder_complete(boolean order_complete) {
    this.order_complete = order_complete;
  }

  public boolean isOrder_restaurant() {
    return order_restaurant;
  }

  public void setOrder_restaurant(boolean order_restaurant) {
    this.order_restaurant = order_restaurant;
  }

  @Override
  public String toString() {
    return "Order{" +
        "orderId=" + orderId +
        ", resId=" + resId +
        ", buyTypeUserId=" + buyTypeUserId +
        ", order_status=" + order_status +
        ", food_order=" + food_order +
        ", destination_location=" + destination_location +
        ", destinationLocationName='" + destinationLocationName + '\'' +
        ", billingCost=" + billingCost +
        ", senderTypeId=" + senderTypeId +
        ", order_complete=" + order_complete +
        ", order_restaurant=" + order_restaurant +
        '}';
  }

  public Order() {
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(this.orderId);
    dest.writeInt(this.resId);
    dest.writeInt(this.buyTypeUserId);
    dest.writeMap(this.order_status);
    dest.writeTypedList(this.food_order);
    dest.writeParcelable(this.destination_location, flags);
    dest.writeString(this.destinationLocationName);
    dest.writeInt(this.billingCost);
    dest.writeInt(this.senderTypeId);
    dest.writeByte(this.order_complete ? (byte) 1 : (byte) 0);
    dest.writeByte(this.order_restaurant ? (byte) 1 : (byte) 0);
  }

  protected Order(Parcel in) {
    this.orderId = in.readInt();
    this.resId = in.readInt();
    this.buyTypeUserId = in.readInt();
    this.order_status = in.readHashMap(OrderStatus.class.getClassLoader());
    this.food_order = in.createTypedArrayList(FoodOrder.CREATOR);
    this.destination_location = in.readParcelable(Location.class.getClassLoader());
    this.destinationLocationName = in.readString();
    this.billingCost = in.readInt();
    this.senderTypeId = in.readInt();
    this.order_complete = in.readByte() != 0;
    this.order_restaurant = in.readByte() != 0;
  }

  public static final Creator<Order> CREATOR = new Creator<Order>() {
    @Override
    public Order createFromParcel(Parcel source) {
      return new Order(source);
    }

    @Override
    public Order[] newArray(int size) {
      return new Order[size];
    }
  };
}
