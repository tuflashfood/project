package com.example.project.model.json;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PurchaseOrder implements Parcelable, Comparable<PurchaseOrder> {

  @SerializedName("order")
  private Order order ;
  @SerializedName("location")
  private Location location ;
  @SerializedName("res_name")
  private String res_name;
  @SerializedName("delivery_cost")
  private int delivery_cost;
  @SerializedName("phone")
  private String phone;

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public String getRes_name() {
    return res_name;
  }

  public void setRes_name(String res_name) {
    this.res_name = res_name;
  }

  public int getDelivery_cost() {
    return delivery_cost;
  }

  public void setDelivery_cost(int delivery_cost) {
    this.delivery_cost = delivery_cost;
  }

  @Override
  public String toString() {
    return "PurchaseOrder{" +
        "order=" + order +
        ", location=" + location +
        ", res_name='" + res_name + '\'' +
        ", delivery_cost=" + delivery_cost +
        ", phone='" + phone + '\'' +
        '}';
  }

  public PurchaseOrder() {
  }


  @Override
  public int compareTo(PurchaseOrder o) {
    Locale locale = new Locale("th") ;
    String time1 = this.order.getOrder_status().get("order").getTime() ;
    Log.i("TAG", "compareTo: " + time1);
    String time2 = o.getOrder().getOrder_status().get("order").getTime() ;
    Log.i("TAG", "compareTo: " + time2);

    return time1.compareTo(time2) ;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeParcelable(this.order, flags);
    dest.writeParcelable(this.location, flags);
    dest.writeString(this.res_name);
    dest.writeInt(this.delivery_cost);
    dest.writeString(this.phone);
  }

  protected PurchaseOrder(Parcel in) {
    this.order = in.readParcelable(Order.class.getClassLoader());
    this.location = in.readParcelable(Location.class.getClassLoader());
    this.res_name = in.readString();
    this.delivery_cost = in.readInt();
    this.phone = in.readString();
  }

  public static final Creator<PurchaseOrder> CREATOR = new Creator<PurchaseOrder>() {
    @Override
    public PurchaseOrder createFromParcel(Parcel source) {
      return new PurchaseOrder(source);
    }

    @Override
    public PurchaseOrder[] newArray(int size) {
      return new PurchaseOrder[size];
    }
  };
}
