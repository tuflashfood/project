package com.example.project.model.json;

import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderBuy implements Parcelable {
  static String TAG = "OrderBuy" ;
  private String res_id ;
  private String buy_type_user_id ;
  private List<HashMap<String,String>> menus ;
  private String destination_location = null;
  private String destination_location_name = null ;
  private String transportCost ;
  private String billingCost ;
//  private HashMap<String,String> listMenuOption ;
  public OrderBuy(){
    menus = new ArrayList<>();
  }

  public OrderBuy(String buy_type_user_id ,String res_id, List<HashMap<String, String>> menus) {
    this.res_id = res_id;
    this.buy_type_user_id = buy_type_user_id;
    this.menus = menus;
    this.transportCost = "0";
    this.billingCost = "0";
  }

  public void addMenu(HashMap<String,String> menu,String form){
    String newKey = menu.get("key");
    int index = findMenuByKey(newKey) ;
    int amount = 0 ;
    if(index != -1){
      if(form.equals("option"))
        amount = Integer.parseInt(menu.get("amount"));
      else
        amount = Integer.parseInt(menus.get(index).get("amount"))+ 1;
      int price = Integer.parseInt(menus.get(index).get("price")) * amount ;
      String product_notes = !menu.get("product_notes").equals("") ? menu.get("product_notes") : "";
      updateMenu(index,amount+"",price+"",product_notes);
      return;
    }
    menus.add(menu);
  }

  public void updateMenu(int index,String amount,String totalPrice,String productNotes){
    menus.get(index).put("amount" , amount);
    menus.get(index).put("total_price" , totalPrice);
    if(!productNotes.equals("null")){
      menus.get(index).put("product_notes",productNotes);
    }
  }

  public void addMenuOption(HashMap<String,String> menu,String keyOld){
    String newKey = menu.get("key");
    // menu update amount
    if(menu.get("key").equals(keyOld)){
      int index = findMenuByKey(newKey) ;
      String product_notes = !menu.get("product_notes").equals("") ? menu.get("product_notes") : "";
      updateMenu(index,menu.get("amount"),menu.get("price"),product_notes);
    }else{
      // new menu
      removeMenuByKey(keyOld);
      int index = findMenuByKey(newKey);
      // menu duplicate
      if(index != -1){
        int amount = Integer.parseInt(menus.get(index).get("amount"))+Integer.parseInt(menu.get("amount"));
        int price = Integer.parseInt(menus.get(index).get("price")) * amount ;
        String product_notes = menu.get("product_notes") != null ? menu.get("product_notes") : "null";
        updateMenu(index,amount+"",price+"",product_notes);
      }else {
        //new menu not duplicate
        menus.add(menu);
      }
    }
  }


  public void removeMenuByKey(String key){
    int index = findMenuByKey(key) ;
    if(index != -1)
      menus.remove(index);
  }

  public void changeAmountMenu(String key , String amount){
    int index = findMenuByKey(key) ;
    if(index != -1){
      int newAmount = Integer.parseInt(amount) + Integer.parseInt(menus.get(index).get("amount"));
      menus.get(index).put("amount" , String.valueOf(newAmount));
      menus.get(index).put("total_price" , String.valueOf(newAmount * Integer.parseInt(menus.get(index).get("price"))));
    }
  }

  public int findMenuByKey(String key){
    for (int i = 0 ; i < menus.size() ; i++){
      if(menus.get(i).get("key").equals(key))
        return i ;
    }
    return -1;
  }

  public void clearOrder(){
    this.res_id = null;
    this.transportCost = "0";
    this.billingCost = "0";
    this.menus.clear();
  }

  public int getAmountProduct(String menu_name){
    int amount = 0;
    for (int i = 0 ; i < menus.size(); i++){
      boolean value = menus.get(i).containsValue(menu_name);
      if(value)
        amount+= Integer.parseInt(menus.get(i).get("amount"));
    }
    if(amount == 0)
      return -1;
    else
      return amount ;
  }

  public int getTotalAmount(){
    int amount = 0 ;
    for (int i = 0 ; i < menus.size() ; i++){
      amount += Integer.parseInt(menus.get(i).get("amount"));
    }
    return amount;
  }



  public int getTotalPrice() {
    int price = 0 ;
    for (int i = 0 ; i < menus.size() ; i++){
      price += Integer.parseInt(menus.get(i).get("total_price"));
    }
    return price;
  }

  public String getRes_id() {
    return res_id;
  }

  public void setRes_id(String res_id) {
    this.res_id = res_id;
  }

  public String getBuy_type_user_id() {
    return buy_type_user_id;
  }

  public void setBuy_type_user_id(String buy_type_user_id) {
    this.buy_type_user_id = buy_type_user_id;
  }

  public List<HashMap<String,String>> getMenu() {
    return menus;
  }

  public void setMenu(List<HashMap<String,String>> menus) {
    this.menus = menus;
  }

  public String getDestination_location() {
    return destination_location;
  }

  public void setDestination_location(String destination_location) {
    this.destination_location = destination_location;
  }

  public String getTransportCost() {
    return transportCost;
  }

  public void setTransportCost(String transportCost) {
    this.transportCost = transportCost;
  }

  public String getBillingCost() {
    return billingCost;
  }

  public void setBillingCost(String billingCost) {
    this.billingCost = billingCost;
  }

  public String getDestination_location_name() {
    return destination_location_name;
  }

  public void setDestination_location_name(String destination_location_name) {
    this.destination_location_name = destination_location_name;
  }

  @Override
  public String toString() {
    return "OrderBuy{" +
        "res_id='" + res_id + '\'' +
        ", buy_type_user_id='" + buy_type_user_id + '\'' +
        ", menus=" + menus +
        ", destination_location='" + destination_location + '\'' +
        ", destination_location_name='" + destination_location_name + '\'' +
        ", transportCost='" + transportCost + '\'' +
        ", billingCost='" + billingCost + '\'' +
//        ", listMenuOption=" + listMenuOption +
        '}';
  }

  public  List<HashMap<String,String>> getMenuSendServer(){
    List<HashMap<String,String>> fixmenus = menus;
    for (int i = 0 ; i < fixmenus.size() ; i++){
      if(fixmenus.get(i).containsKey("option")) {
        fixmenus.get(i).put("key",fixmenus.get(i).get("key").replaceAll(">%<"," "));
        fixmenus.get(i).remove("menu_description");
        fixmenus.get(i).remove("option");
      }
    }
    return fixmenus;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.res_id);
    dest.writeString(this.buy_type_user_id);
    dest.writeList(this.menus);
    dest.writeString(this.destination_location);
    dest.writeString(this.destination_location_name);
    dest.writeString(this.transportCost);
    dest.writeString(this.billingCost);
//    dest.writeSerializable(this.listMenuOption);
  }

  protected OrderBuy(Parcel in) {
    this.res_id = in.readString();
    this.buy_type_user_id = in.readString();
    this.menus = new ArrayList<HashMap<String, String>>();
    in.readList(this.menus, HashMap.class.getClassLoader());
    this.destination_location = in.readString();
    this.destination_location_name = in.readString();
    this.transportCost = in.readString();
    this.billingCost = in.readString();
//    this.listMenuOption = (HashMap<String, String>) in.readSerializable();
  }

  public static final Creator<OrderBuy> CREATOR = new Creator<OrderBuy>() {
    @Override
    public OrderBuy createFromParcel(Parcel source) {
      return new OrderBuy(source);
    }

    @Override
    public OrderBuy[] newArray(int size) {
      return new OrderBuy[size];
    }
  };
}
