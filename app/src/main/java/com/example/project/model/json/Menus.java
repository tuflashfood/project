package com.example.project.model.json;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Menus implements Parcelable {

	@SerializedName("image")
	private String image;

	@SerializedName("detail")
	private String detail;

//	@SerializedName("time")
//	private String time;

	@SerializedName("category")
	private List<String> category;

	@SerializedName("menu")
	private HashMap<String,List<Menu>> menu;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setDetail(String detail){
		this.detail = detail;
	}

	public String getDetail(){
		return detail;
	}

//	public void setTime(String time){
//		this.time = time;
//	}
//
//	public String getTime(){
//		return time;
//	}

	public void setCategory(List<String> category){
		this.category = category;
	}

	public List<String> getCategory(){
		return category;
	}

	public HashMap<String, List<Menu>> getMenu() {
		return menu;
	}

	public void setMenu(HashMap<String, List<Menu>> menu) {
		this.menu = menu;
	}

	@Override
 	public String toString(){
		return 
			"Menus{" +
			"image = '" + image + '\'' +
			",detail = '" + detail + '\'' +
//			",time = '" + time + '\'' +
			",category = '" + category + '\'' +
			",menu = '" + menu + '\'' +
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.image);
//		dest.writeString(this.name);
		dest.writeString(this.detail);
//		dest.writeString(this.time);
		dest.writeStringList(this.category);
		dest.writeMap(this.menu);
	}

	public Menus() {
	}

	protected Menus(Parcel in) {
		this.image = in.readString();
		this.detail = in.readString();
//		this.time = in.readString();
		this.category = in.createStringArrayList();
		this.menu = (HashMap<String, List<Menu>>) in.readSerializable();
	}

	public static final Parcelable.Creator<Menus> CREATOR = new Parcelable.Creator<Menus>() {
		@Override
		public Menus createFromParcel(Parcel source) {
			return new Menus(source);
		}

		@Override
		public Menus[] newArray(int size) {
			return new Menus[size];
		}
	};
}