package com.example.project.model.json;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Menu implements Parcelable {

  @SerializedName("price")
  private String price;

  @SerializedName("name")
  private String name;

  @SerializedName("picture_small")
  private String pictureSmall;

  @SerializedName("description")
  private String description;

  @SerializedName("picture")
  private String picture;

  @SerializedName("option")
  private List<OptionItem> option ;

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPictureSmall() {
    return pictureSmall;
  }

  public void setPictureSmall(String pictureSmall) {
    this.pictureSmall = pictureSmall;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public List<OptionItem> getOption() {
    if(option == null)
      return null ;
    return option;
  }

  public void setOption(List<OptionItem> option) {
    this.option = option;
  }

  @Override
  public String toString() {
    return "Menu{" +
        "price='" + price + '\'' +
        ", name='" + name + '\'' +
        ", pictureSmall='" + pictureSmall + '\'' +
        ", description='" + description + '\'' +
        ", picture='" + picture + '\'' +
        ", option=" + option +
        '}';
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(this.price);
    dest.writeString(this.name);
    dest.writeString(this.pictureSmall);
    dest.writeString(this.description);
    dest.writeString(this.picture);
    dest.writeList(this.option);
  }

  public Menu() {
  }

  protected Menu(Parcel in) {
    this.price = in.readString();
    this.name = in.readString();
    this.pictureSmall = in.readString();
    this.description = in.readString();
    this.picture = in.readString();
    this.option = new ArrayList<OptionItem>();
    in.readList(this.option, OptionItem.class.getClassLoader());
  }

  public static final Parcelable.Creator<Menu> CREATOR = new Parcelable.Creator<Menu>() {
    @Override
    public Menu createFromParcel(Parcel source) {
      return new Menu(source);
    }

    @Override
    public Menu[] newArray(int size) {
      return new Menu[size];
    }
  };
}