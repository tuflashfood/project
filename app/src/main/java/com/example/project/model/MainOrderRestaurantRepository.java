package com.example.project.model;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.project.model.json.Order;
import com.example.project.model.json.PurchaseOrder;
import com.example.project.model.json.Restaurant;
import com.example.project.service.HttpManager;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainOrderRestaurantRepository {
  private static final String TAG = "MainOrderRestaurantRepository" ;
  private MutableLiveData<List<PurchaseOrder>> listOrderSuccess = new MutableLiveData<>();
  private MutableLiveData<List<PurchaseOrder>> listOrderNotSuccess = new MutableLiveData<>();
  private MutableLiveData<Restaurant> restaurant = new MutableLiveData<>();
  private Call<List<PurchaseOrder>> listCallOrderSuccess ;
  private Call<List<PurchaseOrder>> listCallOrderNotSuccess ;
  private Call<Restaurant> restaurantCall ;

  public MutableLiveData<Restaurant> getFetchDataRestaurant(String user_id){
    fetchDataRestaurant(user_id);
    return restaurant ;
  }

  private void fetchDataRestaurant(String user_id){
    restaurantCall = HttpManager.getInstance().getService().getDataRestaurantByUserId(user_id) ;
    restaurantCall.enqueue(new Callback<Restaurant>() {
      @Override
      public void onResponse(Call<Restaurant> call, Response<Restaurant> response) {
        if(response.isSuccessful()){
          restaurant.setValue(response.body());
        }else {
          try {
            Log.e(TAG, "onResponse: " + response.errorBody().string());
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }

      @Override
      public void onFailure(Call<Restaurant> call, Throwable t) {
        Log.e(TAG, "onFailure: " + t.toString());
      }
    });
  }

  public MutableLiveData<Restaurant> getRestaurant() {
    return restaurant;
  }

  public void setListOrderSuccess(List<PurchaseOrder> listOrderSuccess) {
    this.listOrderSuccess.setValue(listOrderSuccess);
  }

  public MutableLiveData<List<PurchaseOrder>> getDataOrderSuccess(String res_id) {
    fetchDataOrderSuccess(res_id) ;
    return listOrderSuccess ;
  }

  private void fetchDataOrderSuccess(String res_id) {
    listCallOrderSuccess = HttpManager.getInstance().getService().getOrderByIdResAndOrderSuccess(res_id) ;
    listCallOrderSuccess.enqueue(new Callback<List<PurchaseOrder>>() {
      @Override
      public void onResponse(Call<List<PurchaseOrder>> call, Response<List<PurchaseOrder>> response) {
        if (response.isSuccessful()) {
          listOrderSuccess.setValue(response.body());
        }else {
          try {
            Log.e(TAG, "onResponse: " + response.errorBody().string());
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }

      @Override
      public void onFailure(Call<List<PurchaseOrder>> call, Throwable t) {
        Log.e(TAG, "onFailure: " + t.toString());
      }
    });
  }

  public MutableLiveData<List<PurchaseOrder>> getDataOrderNotSuccess(String res_id) {
    fetchDataOrderNotSuccess(res_id) ;
    return listOrderNotSuccess ;
  }

  private void fetchDataOrderNotSuccess(String res_id) {
    listCallOrderNotSuccess = HttpManager.getInstance().getService().getOrderByIdResAndNotOrderSuccess(res_id) ;
    listCallOrderNotSuccess.enqueue(new Callback<List<PurchaseOrder>>() {
      @Override
      public void onResponse(Call<List<PurchaseOrder>> call, Response<List<PurchaseOrder>> response) {
        if (response.isSuccessful()) {
          listOrderNotSuccess.setValue(response.body());
        }else {
          try {
            listOrderNotSuccess.setValue(null);
            Log.e(TAG, "onResponse: " + response.errorBody().string());
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }

      @Override
      public void onFailure(Call<List<PurchaseOrder>> call, Throwable t) {
        Log.e(TAG, "onFailure: " + t.toString());
      }
    });
  }
}
