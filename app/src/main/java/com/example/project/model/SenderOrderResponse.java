package com.example.project.model;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.project.model.json.PurchaseOrder;
import com.example.project.model.json.ResponseBody;
import com.example.project.service.HttpManager;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SenderOrderResponse {
  private static final String TAG = "SenderOrderResponse";
  private Call<List<PurchaseOrder>> purchaseOrder;
  private Call<ResponseBody> callCheckQueueSender;
  private Call<ResponseBody> callStatusSender;
  private MutableLiveData<ResponseBody> statusSender = new MutableLiveData<>();
  private MutableLiveData<ResponseBody> checkQueueSender = new MutableLiveData<>();
  private MutableLiveData<List<PurchaseOrder>> purchaseOrderList = new MutableLiveData<>();
  public MutableLiveData<List<PurchaseOrder>> getPurchaseOrderList() {
    return purchaseOrderList;
  }

  public MutableLiveData<List<PurchaseOrder>> getDataPurchaseOrder(String orderSender) {
    fetchDataPurchaseOrder(orderSender);
    return purchaseOrderList;
  }

  public void fetchDataPurchaseOrder(String orderSender) {
    purchaseOrder = HttpManager.getInstance().getService().getDataOrder(orderSender);
    purchaseOrder.enqueue(new Callback<List<PurchaseOrder>>() {
      @Override
      public void onResponse(Call<List<PurchaseOrder>> call, Response<List<PurchaseOrder>> response) {
        Log.i(TAG, "onResponse: " + response);
        if (response.isSuccessful()) {
          purchaseOrderList.setValue(response.body());
        } else {
          if(response.code() == 404){
            purchaseOrderList.setValue(new ArrayList<>());
          }
        }
      }

      @Override
      public void onFailure(Call<List<PurchaseOrder>> call, Throwable t) {
        Log.e(TAG, "onFailure: " + t.toString());
      }
    });
  }

  public MutableLiveData<ResponseBody> getCheckQueueSender() {
    return checkQueueSender;
  }

  public MutableLiveData<ResponseBody> getFetchCheckQueueSender(String sender_id) {
    fetchCheckQueueSender(sender_id);
    return checkQueueSender;
  }

  private void fetchCheckQueueSender(String sender_id) {
    callCheckQueueSender = HttpManager.getInstance().getService().checkQueueSender(sender_id);
    callCheckQueueSender.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if (response.isSuccessful()) {
          checkQueueSender.setValue(response.body());
        } else {
          Log.i(TAG, "onResponse: " + response.errorBody().toString());
        }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e(TAG, "onFailure: " + t.toString());
      }
    });
  }

  public Call<ResponseBody> getCallStatusSender() {
    return callStatusSender;
  }

  public MutableLiveData<ResponseBody> getSendStatusSender(String sender_id, String Status) {
    sendStatusSender(sender_id, Status);
    return statusSender;
  }

  private void sendStatusSender(String sender_id, String status) {
    callStatusSender = HttpManager.getInstance().getService().sendStatusSender(sender_id, status);
    callStatusSender.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if (response.isSuccessful()) {
          statusSender.setValue(response.body());
        } else {
          Gson gson = new Gson() ;
          try {
            statusSender.setValue(gson.fromJson(response.errorBody().string(),ResponseBody.class));
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e(TAG, "onFailure: " + t.toString());
      }
    });
  }
}
