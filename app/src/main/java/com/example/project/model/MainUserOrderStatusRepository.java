package com.example.project.model;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.project.model.json.PurchaseOrder;
import com.example.project.service.HttpManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainUserOrderStatusRepository {
  private static final String TAG = "UserOrderStatusRepository" ;
  private MutableLiveData<List<PurchaseOrder>> responsePurchaseOrderByIdUser = new MutableLiveData<>() ;
  private Call<List<PurchaseOrder>> callGetOrderByIdUser ;

  public MutableLiveData<List<PurchaseOrder>> getResponsePurchaseOrderByIdUser() {
    return responsePurchaseOrderByIdUser;
  }

  public MutableLiveData<List<PurchaseOrder>> getFetchGetOrderById(String user_id){
    fetchGetOrderById(user_id);
    return responsePurchaseOrderByIdUser ;
  }

  private void fetchGetOrderById(String user_id){
    callGetOrderByIdUser = HttpManager.getInstance().getService().getOrderByIdUser(user_id) ;
    callGetOrderByIdUser.enqueue(new Callback<List<PurchaseOrder>>() {
      @Override
      public void onResponse(Call<List<PurchaseOrder>> call, Response<List<PurchaseOrder>> response) {
        if(response.isSuccessful()){
          responsePurchaseOrderByIdUser.setValue(response.body());
        }else {
          responsePurchaseOrderByIdUser.setValue(null);
        }
      }

      @Override
      public void onFailure(Call<List<PurchaseOrder>> call, Throwable t) {
        Log.e(TAG, "onFailure: " + t.toString());
      }
    });
  }
}
