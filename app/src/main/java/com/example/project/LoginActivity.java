package com.example.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.project.interfaces.Callbacklogin;
import com.example.project.model.json.PurchaseOrder;
import com.example.project.model.json.User;
import com.example.project.ui.login.LoginFragment;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.parceler.Parcels;


public class LoginActivity extends AppCompatActivity implements Callbacklogin {

  private static final int REQUEST_CODE_LOCATION = 1;
  private static final String TAG = "Login" ;
  private User user ;
  private PurchaseOrder purchaseOrder ;
  private String time ;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    String s = FirebaseInstanceId.getInstance().getToken();
    Log.i(TAG, "onCreate: " + s);
//    LocalBroadcastManager.getInstance(this).registerReceiver(mHandler,new IntentFilter("messageFormFCM"));
    onNewIntent(getIntent()) ;
    //
    if (ContextCompat.checkSelfPermission(LoginActivity.this,
        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
      ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION);
    }else {
      // allow permission ACCESS_FINE_LOCATION
      Log.i(TAG, "onCreate: " + "allow permission");
    }
    if(checkUserLogin()){
      intentByTypeUser(user.getType());
    }else {
      if (savedInstanceState == null) {
        getSupportFragmentManager().beginTransaction()
            .add(R.id.contentContainer, LoginFragment.newInstance())
            .commit();
      }
    }
  }


  @Override
  protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
    //form background
    Bundle extras = intent.getExtras() ;
    if(extras != null) {
      for (String key : extras.keySet()){
        Log.i(TAG, "onNewIntent: eee " + key);
      }
    }else {
      Log.i(TAG, "onNewIntent: asdf");
    }
    if (extras != null ) {
      if (extras.containsKey( "data" )) {
        String msg = extras.getString( "data" ) ;
        setPurchaseOrder(msg);
        Log.i(TAG, "onNewIntent: " + msg);
      }
      if(extras.containsKey("time")){
        time = extras.getString( "time" ) ;
        Log.i(TAG, "onNewIntent: " +time);
      }
    }

  }

  public void setPurchaseOrder(String msg) {
    Gson gson = new Gson();
    gson.fromJson(msg,PurchaseOrder.class) ;
    this.purchaseOrder = gson.fromJson(msg, PurchaseOrder.class);
    Log.i(TAG, "setPurchaseOrder: " + this.purchaseOrder);
  }


  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if(requestCode == REQUEST_CODE_LOCATION){
      //require pass = 0 / nopass -1
      if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
        if (ContextCompat.checkSelfPermission(LoginActivity.this,
            Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED){
          Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show();
        }
      }else {
        Toast.makeText(this,"Please enable gps",Toast.LENGTH_LONG).show();
      }
    }
  }

  public void intentByTypeUser(String type){
    switch (type){
      case "restaurant":
        Intent intentRestaurant = new Intent(this, RestaurantActivity.class);
        intentRestaurant.putExtra("user", Parcels.wrap(user));
        if (purchaseOrder != null) {
          intentRestaurant.putExtra("data", Parcels.wrap(purchaseOrder));
        }
        startActivity(intentRestaurant);
        finish();
        break;
      case "user":
        Log.i(TAG, "intentByTypeUser: token " + user.getFirebase_token());
        Intent intentUser = new Intent(this, MainActivity.class);
        intentUser.putExtra("user", Parcels.wrap(user));
        startActivity(intentUser);
        finish();
        break;
      case "sender":
        Intent intentSernder = new Intent(this, SenderActivity.class);
        Log.i(TAG, "intentByTypeUser: " + user);
        if (purchaseOrder != null) {
          intentSernder.putExtra("time", time);
          intentSernder.putExtra("data", Parcels.wrap(purchaseOrder));
        }
        intentSernder.putExtra("user", Parcels.wrap(user));
        startActivity(intentSernder);
        finish();
        break;
    }
  }

  public void replaceFragment(Fragment fragment) {
    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
    transaction.add(R.id.contentContainer, fragment);
    transaction.addToBackStack(null);
    transaction.commit();
  }


  public Boolean checkUserLogin(){
    SharedPreferences shared_pref = getSharedPreferences("MyPrefsFile", Context.MODE_PRIVATE);
    String textUser = shared_pref.getString("user", "No Pref");
    Log.i(TAG, "checkUserLogin: "+ textUser);
    if(!textUser.equals("No Pref")){
      Gson gson = new Gson();
      user = gson.fromJson(textUser,User.class);
      Log.i(TAG, "checkUserLogin: " + user);
      return true ;
    }
    return false ;
  }


  @Override
  public void changeFragment(Fragment fragment) {
    replaceFragment(fragment);
  }

}
