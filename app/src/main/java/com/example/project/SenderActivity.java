package com.example.project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.project.interfaces.TalkMainActivity;
import com.example.project.model.json.PurchaseOrder;
import com.example.project.model.json.User;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.parceler.Parcels;

public class SenderActivity extends AppCompatActivity implements TalkMainActivity {
  private static final String ARG_BUNDLE_USER = "ARG_BUNDLE_USER" ;
  private static final String TAG = "SenderActivity" ;
  private AppBarConfiguration mAppBarConfiguration;
  private Toolbar toolbar ;
  private DrawerLayout drawer ;
  private User user ;
  private PurchaseOrder purchaseOrder ;
  private PurchaseOrder newPurchaseOrder ;
  private String time ;
  private TextView nav_header_title ;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    String s = FirebaseInstanceId.getInstance().getToken();
    Log.i(TAG, "onCreate: " + s);
    toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getExtraFromLogin();
    LinearLayout linear_logout = (LinearLayout) findViewById(R.id.linear_logout);
    linear_logout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        SharedPreferences shared_pref = getSharedPreferences("MyPrefsFile", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared_pref.edit();
        editor.remove("user");
        editor.commit();
        Intent intent = new Intent(SenderActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
      }
    });
    drawer = findViewById(R.id.drawer_layout);
    NavigationView navigationView = findViewById(R.id.nav_view);
    View hView =  navigationView.getHeaderView(0);
    nav_header_title = (TextView)hView.findViewById(R.id.nav_header_title);
    if(user != null) nav_header_title.setText("คุณ " + user.getName());
    navigationView.inflateMenu(R.menu.activity_main_drawer_sender);
    //fragment show
    mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.nav_sender)
        .setDrawerLayout(drawer)
        .build();
    NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
    Bundle bundle = new Bundle();
    bundle.putParcelable(ARG_BUNDLE_USER, Parcels.wrap(user));
    if(newPurchaseOrder != null){
      bundle.putParcelable("data", Parcels.wrap(newPurchaseOrder));
      bundle.putString("time", time);
    }
    navController.setGraph(R.navigation.sender_navigation,bundle);
    NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
    NavigationUI.setupWithNavController(navigationView, navController);
  }
  public void getExtraFromLogin(){
    Bundle extras = getIntent().getExtras();
    if(extras != null){
      if(extras.containsKey("user"))
        user = Parcels.unwrap(getIntent().getParcelableExtra("user"));
      if(extras.containsKey("data"))
        newPurchaseOrder = Parcels.unwrap(getIntent().getParcelableExtra("data"));
      if(extras.containsKey("time"))
        time = getIntent().getStringExtra("time");
    }
//    for (String key : extras.keySet()){
//      Log.i(TAG, "initDataUserFromBundle: " + key);
//    }
  }

//  @Override
//  protected void onNewIntent(Intent intent) {
//    super.onNewIntent(intent);
//    //form background
//    Bundle extras = intent.getExtras() ;
//    if (extras != null ) {
//      if(extras.containsKey( "dataFromForeGround" )) return ;
//      if (extras.containsKey("data")) {
//        String msg = extras.getString("data") ;
//        setPurchaseOrder(msg);
//        Log.i(TAG, "onNewIntent: " + this.purchaseOrder);
//      }
//      if(extras.containsKey("time")) {
//        time = extras.getString("time") ;
//      }
//    }
//
//  }

  public void setPurchaseOrder(String purchaseOrder){
    Gson gson = new Gson();
    this.purchaseOrder = gson.fromJson(purchaseOrder, PurchaseOrder.class);
  }

  @Override
  protected void onPause() {
    super.onPause();
  }

  @Override
  public boolean onSupportNavigateUp() {
    NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
    return NavigationUI.navigateUp(navController, mAppBarConfiguration)
        || super.onSupportNavigateUp();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override
  public void setDisplayToolBar(Boolean bool) {
    int display = bool ? View.VISIBLE:View.GONE;
    toolbar.setVisibility(display);
  }

  public void setDisplayDrawerLayout(Boolean bool){
    int display = bool ? DrawerLayout.LOCK_MODE_UNLOCKED:DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
    drawer.setDrawerLockMode(display);
  }

}
