package com.example.project.interfaces;

import com.example.project.model.json.Order;
import com.example.project.model.json.ResponseBody;
import com.example.project.model.json.PurchaseOrder;
import com.example.project.model.json.Restaurant;
import com.example.project.model.json.User;


import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Api {
  //restaurant
  @GET("api/restaurant/open")
  Call<List<Restaurant>> getDataRestaurantOpen();

  @GET("api/restaurant/getdatauserid/{user_id}")
  Call<Restaurant> getDataRestaurantByUserId(@Path("user_id") String user_id);

  @FormUrlEncoded
  @POST("api/restaurant/searchsender")
  Call<ResponseBody> sendSearchSender(@Field("order_id") String order_id);

  //order
  @FormUrlEncoded
  @POST("api/order/updatestatusorder")
  Call<ResponseBody> sendUpdateStatusOrder(@Field("order_id") String order_id,@Field("status") String status);

  @FormUrlEncoded
  @POST("api/order/createorder")
  Call<ResponseBody> sendOrder(@Field("res_id") String res_id, @Field("buy_type_user_id") String buy_type_user_id, @Field("food_order") List<HashMap<String,String>>food_order,
       @Field("billing_cost") String billingCost ,@Field("destination_location") String destination_location ,@Field("destination_location_name") String destination_location_name ,@Field("time") String time);

  @GET("api/order/getorderbyidsender/{sender_id}")
  Call<List<PurchaseOrder>> getDataOrder(@Path("sender_id") String order_id);

  @GET("api/order/checkorderrestaurantordersuccess/{order_id}")
  Call<ResponseBody> checkOrderRestaurantOrderSuccess(@Path("order_id") String order_id);

  @GET("api/order/getorderbyidresandordersuccess/{res_id}")
  Call<List<PurchaseOrder>> getOrderByIdResAndOrderSuccess(@Path("res_id") String res_id);

  @FormUrlEncoded
  @POST("api/order/updateorderrestaurant")
  Call<ResponseBody> sendUpdateOrderRestaurant(@Field("order_id") String order_id ,@Field("value") String value);

  @FormUrlEncoded
  @POST("api/order/updateordercomplete")
  Call<ResponseBody> sendUpdateOrderComplete(@Field("order_id") String order_id ,@Field("value") String value);

  @GET("api/order/getorderbyidresandnotordersuccess/{res_id}")
  Call<List<PurchaseOrder>> getOrderByIdResAndNotOrderSuccess(@Path("res_id") String res_id);

  @GET("api/order/removeorderrestaurant/{order_id}")
  Call<ResponseBody> getRemoveOrderSender(@Path("order_id") String order_id);

  @FormUrlEncoded
  @POST("api/order/updatesenderidorder")
  Call<ResponseBody> sendUpdateSenderIdOrder(@Field("order_id") String order_id,@Field("sender_id") String sender_id);

  @GET("api/order/getorderbyiduser/{user_id}")
  Call<List<PurchaseOrder>> getOrderByIdUser(@Path("user_id") String user_id);

  //user
  @FormUrlEncoded
  @POST("api/user/addtokenfibase")
  Call<ResponseBody> sendNewToken(@Field("token") String token,@Field("user_id") String user_id);

  @FormUrlEncoded
  @POST("api/user/login")
  Call<User> login(@Field("username") String username,@Field("password") String password);

  @FormUrlEncoded
  @POST("api/user/createuser")
  Call<ResponseBody> createuser(@Field("name") String name,@Field("username") String username,@Field("password") String password,@Field("phone") String phone);

  @FormUrlEncoded
  @POST("api/user/currentLocation")
  Call<ResponseBody> getCurrentLocation(@Field("latlong") String latlong );

  //sender
  @GET("api/sender/checkqueue/{sender_id}")
  Call<ResponseBody> checkQueueSender(@Path("sender_id") String sender_id);

  @FormUrlEncoded
  @POST("api/sender/statussender")
  Call<ResponseBody> sendStatusSender(@Field("sender_id") String sender_id,@Field("status") String status);

  @FormUrlEncoded
  @POST("api/sender/answertosend")
  Call<ResponseBody> sendAnswerToSender(@Field("sender_id") String sender_id,@Field("order_id") String order_id, @Field("value") String value);

}
