package com.example.project.interfaces;

import com.example.project.model.json.OrderBuy;
import com.example.project.model.json.User;

public interface TalkMainActivity {
  public void setDisplayToolBar(Boolean bool) ;
  public void setDisplayDrawerLayout(Boolean bool) ;

}
