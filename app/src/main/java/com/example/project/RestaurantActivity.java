package com.example.project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.project.interfaces.TalkMainActivity;
import com.example.project.model.json.PurchaseOrder;
import com.example.project.model.json.User;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;

import org.parceler.Parcels;

public class RestaurantActivity extends AppCompatActivity implements TalkMainActivity {
  private static final String ARG_BUNDLE_USER = "ARG_BUNDLE_USER" ;
  private static final String TAG = "RestaurantActivity " ;
  private AppBarConfiguration mAppBarConfiguration;
  private Toolbar toolbar ;
  private DrawerLayout drawer ;
  private User user ;
  private PurchaseOrder newPurchaseOrder ;
  private String time ;
  private TextView nav_header_title ;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getExtraFromLogin();
    String s = FirebaseInstanceId.getInstance().getToken();
    Log.i(TAG, "onCreate: " + s);
    LinearLayout linear_logout = (LinearLayout) findViewById(R.id.linear_logout);
    linear_logout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        SharedPreferences shared_pref = getSharedPreferences("MyPrefsFile", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared_pref.edit();
        editor.remove("user");
        editor.commit();
        Intent intent = new Intent(RestaurantActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
      }
    });

    drawer = findViewById(R.id.drawer_layout);
    NavigationView navigationView = findViewById(R.id.nav_view);
    View hView =  navigationView.getHeaderView(0);
    nav_header_title = (TextView)hView.findViewById(R.id.nav_header_title);
    if(user != null) nav_header_title.setText("คุณ " + user.getName());
    navigationView.inflateMenu(R.menu.activity_main_drawer_restaurant);
    //fragment show
    mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.nav_main_restaurant)
        .setDrawerLayout(drawer)
        .build();
    NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
    Bundle bundle = new Bundle();
    if(newPurchaseOrder != null){
      bundle.putParcelable("data", Parcels.wrap(newPurchaseOrder));
    }
    bundle.putParcelable(ARG_BUNDLE_USER, Parcels.wrap(user));
    navController.setGraph(R.navigation.restaurant_navigation,bundle);
    NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
    NavigationUI.setupWithNavController(navigationView, navController);
  }

  @Override
  public void setDisplayToolBar(Boolean bool) {
    int display = bool ? View.VISIBLE:View.GONE;
    toolbar.setVisibility(display);
  }

  @Override
  public void setDisplayDrawerLayout(Boolean bool) {
    int display = bool ? DrawerLayout.LOCK_MODE_UNLOCKED:DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
    drawer.setDrawerLockMode(display);
  }

  @Override
  public boolean onSupportNavigateUp() {
    NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
    return NavigationUI.navigateUp(navController, mAppBarConfiguration)
        || super.onSupportNavigateUp();
  }

  public void getExtraFromLogin(){
    Bundle extras = getIntent().getExtras();
    if(extras != null)
      user = Parcels.unwrap(getIntent().getParcelableExtra("user"));
    if(extras.containsKey("data"))
      newPurchaseOrder = Parcels.unwrap(getIntent().getParcelableExtra("data"));
    if(extras.containsKey("time"))
      time = getIntent().getStringExtra("time");
  }
}
