package com.example.project.ui.login;

import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.MainActivity;
import com.example.project.R;
import com.example.project.RestaurantActivity;
import com.example.project.SenderActivity;
import com.example.project.interfaces.Callbacklogin;
import com.example.project.model.json.ResponseBody;
import com.example.project.model.json.Restaurant;
import com.example.project.model.json.User;
import com.example.project.service.HttpManager;
import com.example.project.ui.register.RegisterFragment;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import org.parceler.Parcels;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends Fragment implements View.OnClickListener {

  private LoginViewModel mViewModel;
  private EditText username ;
  private EditText password ;
  private Button textView_goto_signUp ;
  private Button button ;
  private Call<User> userCall;
  private User user = new User();
  private static final String TAG = "LoginFragment";
  private Callbacklogin callbacklogin ;
  private String token ;
  public static LoginFragment newInstance() {
    return new LoginFragment();
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    try {
      callbacklogin = (Callbacklogin) context;
    } catch (ClassCastException e) {
      throw new ClassCastException(context.toString() + " must implement Callback");
    }
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {

    final View view = inflater.inflate(R.layout.login_fragments, container, false);
    FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( getActivity(),  new OnSuccessListener<InstanceIdResult>() {
      @Override
      public void onSuccess(InstanceIdResult instanceIdResult) {
        token = instanceIdResult.getToken();
        Log.i(TAG, "onCreateView: " + token);
      }
    });
//    token = FirebaseInstanceId.getInstance().getToken();
    username = view.findViewById(R.id.username);
    password = view.findViewById(R.id.password);
    button = view.findViewById(R.id.sign_in);
    textView_goto_signUp = view.findViewById(R.id.textView_goto_signUp);
    button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        checkLogin(username.getText().toString(),password.getText().toString());
      }
    });
    textView_goto_signUp.setOnClickListener(this);
    return view ;
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
    // TODO: Use the ViewModel
  }

  public void addUsertoPreferences(){
    Gson gson = new Gson();
    SharedPreferences shared_pref = this.getActivity().getSharedPreferences("MyPrefsFile", Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = shared_pref.edit();
    editor.putString("user", gson.toJson(user));
    editor.commit();
  }

  public void sendTokenToServer(){
    Call<ResponseBody> callOrder = HttpManager.getInstance().getService().sendNewToken(token,user.getUser_id());
    callOrder.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        Log.i(TAG, "onResponse: " + response.body());
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Toast.makeText(getContext(),t.toString(),Toast.LENGTH_SHORT).show();
        Log.i(TAG, t.toString());
      }

    });
  }

  public void intentByTypeUser(String type){
    if(user.getFirebase_token() == null || !user.getFirebase_token().equals(token)){
      sendTokenToServer();
    }
    switch (type){
      case "restaurant":
        Intent intentRestaurant = new Intent(getActivity(), RestaurantActivity.class);
        intentRestaurant.putExtra("user", Parcels.wrap(user));
        startActivity(intentRestaurant);
        getActivity().finish();
        break;
      case "user":
        Intent intentUser = new Intent(getActivity(), MainActivity.class);
        intentUser.putExtra("user", Parcels.wrap(user));
        startActivity(intentUser);
        getActivity().finish();
        break;
      case "sender":
        Intent intentSernder = new Intent(getActivity(), SenderActivity.class);
        intentSernder.putExtra("user", Parcels.wrap(user));
        startActivity(intentSernder);
        getActivity().finish();
        break;
    }
  }

  private void checkLogin(String username,String password ){
    if(username.equals("") || password.equals("")) {
      Toast.makeText(getContext(),"กรอกข้อมูลไม่ครบ",Toast.LENGTH_SHORT).show();
      return ;
    }

    if(password.length() < 6) {
      Toast.makeText(getContext(),"รหัสน้อยกว่า 6 ตัว",Toast.LENGTH_SHORT).show();
      return ;
    }
    userCall = HttpManager.getInstance().getService().login(username,password);
    userCall.enqueue(new Callback<User>() {
      @Override
      public void onResponse(Call<User> call, Response<User> response) {
        Gson gsonUser = new Gson();
        if (response.isSuccessful()) {
          user = response.body();
          if(user.getType() != null){
            addUsertoPreferences();
            intentByTypeUser(user.getType());
          }
          Log.i(TAG, response.body() + "");
          Log.i(TAG, "onResponse: " + "success");
        } else {
          Log.i(TAG, "onResponse: " + "fail");
          try {
            user = gsonUser.fromJson(response.errorBody().string(), User.class);
            if(user.getErrors() != null)
              Toast.makeText(getContext(),user.getErrors(),Toast.LENGTH_SHORT).show();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }

      @Override
      public void onFailure(Call<User> call, Throwable t) {
        Log.i(TAG, "onFailure: " + t.toString());
        if(t instanceof IOException) {
          Toast.makeText(getContext(),"internet not connection",Toast.LENGTH_LONG).show();
        }else {
          Toast.makeText(getContext(),"error check login",Toast.LENGTH_LONG).show();
        }
      }
    });
  }

  @Override
  public void onClick(View v) {
    Fragment fragment;
    switch (v.getId()) {
      case R.id.textView_goto_signUp:
        fragment = RegisterFragment.newInstance();
        callbacklogin.changeFragment(fragment);
        break ;
    }
  }
}
