package com.example.project.ui.main_order_restaurant;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.project.model.MainOrderRestaurantRepository;
import com.example.project.model.json.Order;
import com.example.project.model.json.PurchaseOrder;
import com.example.project.model.json.Restaurant;
import com.example.project.model.json.User;

import java.util.List;

public class MainOrderRestaurantViewModel extends ViewModel {
  // TODO: Implement the ViewModel
  private MutableLiveData<Integer> numberMutableLiveData = new MutableLiveData<>() ;
  private MutableLiveData<User> userMutableLiveData = new MutableLiveData<>() ;
  private MainOrderRestaurantRepository mainOrderRestaurantRepository = new MainOrderRestaurantRepository();
  private MutableLiveData<List<PurchaseOrder>> listOrderSuccess = new MutableLiveData<>();
  private MutableLiveData<List<PurchaseOrder>> listOrderNotSuccess = new MutableLiveData<>();
  private MutableLiveData<List<PurchaseOrder>> newOrder = new MutableLiveData<>() ;
  private MutableLiveData<List<PurchaseOrder>> agreeOrder = new MutableLiveData<>() ;
  private MutableLiveData<Boolean> setPopBackStackDetailOrderFragement = new MutableLiveData<>();
  private MutableLiveData<Restaurant> restaurant = new MutableLiveData<>();

  public MutableLiveData<Restaurant> getFetchDataRestaurant(String user_id){
    restaurant = mainOrderRestaurantRepository.getFetchDataRestaurant(user_id);
    return restaurant ;
  }

  public void setSetPopBackStackDetailOrderFragement(Boolean setPopBackStackDetailOrderFragement) {
    this.setPopBackStackDetailOrderFragement.setValue(setPopBackStackDetailOrderFragement);
  }

  public MutableLiveData<Boolean> getSetPopBackStackDetailOrderFragement() {
    return setPopBackStackDetailOrderFragement;
  }

  public void setAgreeOrder(List<PurchaseOrder> agreeOrder) {
    this.agreeOrder.setValue(agreeOrder);
  }

  public MutableLiveData<List<PurchaseOrder>> getAgreeOrder() {
    return agreeOrder;
  }

  public void setNewOrder(List<PurchaseOrder> newOrder) {
    this.newOrder.setValue(newOrder);
  }

  public MutableLiveData<List<PurchaseOrder>> getNewOrder() {
    return newOrder;
  }

  public void setListOrderNotSuccess(List<PurchaseOrder> listOrderNotSuccess) {
    this.listOrderNotSuccess.setValue(listOrderNotSuccess);
  }

  public MutableLiveData<List<PurchaseOrder>> getListOrderNotSuccess() {
    return listOrderNotSuccess;
  }

  public void setListOrderSuccess(List<PurchaseOrder> listOrderSuccess) {
    this.listOrderSuccess.setValue(listOrderSuccess);
  }

  public MutableLiveData<List<PurchaseOrder>> getListOrderSuccess() {
    return listOrderSuccess;
  }

  public MutableLiveData<List<PurchaseOrder>> getDataOrderSuccess(String res_id) {
    listOrderSuccess = mainOrderRestaurantRepository.getDataOrderSuccess(res_id) ;
    return listOrderSuccess ;
  }

  public MutableLiveData<List<PurchaseOrder>> getDataOrderNotSuccess(String res_id) {
    return mainOrderRestaurantRepository.getDataOrderNotSuccess(res_id) ;
//    return listOrderNotSuccess ;
  }

  public MutableLiveData<User> getUserMutableLiveData() {
    return userMutableLiveData;
  }

  public void setUserMutableLiveData(User userMutableLiveData) {
    this.userMutableLiveData.setValue(userMutableLiveData);
  }

  public MutableLiveData<Integer> getNumberMutableLiveData() {
    return numberMutableLiveData;
  }

  public void setNumberMutableLiveData(Integer numberMutableLiveData) {
    this.numberMutableLiveData.setValue(numberMutableLiveData);
  }
}
