package com.example.project.ui.main_statusorder;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.project.model.MainUserOrderStatusRepository;
import com.example.project.model.json.PurchaseOrder;

import java.util.List;

public class MainUserStatusOrderViewModel extends ViewModel {
  // TODO: Implement the ViewModel
  private MainUserOrderStatusRepository mainUserOrderStatusRepository = new MainUserOrderStatusRepository();
  private MutableLiveData<List<PurchaseOrder>> statusorder = new MutableLiveData<>() ;

  public MutableLiveData<List<PurchaseOrder>> getStatusOrder(String user_id){
    statusorder = mainUserOrderStatusRepository.getFetchGetOrderById(user_id);
    return statusorder ;
  }
}
