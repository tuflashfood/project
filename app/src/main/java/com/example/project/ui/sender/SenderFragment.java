package com.example.project.ui.sender;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.widget.SwitchCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.adapter.SenderMainAdapter;
import com.example.project.interfaces.TalkMainActivity;
import com.example.project.model.json.OrderStatus;
import com.example.project.model.json.PurchaseOrder;
import com.example.project.model.json.ResponseBody;
import com.example.project.model.json.User;
import com.example.project.ui.detail_order.DetailOrderViewModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.parceler.Parcels;

import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

public class SenderFragment extends Fragment {
  private SwipeRefreshLayout swipeRefreshLayout;
  private SenderViewModel mViewModel;
  private DetailOrderViewModel detailOrderViewModel;
  private TalkMainActivity talkMainActivity;
  private RecyclerView recyclerView;
  private User user;
  private SenderMainAdapter senderMainAdapter = new SenderMainAdapter();
  private static final String ARG_BUNDLE_USER = "ARG_BUNDLE_USER";
  private static final String ARG_BUNDLE_PHONE = "ARG_BUNDLE_PHONE";
  private static final String ARG_BUNDLE_PURCHASEORDER = "ARG_BUNDLE_PURCHASEORDER";
  private static final String TAG = "SenderFragment";
  private PurchaseOrder newPurchaseOrder;
  private List<PurchaseOrder> purchaseOrderList = new ArrayList<>();
  private boolean checkBroadcastRegister = false ;
  private BroadcastReceiver mHandler;
  private Handler time;
  private Runnable runnable;
  private String textTime;
  private Long totalTimeFromBackground;
  private Boolean isTouched = false;
  private SwitchCompat switchCompat;

  public static SenderFragment newInstance() {
    return new SenderFragment();
  }

  @SuppressLint("ClickableViewAccessibility")
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {

    talkMainActivity.setDisplayToolBar(true);
    talkMainActivity.setDisplayDrawerLayout(true);
    View view = inflater.inflate(R.layout.sender_fragment, container, false);
    initBroadcastReceiver();
//    LocalBroadcastManager.getInstance(getContext()).registerReceiver(mHandler, new IntentFilter("messageFormFCMCancel"));
    switchCompat = (SwitchCompat) view.findViewById(R.id.switch_sender);
    switchCompat.setOnTouchListener(new View.OnTouchListener() {
      @Override
      public boolean onTouch(View view, MotionEvent motionEvent) {
        isTouched = true;
        return false;
      }
    });
    switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isTouched) {
          isTouched = false;
          if (isChecked) {
            Log.i(TAG, "onCheckedChanged: addddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd");
            mViewModel.sendStatusSender(user.getUser_id(), "add").observe(getViewLifecycleOwner(), responseBody -> {
              if (responseBody.getMessage().equals("กรุณาส่งสินค้าให้เสร็จ")) {
                Toast.makeText(getContext(), responseBody.getMessage(), Toast.LENGTH_SHORT).show();
                switchCompat.setChecked(false);
              }
            });
          } else {
            mViewModel.sendStatusSender(user.getUser_id(), "remove").getValue();
          }
        }
      }
    });
    initDataUserFromBundle();
    swipeRefreshLayout = view.findViewById(R.id.swipe_sender);
    swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override
      public void onRefresh() {
        mViewModel.getDataPurchaseOrder(user.getUser_id());
        swipeRefreshLayout.setRefreshing(false);
      }
    });
    recyclerView = view.findViewById(R.id.sender_recycler_view);
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    recyclerView.setHasFixedSize(true);
    return view;
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    // TODO: Use the ViewModel
    mViewModel = ViewModelProviders.of(requireActivity()).get(SenderViewModel.class);
    mViewModel.getSetSwitchCompat().observe(requireActivity(), aBoolean -> {
      Log.i(TAG, "onActivityCreated: " + "setSwitch " + aBoolean);
      if (aBoolean != null) {
//        changeSwitch = false;
        switchCompat.setChecked(aBoolean);
      }
    });
    detailOrderViewModel = ViewModelProviders.of(requireActivity()).get(DetailOrderViewModel.class);
    detailOrderViewModel.getAnswerOrder().observe(requireActivity(), s -> {
      Log.i(TAG, "onChanged: time.removeCallbacks -----------" + s);
      time.removeCallbacks(runnable);
      if (s.equals("ปฏิเสธรับออเดอร์")) {
        removeNewOrder();
      }
    });
    if (this.user != null)
      mViewModel.setUserMutableLiveData(this.user);
    else
      this.user = mViewModel.getUserMutableLiveData().getValue();

    senderMainAdapter.setOnItemClickListener((position, v) -> {
      Bundle bundle = new Bundle();
      bundle.putParcelable(ARG_BUNDLE_PURCHASEORDER, Parcels.wrap(purchaseOrderList.get(position)));
      Navigation.findNavController(v).navigate(R.id.nav_sender_order_detail, bundle);
    });
    mViewModel.getDataPurchaseOrder(this.user.getUser_id());
    mViewModel.getCheckQueueSender(this.user.getUser_id()).observe(getViewLifecycleOwner(), responseBody -> {
//      Log.i(TAG, "onActivityCreated: " + responseBody.getMessage() + " getCheckQueueSender");
//      Log.i(TAG, "onActivityCreated:changeSwitch " + changeSwitch);
      boolean value = responseBody.getMessage().equals("true");
      if (responseBody.getMessage().equals("true")) {
//        changeSwitch = false;
      }
      switchCompat.setChecked(value);
    });
    mViewModel.getListMutableLiveData().observe(getViewLifecycleOwner(), purchase -> {
      purchaseOrderList = addDataPurchaseOnResume(purchaseOrderList, purchase);
      senderMainAdapter.setPurchaseOrder(purchaseOrderList);
      senderMainAdapter.notifyDataSetChanged();
      recyclerView.setAdapter(senderMainAdapter);
    });

  }

  public List<PurchaseOrder> addDataPurchaseOnResume(List<PurchaseOrder> old, List<PurchaseOrder> newPur) {
    for (int i = old.size() - 1; i >= 0; i--) {
      if (old.get(i).getOrder().getSenderTypeId() != 0)
        old.remove(i);
    }
    if (old.size() > 0) {
      HashMap<String, PurchaseOrder> hashMap = new HashMap<>();
      for (int i = 0; i < old.size(); i++) {
        hashMap.put(old.get(i).getOrder().getOrderId() + "", old.get(i));
      }
      List<PurchaseOrder> purchase = new ArrayList<>();
      for (String key : hashMap.keySet()) {
        purchase.add(hashMap.get(key));
      }
      purchase.addAll(newPur);
      Log.i(TAG, "addDataPurchaseOnResume: " + purchase.size());
      return purchase;
    }
    return newPur;
  }

  public void countTimeOrder(int millis) {
    time = new Handler();
    runnable = new Runnable() {
      @Override
      public void run() {
        removeNewOrder();
//        Log.i(TAG, "run: " + changeSwitch);
        mViewModel.setTimeOutOrder(true);
        //fix bug on user no move to detail Order
        //cause order next on click detail Order off auto
        //because value timeOut in detail Order is true
        mViewModel.setTimeOutOrder(false);
        Toast.makeText(getContext(), "หมดเวลากดสั่งสินค้า", Toast.LENGTH_SHORT).show();
      }
    };
    time.postDelayed(runnable, millis);
  }

  public void removeNewOrder() {
    for (PurchaseOrder p : purchaseOrderList) {
      if (p.getOrder().getSenderTypeId() == 0) {
        purchaseOrderList.remove(p);
        senderMainAdapter.setPurchaseOrder(purchaseOrderList);
        senderMainAdapter.notifyDataSetChanged();
        //bug
        break;
      }
    }
  }

  @Override
  public void onStart() {
    super.onStart();
  }

  @Override
  public void onResume() {
    super.onResume();
    // fix bug on change to detail first
    if (this.purchaseOrderList.size() > 0) {
      senderMainAdapter.setPurchaseOrder(purchaseOrderList);
      senderMainAdapter.notifyDataSetChanged();
      recyclerView.setAdapter(senderMainAdapter);
    }
//    Log.i(TAG, "onResume:changeSwitch " + changeSwitch);

  }

  public void initBroadcastReceiver() {
    //from foreground
    if (mHandler != null || checkBroadcastRegister) return;
    mHandler = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        for (String key : extras.keySet()) {
          Log.i(TAG, "onReceive: -------------- " + extras.getString(key));
        }
        if (extras != null) {
          if (extras.containsKey("dataFromForeGround")) {
            String msg = extras.getString("dataFromForeGround");
            addPurchaseOrder(msg);
//            Log.i(TAG, "onReceive: " + msg);
            extras.remove("dataFromForeGround");
//            Log.i(TAG, "onReceive: " + extras.containsKey("dataFromForeGround"));
          }
          if (extras.containsKey("dataFromForeGroundTime")) {
            Log.i(TAG, "onReceive: " + extras.getString("dataFromForeGroundTime"));
            extras.remove("dataFromForeGroundTime");
//            Log.i(TAG, "onReceive: " + extras.containsKey("dataFromForeGroundTime"));
          }
          if (extras.containsKey("messageCancelQueueSender")) {
            String msg = extras.getString("messageCancelQueueSender");
            removeNewOrder();
            time.removeCallbacks(runnable);
            mViewModel.setTimeOutOrder(true);
            //fix bug on user no move to detail Order
            //cause order next on click detail Order off auto
            //because value timeOut in detail Order is true
            mViewModel.setTimeOutOrder(false);
            switchCompat.setChecked(false);
            Toast.makeText(getContext(), "หมดเวลากดสั่งสินค้า", Toast.LENGTH_SHORT).show();
            extras.remove("messageCancelQueueSender");
//            Log.i(TAG, "onReceive: " + extras.containsKey("dataFromForeGroundTime") );
          }
        }
      }
    };
    LocalBroadcastManager.getInstance(getContext()).registerReceiver(mHandler, new IntentFilter("messageFormFCM"));
    checkBroadcastRegister = true ;
  }

  public void addPurchaseOrder(String purchaseOrder) {
    Gson gson = new Gson();
    this.newPurchaseOrder = gson.fromJson(purchaseOrder, PurchaseOrder.class);
    purchaseOrderList.add(0, newPurchaseOrder);
    senderMainAdapter.setPurchaseOrder(purchaseOrderList);
    senderMainAdapter.notifyDataSetChanged();
    recyclerView.setAdapter(senderMainAdapter);
    countTimeOrder(15000);
    newPurchaseOrder = null;
  }

  @Override
  public void onAttach(@NonNull Context context) {
    super.onAttach(context);
    if (context instanceof TalkMainActivity) {
      talkMainActivity = (TalkMainActivity) context;
    } else {
      throw new ClassCastException(context.toString()
          + " must implement MyListFragment.OnItemSelectedListener");
    }
  }

  public long getDelayAfterUserOpenNotification(String timeFCM) {
    String formatGetTime = "dd/MM/yyyy - HH:mm";
    Locale locale = new Locale("th");
    SimpleDateFormat isoFormat = new SimpleDateFormat(formatGetTime, locale);
    Date date1 = null;
    Date date2 = null;
    try {
      date1 = new SimpleDateFormat(formatGetTime, locale).parse(timeFCM);
      date2 = new Date();
      long l = date2.getTime() - date1.getTime();
      return 30 - (l / 1000);
    } catch (ParseException e) {
      e.printStackTrace();
      Log.i(TAG, "getDelayAfterUserOpenNotification: " + "error");
      return -1;
    }
  }

  public void initDataUserFromBundle() {
    Bundle extras = getArguments();
    if (extras != null) {
      if (extras.containsKey(ARG_BUNDLE_USER)) {
        this.user = Parcels.unwrap(extras.getParcelable(ARG_BUNDLE_USER));
//        Log.i(TAG, "onReceive: " + user);
      }
      if (extras.containsKey("time")) {
        this.textTime = extras.getString("time");
        totalTimeFromBackground = getDelayAfterUserOpenNotification(textTime);
        Log.i(TAG, "initDataUserFromBundle: " + totalTimeFromBackground);
        Log.i(TAG, "onReceive: " + textTime);
      }
      if (extras.containsKey("data")) {
        // form background
        Log.i(TAG, "initDataUserFromBundle: " + extras.containsKey("data"));
        this.newPurchaseOrder = Parcels.unwrap(extras.getParcelable("data"));
        if (totalTimeFromBackground > 0) {
          purchaseOrderList.add(newPurchaseOrder);
          senderMainAdapter.setPurchaseOrder(purchaseOrderList);
          senderMainAdapter.notifyDataSetChanged();
          countTimeOrder((int) (totalTimeFromBackground * 1000));
        } else {
          Toast.makeText(getContext(), "หมดเวลา", Toast.LENGTH_SHORT).show();
        }
        //fix bug background after time out
        extras.remove("data");
      }

    }

//    for (String key : extras.keySet()){
//      Log.i(TAG, "initDataUserFromBundle: " + key);
//    }
  }

  @Override
  public void onPause() {
    super.onPause();
    Log.i(TAG, "onPause: " + this.purchaseOrderList.size());
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    checkBroadcastRegister = false ;
    LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mHandler);
  }
}
