package com.example.project.ui.sender;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.project.model.SenderOrderResponse;
import com.example.project.model.json.PurchaseOrder;
import com.example.project.model.json.ResponseBody;
import com.example.project.model.json.User;

import java.util.ArrayList;
import java.util.List;

public class SenderViewModel extends ViewModel {
  // TODO: Implement the ViewModel
  private static final String TAG = "SenderViewModel" ;
  private SenderOrderResponse senderOrderResponse = new SenderOrderResponse();
  private MutableLiveData<List<PurchaseOrder>> listMutableLiveData = new MutableLiveData<>() ;
  private MutableLiveData<User> userMutableLiveData = new MutableLiveData<>() ;
  private MutableLiveData<Boolean> timeOutOrder = new MutableLiveData<>() ;
  private MutableLiveData<ResponseBody> checkQueueSender = new MutableLiveData<>();
  private MutableLiveData<ResponseBody> statusSender = new MutableLiveData<>();
  private MutableLiveData<Boolean> setSwitchCompat = new MutableLiveData<>() ;
  private MutableLiveData<Boolean> setFalse = new MutableLiveData<>() ;

  public void setSetFalse(Boolean setFalse) {
    this.setFalse.setValue(setFalse);
  }

  public MutableLiveData<Boolean> getSetFalse() {
    return setFalse;
  }

  public void setSetSwitchCompat(Boolean setSwitchCompat) {
    this.setSwitchCompat.setValue(setSwitchCompat);
  }

  public MutableLiveData<Boolean> getSetSwitchCompat() {
    return setSwitchCompat;
  }

  public void setListMutableLiveData(List<PurchaseOrder> listMutableLiveData) {
    this.listMutableLiveData.setValue(listMutableLiveData);
  }

  public MutableLiveData<List<PurchaseOrder>> getListMutableLiveData() {
    return listMutableLiveData;
  }

  public MutableLiveData<User> getUserMutableLiveData() {
    return userMutableLiveData;
  }

  public void setUserMutableLiveData(User userMutableLiveData) {
    this.userMutableLiveData.setValue(userMutableLiveData);
  }

  public MutableLiveData<ResponseBody> getCheckQueueSender() {
    return checkQueueSender;
  }

  public MutableLiveData<ResponseBody> getCheckQueueSender(String sender_id){
   checkQueueSender = senderOrderResponse.getFetchCheckQueueSender(sender_id);
   return checkQueueSender;
  }

  public MutableLiveData<List<PurchaseOrder>> getDataPurchaseOrder(String orderSender) {
    Log.i(TAG, "getDataPurchaseOrder: " + orderSender);
    this.listMutableLiveData = senderOrderResponse.getDataPurchaseOrder(orderSender) ;
    List<PurchaseOrder> a = new ArrayList<>();
    a = listMutableLiveData.getValue();
    if(a != null){
      Log.i(TAG, "getDataPurchaseOrder: " + a.size());
    }
    return this.listMutableLiveData ;
  }

  public MutableLiveData<ResponseBody> getStatusSender() {
    return statusSender;
  }

  public MutableLiveData<ResponseBody> sendStatusSender(String sender_id,String status){
    statusSender = senderOrderResponse.getSendStatusSender(sender_id,status);
    return statusSender;
  }

  public void setTimeOutOrder(Boolean timeOutOrder) {
    this.timeOutOrder.setValue(timeOutOrder);
  }

  public MutableLiveData<Boolean> getTimeOutOrder() {
    return timeOutOrder;
  }
}
