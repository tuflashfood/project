package com.example.project.ui.option_menu;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.example.project.model.json.OrderBuy;
import com.example.project.R;
import com.example.project.model.json.Menu;
import com.example.project.model.json.OptionItem;
import com.example.project.ui.select_restaurant.SelectRestaurantViewModel;
import com.google.gson.Gson;

import org.parceler.Parcels;

import java.lang.reflect.TypeVariable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OptionMenuFragment extends Fragment {
  private static final String SEND_MENU_ARG_BUNDLE = "menu";
  private static final String SEND_RESIDANDCOST_ARG_BUNDLE = "residandcost";
  private static final String SEND_BASKET_ARG_BUNDLE = "basket";
  private static final String TAG = "OptionMenuFragment";
  private String textCheckRadioGroup = "" ;
  private String keyMenu;
  private String menu_name;
  private String menu_description;
  private String menu_price;
  private int menuRequire = 0;
  private int amountOrder = 1;
  private HashMap<String, String> orderFromBasket;
  private String res_cost = "";
  private String selectOption = "";
  private String productNotes = "";
  private HashMap<String, Integer> jsonInOptionAll;
  private Boolean sendFromMenuRes;
  private ImageView image_bigMenu ;
  private OrderBuy orderBuy;
  private TextView option_amount_order;
  private TextView option_menu_name;
  private TextView option_menu_description;
  private TextView option_menu_price;
  private LinearLayout linearOptionMain;
  private Button option_checkOut;
  private Button deduct_amount;
  private RelativeLayout mainRelative_imageBig ;
  private OptionMenuViewModel mViewModel;
  private SelectRestaurantViewModel selectResViewModel;
  private Menu menu;
  private ImageView imageView_close_option ;

  public static OptionMenuFragment newInstance() {
    return new OptionMenuFragment();
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    selectResViewModel = ViewModelProviders.of(requireActivity()).get(SelectRestaurantViewModel.class);
    selectResViewModel.getOrderBuy().observe(getViewLifecycleOwner(), new Observer<OrderBuy>() {
      @Override
      public void onChanged(OrderBuy order) {
        orderBuy = order;
      }
    });
    mViewModel = ViewModelProviders.of(getParentFragment()).get(OptionMenuViewModel.class);

    if (!sendFromMenuRes) {
      Gson gson = new Gson();
      menu = gson.fromJson(orderFromBasket.get("option"), Menu.class);
      keyMenu = orderFromBasket.get("key");
      amountOrder = Integer.parseInt(orderFromBasket.get("amount"));
      option_checkOut.setEnabled(true);
      option_checkOut.setBackgroundColor(getResources().getColor(R.color.main_color));
    }
    if (amountOrder == 1) {
      deduct_amount.setEnabled(false);
      deduct_amount.setBackground(getResources().getDrawable(R.drawable.ic_remove_gray_50dp));
    }
    if (menu == null) {
//      mainRelative_imageBig.setVisibility(View.GONE);
      linearOptionMain.addView(createProductNotes());
      menu_name = orderFromBasket.get("menu_name");
      menu_description = orderFromBasket.get("menu_description");
      menu_price = orderFromBasket.get("price");
    } else {
      getDataOption(menu);
      menu_name = menu.getName();
      menu_description = menu.getDescription();
      menu_price = menu.getPrice();
      image_bigMenu.setImageURI(Uri.parse("android.resource://"+getContext().getPackageName()+"/drawable/"+menu.getPicture()));
    }


    //init amountOrder
    option_amount_order.setText(String.valueOf(amountOrder));
    option_menu_name.setText(menu_name);
    option_menu_description.setText(menu_description);
    option_menu_price.setText("฿ " + menu_price);

    option_checkOut.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (orderBuy == null) {
          orderBuy = new OrderBuy();
        }
        if (orderBuy.getRes_id() == null) {
          orderBuy.setRes_id(res_cost.split(" ")[0]);
          orderBuy.setTransportCost(res_cost.split(" ")[1]);
        }

        if(menu != null){
          selectOption += menu_name + ">%<";
          getDataOptionFromUserSelect();
        }else {
          getDataProductNoteWithOutMenu();
        }

        if(!orderBuy.getRes_id().equals(res_cost.split(" ")[0]) && !res_cost.equals("")){
          alertNewRestaurant();
        }else {
          if (sendFromMenuRes)
            orderBuy.addMenu(createMenuOption(selectOption, amountOrder),"option");
          else {
            if(menu != null)
              orderBuy.addMenuOption(createMenuOption(selectOption, amountOrder), orderFromBasket.get("key"));
            else
              orderBuy.addMenu(createMenuOption(amountOrder),"option");
          }
          Log.i(TAG, "onClick: " + orderBuy);
          selectResViewModel.setOrderBuy(orderBuy);
          Navigation.findNavController(v).popBackStack(R.id.nav_menu_option, true);
        }
      }
    });
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

    View view = inflater.inflate(R.layout.option_menu_fragment, container, false);
    linearOptionMain = view.findViewById(R.id.option_main);
    option_menu_name = view.findViewById(R.id.option_menu_name);
    option_menu_description = view.findViewById(R.id.option_menu_description);
    option_menu_price = view.findViewById(R.id.option_menu_price);
    deduct_amount = view.findViewById(R.id.deduct_amount);
    option_amount_order = view.findViewById(R.id.option_amount_order);
    option_checkOut = view.findViewById(R.id.option_checkOut);
    Button increase_amount = view.findViewById(R.id.increase_amount);
    image_bigMenu = view.findViewById(R.id.image_bigMenu) ;
    mainRelative_imageBig = view.findViewById(R.id.mainRelative_imageBig) ;
    imageView_close_option = view.findViewById(R.id.imageView_close_option);
    imageView_close_option.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Navigation.findNavController(getView()).popBackStack(R.id.nav_menu_option, true);
      }
    });
    //init option menu
    sendFromMenuRes = initDataMenuFromBundle().equals("sendFromMenuRes");
    deduct_amount.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        deductAmountOrder(v);
      }
    });
    increase_amount.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        increaseAmountOrder();
      }
    });
    return view;
  }

  public void alertNewRestaurant(){
    final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
    LinearLayout linearLayout = new LinearLayout(getContext());
    linearLayout.setBackgroundColor(getResources().getColor(R.color.color_red));
    linearLayout.setPadding(0,10,0,0);
    linearLayout.setGravity(Gravity.CENTER);
    ImageView imageView = new ImageView(getContext());
    int dp25 = (int)pxFromDp(getContext(),25);
    imageView.setLayoutParams(new LinearLayout.LayoutParams(dp25,dp25));
    imageView.setImageResource(R.drawable.signs);
    TextView title = new TextView(getContext());
    title.setTextColor(getResources().getColor(R.color.colorWhite));
    title.setText(" โปรดตรวจสอบ");
    title.setGravity(Gravity.CENTER);
    title.setPadding(30,0,0,0);
    title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
    linearLayout.setOrientation(LinearLayout.HORIZONTAL);
    linearLayout.addView(imageView);
    linearLayout.addView(title);
    builder.setCustomTitle(linearLayout) ;
    builder.setMessage("คุณเลือกร้านอื่น หากดำเนินการต่อรายการในตะกร้าของคุณจะถูกลบ");
    builder.setCancelable(false);
    builder.setNegativeButton("ใช่", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        orderBuy.clearOrder();
        Log.i(TAG, "onClick: clear " + orderBuy.toString());
        orderBuy.setRes_id(res_cost.split(" ")[0]);
        orderBuy.setTransportCost(res_cost.split(" ")[1]);
        orderBuy.addMenu(createMenuOption(selectOption, amountOrder),"option");
        Log.i(TAG, "onClick: " + orderBuy.toString());
        selectResViewModel.setOrderBuy(orderBuy);
        Navigation.findNavController(getView()).popBackStack(R.id.nav_menu_option, true);
      }
    });
    builder.setPositiveButton("ยกเลิก", null);
    AlertDialog dialog = builder.create();
    dialog.show();
    TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
    messageText.setTypeface(ResourcesCompat.getFont(getContext(), R.font.layijimahaniyom));
    messageText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
    messageText.setGravity(Gravity.CENTER);
  }

  public static float pxFromDp(final Context context, final float dp) {
    return dp * context.getResources().getDisplayMetrics().density;
  }

  public HashMap<String,String> createMenuOption(int amount){
    HashMap<String,String> menuOption = new HashMap<>();
    menuOption.put("key",menu_name);
    menuOption.put("menu_name",menu_name);
    menuOption.put("menu_description",menu_description);
    menuOption.put("price",menu_price);
    menuOption.put("total_price",(Integer.parseInt(menu_price)*amount) + "");
    menuOption.put("amount",amount+"");
    menuOption.put("product_notes", productNotes);
    return menuOption;
  }

  public HashMap<String, String> createMenuOption(String option, int amount) {
    int totalPrice = calCuRatePriceOption(option) + Integer.parseInt(menu_price);
    HashMap<String, String> menuOption = new HashMap<>();
    Gson gson = new Gson();
    menuOption.put("key", option);
    menuOption.put("amount", amount + "");
    menuOption.put("price", totalPrice + "");
    menuOption.put("total_price", String.valueOf(totalPrice * amount));
    menuOption.put("menu_name", menu_name);
    menuOption.put("menu_description", menu_description);
    menuOption.put("option", gson.toJson(menu));
    menuOption.put("product_notes", productNotes);
    return menuOption;
  }

  public boolean checkKey(String data, String key) {
    String keysplit[] = key.split(">%<");
    //key i = 0 is menu_name
    for (int i = 1; i < keysplit.length; i++) {
      if (keysplit[i].equals(data)) {
        return true;
      }
    }
    return false;
  }


  public int calCuRatePriceOption(String selectOption) {
    String splitSelectOption[] = selectOption.split(">%<");
    int priceOption = 0;
    // i = 0 menu_name
    for (int i = 1; i < splitSelectOption.length; i++) {
      if (jsonInOptionAll.get(splitSelectOption[i]) != null) {
        priceOption += jsonInOptionAll.get(splitSelectOption[i]);
      }
    }
    Log.i(TAG, "calCuRatePriceOption: " + priceOption);
    return priceOption;
  }

  public void deductAmountOrder(View v) {
    amountOrder--;
    option_amount_order.setText(String.valueOf(amountOrder));
    if (amountOrder == 1) {
      deduct_amount.setEnabled(false);
      deduct_amount.setBackground(getResources().getDrawable(R.drawable.ic_remove_gray_50dp));
    }
  }

  public void increaseAmountOrder() {
    amountOrder++;
    option_amount_order.setText(String.valueOf(amountOrder));
    if (amountOrder < 3) {
      deduct_amount.setEnabled(true);
      deduct_amount.setBackground(getResources().getDrawable(R.drawable.ic_remove_maincolor_50dp));
    }
  }

  public void getDataOptionFromUserSelect() {
    for (int i = 0; i < linearOptionMain.getChildCount(); i++) {
      LinearLayout option = (LinearLayout) linearOptionMain.getChildAt(i);
      //option
      if (i != linearOptionMain.getChildCount() - 1) {
        //data option
        LinearLayout linearOption = (LinearLayout) option.getChildAt(1);
        LinearLayout linearOptionAll = (LinearLayout) linearOption.getChildAt(0);
        for (int k = 0; k < linearOptionAll.getChildCount(); k++) {
          if (linearOptionAll.getChildAt(k) instanceof RadioButton) {
            RadioButton radioButton = (RadioButton) linearOptionAll.getChildAt(k);
            if (radioButton.isChecked()) {
              selectOption = selectOption.concat(radioButton.getText() + ">%<");
            }
          } else if (linearOptionAll.getChildAt(k) instanceof CheckBox) {
            CheckBox checkBox = (CheckBox) linearOptionAll.getChildAt(k);
            if (checkBox.isChecked()) {
              selectOption = selectOption.concat(checkBox.getText() + ">%<");
            }
          }
        }
      } else {
        //createProductNotes
        LinearLayout option1 = (LinearLayout) option.getChildAt(2);
        EditText editText = (EditText) option1.getChildAt(0);
        if (!editText.getText().toString().equals("")) {
          productNotes = editText.getText().toString();
          Log.i(TAG, "getDataOptionFromUserSelect: -----------" + productNotes);
        }
      }
    }
  }

  public void getDataProductNoteWithOutMenu() {
    LinearLayout linearLayout = (LinearLayout) linearOptionMain.getChildAt(0);
    LinearLayout linearLayout1 = (LinearLayout) linearLayout.getChildAt(1);
    EditText editText = (EditText) linearLayout1.getChildAt(0);
    if (!editText.getText().toString().equals("")) {
      productNotes = editText.getText().toString();
      Log.i(TAG, "getDataOptionFromUserSelect: -----------" + productNotes);
    }
  }
  public String initDataMenuFromBundle() {
    //get data from menu with bundle key is menu
    String result = "";
    if (getArguments() != null) {
      if (Parcels.unwrap(getArguments().getParcelable(SEND_RESIDANDCOST_ARG_BUNDLE)) != null && Parcels.unwrap(getArguments().getParcelable(SEND_MENU_ARG_BUNDLE)) != null) {
        menu = Parcels.unwrap(getArguments().getParcelable(SEND_MENU_ARG_BUNDLE));
        res_cost = Parcels.unwrap(getArguments().getParcelable(SEND_RESIDANDCOST_ARG_BUNDLE));
        result = "sendFromMenuRes";
      } else {
        orderFromBasket = Parcels.unwrap(getArguments().getParcelable(SEND_BASKET_ARG_BUNDLE));
        result = "sendFromShoppingBasker";
      }

    }
    return result;
  }

  public void setLayoutOption(String detail, String select, String[][] data) {
    int required = select.equals("radio") ? 1 : data.length;

    LinearLayout.LayoutParams layoutParamsOption = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    layoutParamsOption.setMargins(35, 30, 0, 0);
    LinearLayout option = new LinearLayout(getContext());
    option.setOrientation(LinearLayout.VERTICAL);
    option.setLayoutParams(layoutParamsOption);

    LinearLayout.LayoutParams topMargin = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    topMargin.setMargins(0, 10, 0, 0);

    option.addView(createDetailAndRequired(detail, required));
    option.addView(createRadioGroupAndLinearCheckBox(data, select), topMargin);

    linearOptionMain.addView(option);
  }

  public FrameLayout createDetailAndRequired(String detail, int required) {
    FrameLayout optionTextView = new FrameLayout(getContext());
    FrameLayout.LayoutParams layoutParamsOptionTextView = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
    layoutParamsOptionTextView.setMarginEnd(30);
    optionTextView.setLayoutParams(layoutParamsOptionTextView);

    TextView tViewDetail = new TextView(getContext());
    tViewDetail.setTextColor(getResources().getColor(R.color.colorBlack));
    tViewDetail.setText(detail);

    TextView tViewRequire = new TextView(getContext());
    tViewRequire.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
    tViewRequire.setGravity(Gravity.CENTER);
    tViewRequire.setBackgroundColor(getResources().getColor(R.color.colorGray));
    tViewRequire.setTypeface(ResourcesCompat.getFont(getContext(), R.font.layijimahaniyom), Typeface.BOLD);
    if(required > 1){
      tViewRequire.setText(" OPTIONAL ");
    }
    else{
      tViewRequire.setText(" " + required + " REQUIRED ");
      tViewRequire.setTextColor(getResources().getColor(R.color.colorOption));
    }


    optionTextView.addView(tViewDetail);
    optionTextView.addView(tViewRequire, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, Gravity.RIGHT | Gravity.BOTTOM));

    return optionTextView;
  }

  public LinearLayout createRadioGroupAndLinearCheckBox(String data[][], String select) {
    LinearLayout linearMain = new LinearLayout(getContext());
    linearMain.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
    linearMain.setOrientation(LinearLayout.HORIZONTAL);

    LinearLayout linearOption = new LinearLayout(getContext());
    linearOption.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT));
    linearOption.setOrientation(LinearLayout.VERTICAL);

    if (select.equals("radio")) {
      linearMain.addView(crateRadioGroup(data, linearOption));
    } else {
      linearMain.addView(crateLinearCheckBox(data, linearOption));
    }

    FrameLayout frameLayout = new FrameLayout(getContext());
    frameLayout.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
    frameLayout.addView(linearOption, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.MATCH_PARENT, Gravity.RIGHT));
    linearMain.addView(frameLayout);
    return linearMain;
  }

  public RadioGroup crateRadioGroup(String data[][], LinearLayout linearOption) {
    //add option all in menu require
    final RadioGroup radioGroup = new RadioGroup(getContext());
    radioGroup.setOrientation(LinearLayout.VERTICAL);
    radioGroup.setLayoutParams(new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT));
    for (int i = 0; i < data.length; i++) {
      radioGroup.addView(crateRadioButton(data[i][0]));
      if (!sendFromMenuRes && checkKey(data[i][0], keyMenu)) {
        radioGroup.check(radioGroup.getChildAt(i).getId());
      }
      linearOption.addView(createPrice(data[i][1]));
    }
    if (sendFromMenuRes) {
      menuRequire++;
      radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
          //onCheckedChanged check all radio group on click
          if(textCheckRadioGroup.lastIndexOf(group.toString()) == -1) {
            textCheckRadioGroup += group.toString() + " ";
            menuRequire--;
          }
          if (menuRequire == 0) {
            option_checkOut.setEnabled(true);
            option_checkOut.setBackgroundColor(getResources().getColor(R.color.main_color));
          }
        }
      });
    }
    return radioGroup;
  }

  public LinearLayout crateLinearCheckBox(String data[][], LinearLayout linearOption) {
    LinearLayout linearCheckBox = new LinearLayout(getContext());
    linearCheckBox.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
    linearCheckBox.setOrientation(LinearLayout.VERTICAL);

    for (int i = 0; i < data.length; i++) {
      linearCheckBox.addView(crateCheckBox(data[i][0]));
      linearOption.addView(createPrice(data[i][1]));
    }
    return linearCheckBox;
  }


  public RadioButton crateRadioButton(String data) {
    RadioButton radioButton = new RadioButton(getContext());
    radioButton.setText(data);
    return radioButton;
  }

  public CheckBox crateCheckBox(String data) {
    CheckBox checkBox = new CheckBox(getContext());
    checkBox.setText(data);
    if (!sendFromMenuRes && checkKey(data, keyMenu)) {
      checkBox.setChecked(true);
    }
    return checkBox;
  }

  public TextView createPrice(String data) {
    TextView textView = new TextView(getContext());
//    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,16);
    textView.setText("+฿ " + data + ".00");
    textView.setTextColor(getResources().getColor(R.color.colorBlack));
    textView.setGravity(Gravity.CENTER);
    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
    layoutParams.setMargins(0, 0, 40, 0);
    textView.setLayoutParams(layoutParams);
    return textView;
  }

//  public void getDataOptionString(String option){
//    //or replaceAll "{" to ""
//    jsonInOptionAll = new HashMap<>();
//    option = option.substring(1,option.length()-1);
//    String splitOption [] = option.split(",");
//    for (String s:splitOption) {
//      String[] parts = s.split("=");
//      jsonInOptionAll.put( parts[0].trim(), Integer.parseInt(parts[1].trim()) );
//    }
//  }

  public void getDataOption(Menu menu) {
    List<OptionItem> option = menu.getOption();
    jsonInOptionAll = new HashMap<>();
    for (int i = 0; i < option.size(); i++) {
      List<HashMap<String, String>> jsonInOption = option.get(i).getData();
      String dataInJson[][] = new String[jsonInOption.size()][2];
      int k = 0;
      for (int j = 0; j < jsonInOption.size(); j++) {
        for (Map.Entry<String, String> entry : jsonInOption.get(j).entrySet()) {
          dataInJson[k][0] = entry.getKey();
          dataInJson[k][1] = entry.getValue();
          jsonInOptionAll.put(entry.getKey(), Integer.parseInt(entry.getValue()));
          k++;
        }
      }
      setLayoutOption(option.get(i).getDetail(), option.get(i).getSelect(), dataInJson);
    }
    linearOptionMain.addView(createProductNotes());
  }

  public LinearLayout createProductNotes() {
    LinearLayout linearLayoutMain = new LinearLayout(getContext());
    linearLayoutMain.setOrientation(LinearLayout.VERTICAL);
    LinearLayout linearSpace = new LinearLayout(getContext());
    LinearLayout.LayoutParams linearSpaceParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) getResources().getDimension(R.dimen.value_4dp));
    linearSpaceParams.setMargins(getResources().getDimensionPixelSize(R.dimen.value_10dp), getResources().getDimensionPixelSize(R.dimen.value_10dp),
        getResources().getDimensionPixelSize(R.dimen.value_10dp), getResources().getDimensionPixelSize(R.dimen.value_10dp));
    linearSpace.setLayoutParams(linearSpaceParams);
    linearSpace.setBackgroundColor(getResources().getColor(R.color.colorGray));

    LinearLayout linearLayout = new LinearLayout(getContext());
    linearLayout.setBackgroundColor(getResources().getColor(R.color.colorGray));
    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    layoutParams.setMargins(getResources().getDimensionPixelSize(R.dimen.value_10dp), 0,
        getResources().getDimensionPixelSize(R.dimen.value_10dp), getResources().getDimensionPixelSize(R.dimen.value_10dp));
    linearLayout.setLayoutParams(layoutParams);
    linearLayout.setOrientation(LinearLayout.VERTICAL);

    LinearLayout.LayoutParams layoutViewTextView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    layoutViewTextView.setMargins(getResources().getDimensionPixelSize(R.dimen.value_13dp), 0, 0, getResources().getDimensionPixelSize(R.dimen.value_5dp));

    LinearLayout.LayoutParams layoutViewEditText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.value_10dp) * 9);
    layoutViewEditText.setMargins(getResources().getDimensionPixelSize(R.dimen.value_3dp), getResources().getDimensionPixelSize(R.dimen.value_3dp),
        getResources().getDimensionPixelSize(R.dimen.value_3dp), getResources().getDimensionPixelSize(R.dimen.value_3dp));

    TextView textView = new TextView(getContext());

    textView.setText("หมายเหตุ");
    textView.setTextColor(getResources().getColor(R.color.colorBlack));
    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,25);
    textView.setTypeface(ResourcesCompat.getFont(getContext(), R.font.layijimahaniyom), Typeface.BOLD);
    EditText editText = new EditText(getContext());
    editText.setGravity(Gravity.TOP);
    editText.setHint(" ใส่ข้อมูลลงที่นี้");
    editText.setTextSize(TypedValue.COMPLEX_UNIT_SP,18);
    editText.setPadding(getResources().getDimensionPixelSize(R.dimen.value_5dp), getResources().getDimensionPixelSize(R.dimen.value_5dp), 0, 0);
    editText.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
    editText.setBackgroundColor(getResources().getColor(R.color.colorWhite));
    if (!sendFromMenuRes) {
      if (orderFromBasket.get("product_notes") != null)
        editText.setText(orderFromBasket.get("product_notes"));
    }
    linearLayout.addView(editText, layoutViewEditText);
    if(menu != null)
      linearLayoutMain.addView(linearSpace);
    linearLayoutMain.addView(textView, layoutViewTextView);
    linearLayoutMain.addView(linearLayout);
    return linearLayoutMain;
  }

  @Override
  public void onAttach(@NonNull Context context) {
    super.onAttach(context);
  }

  @Override
  public void onDetach() {
    super.onDetach();
  }
}
