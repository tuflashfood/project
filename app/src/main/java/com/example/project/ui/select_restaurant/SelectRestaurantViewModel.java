package com.example.project.ui.select_restaurant;

import android.graphics.Point;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.project.model.SelectRestaurantRepository;
import com.example.project.model.json.OrderBuy;
import com.example.project.model.RestaurantRepository;
import com.example.project.model.json.ResponseBody;
import com.example.project.model.json.Restaurant;
import com.example.project.model.json.User;

import java.util.List;

public class SelectRestaurantViewModel extends ViewModel {
  // TODO: Implement the ViewModel
  private static final String TAG = "SelectRestaurantViewModel" ;
  private RestaurantRepository restaurantRepository = new RestaurantRepository(); ;
  private MutableLiveData<List<Restaurant>> listMutableLiveData = new MutableLiveData<>();;
  private MutableLiveData<OrderBuy> orderBuy = new MutableLiveData<>(); ;
  private SelectRestaurantRepository selectRestaurantRepository = new SelectRestaurantRepository() ;
  private MutableLiveData<User> userMutableLiveData = new MutableLiveData<>();

  public void setUserMutableLiveData(User userMutableLiveData) {
    this.userMutableLiveData.setValue(userMutableLiveData);
  }

  public MutableLiveData<User> getUserMutableLiveData() {
    return userMutableLiveData;
  }

  public MutableLiveData<List<Restaurant>> getRestaurantLiveData() {
    listMutableLiveData = restaurantRepository.getRestaurantMutableLiveData();
    return listMutableLiveData ;
  }

  public MutableLiveData<ResponseBody> sendDataOrderToServer(String time, String latlong, String nameLocation){
    OrderBuy orderfix  = orderBuy.getValue();
    orderfix.setDestination_location("14.073563,100.606313");
    orderfix.setDestination_location_name(nameLocation);
    return selectRestaurantRepository.getSendDataOrderToServer(orderfix,time);
  }

  public MutableLiveData<Restaurant> getIndexRestaurantLiveData(int index){
    return restaurantRepository.getRestaurantByIndex(index);
  }

  public void setOrderBuy(OrderBuy orderBuy) {
    this.orderBuy.setValue(orderBuy);
  }

  public MutableLiveData<OrderBuy> getOrderBuy() {
    return orderBuy;
  }


}
