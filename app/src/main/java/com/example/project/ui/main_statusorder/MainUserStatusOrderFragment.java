package com.example.project.ui.main_statusorder;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.project.R;
import com.example.project.adapter.MainStatusOrderAdapter;
import com.example.project.interfaces.TalkMainActivity;
import com.example.project.model.json.OrderStatus;
import com.example.project.model.json.PurchaseOrder;
import com.example.project.model.json.User;
import com.google.gson.Gson;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainUserStatusOrderFragment extends Fragment {
  private static final String TAG = "MainUserStatusOrderFragment" ;
  private static final String ARG_BUNDLE_ORDERSTATUS = "order_status" ;
  private static final String ARG_BUNDLE_ORDERID = "order_id";
  private SwipeRefreshLayout swipeRefreshLayout ;
  private MainUserStatusOrderViewModel mViewModel;
  private RecyclerView recyclerView;
  private List<PurchaseOrder> purchaseOrderList = new ArrayList<>();;
  private MainStatusOrderAdapter mainStatusOrderAdapter = new MainStatusOrderAdapter();
  private ImageView imageView_close_statusOrder ;
  private User user = new User();
  private TalkMainActivity talkMainActivity;

  public static MainUserStatusOrderFragment newInstance() {
    return new MainUserStatusOrderFragment();
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.main_user_status_order_fragment, container, false);
    talkMainActivity.setDisplayToolBar(false);
    talkMainActivity.setDisplayDrawerLayout(false);
    getStringFormFile();
    imageView_close_statusOrder = view.findViewById(R.id.imageView_close_statusOrder);
    imageView_close_statusOrder.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Navigation.findNavController(getView()).popBackStack(R.id.nav_mainUserStatusOrderFragment,true);
      }
    });
    swipeRefreshLayout = view.findViewById(R.id.swipe_user);
    swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override
      public void onRefresh() {
        getDataStatusOrder();
        swipeRefreshLayout.setRefreshing(false);
      }
    });
    recyclerView = (RecyclerView) view.findViewById(R.id.statusorder_recycler_view);
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    recyclerView.setHasFixedSize(true);
    return view ;
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    mViewModel = ViewModelProviders.of(requireActivity()).get(MainUserStatusOrderViewModel.class);
    getDataStatusOrder();
    // TODO: Use the ViewMode
  }

  public void getDataStatusOrder(){
    mViewModel.getStatusOrder(user.getUser_id()).observe(getViewLifecycleOwner(), new Observer<List<PurchaseOrder>>() {
      @Override
      public void onChanged(List<PurchaseOrder> purchaseOrders) {
        if(purchaseOrders != null) {
          purchaseOrderList = purchaseOrders ;
          mainStatusOrderAdapter.setPurchaseOrder(purchaseOrderList);
          mainStatusOrderAdapter.notifyDataSetChanged();
          mainStatusOrderAdapter.setOnItemClickListener(new MainStatusOrderAdapter.MyClickListener() {
            @Override
            public void onItemClick(HashMap<String, OrderStatus> order_status, String statusOrderId) {
              Bundle bundle = new Bundle();
              bundle.putParcelable(ARG_BUNDLE_ORDERSTATUS, Parcels.wrap(order_status));
              bundle.putString(ARG_BUNDLE_ORDERID, statusOrderId);
              Navigation.findNavController(getView()).navigate(R.id.nav_userStatusOrderFragment, bundle);
            }
          });
          recyclerView.setAdapter(mainStatusOrderAdapter);
        }
      }
    }); ;
  }

  public void getStringFormFile() {
    SharedPreferences shared_pref = getContext().getSharedPreferences("MyPrefsFile", Context.MODE_PRIVATE);
    String textUser = shared_pref.getString("user", "No Pref");
    if (!textUser.equals("No Pref")) {
      Gson gson = new Gson();
      user = gson.fromJson(textUser, User.class);
    }
  }

  @Override
  public void onAttach(@NonNull Context context) {
    super.onAttach(context);
    if (context instanceof TalkMainActivity) {
      talkMainActivity = (TalkMainActivity) context;
    } else {
      throw new ClassCastException(context.toString()
          + " must implement MyListFragment.OnItemSelectedListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    Log.i(TAG, "onDetach: ");
    talkMainActivity = null;
  }
}
