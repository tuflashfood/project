package com.example.project.ui.detail_order;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.project.model.DetailOrderRepository;
import com.example.project.model.json.ResponseBody;
import com.example.project.service.HttpManager;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailOrderViewModel extends ViewModel {
  // TODO: Implement the ViewModel
  private DetailOrderRepository detailOrderRepository = new DetailOrderRepository();
  private static final String TAG = "DetailOrderViewModel" ;
  private Call<ResponseBody> callResponseMRS;
  private MutableLiveData<ResponseBody> resultMessageRestaurantSuccess = new MutableLiveData<>();
  private MutableLiveData<ResponseBody> resultMessageUpdateStatusOrder = new MutableLiveData<>();
  private MutableLiveData<ResponseBody> resultMessageSearchSender = new MutableLiveData<>();
  private MutableLiveData<ResponseBody> resultMessageUpdateSenderIdOrder = new MutableLiveData<>();
  private MutableLiveData<ResponseBody> resultMessageRemoveOrder = new MutableLiveData<>();
  private MutableLiveData<ResponseBody> resultMessageUpdateOrderRestaurant = new MutableLiveData<>();
  private MutableLiveData<ResponseBody> resultMessageUpdateOrderComplete = new MutableLiveData<>();
  private MutableLiveData<ResponseBody> resultMessageAnswerToSender = new MutableLiveData<>();
  private MutableLiveData<String> answerOrder = new MutableLiveData<>();

  public MutableLiveData<ResponseBody> sendAnswerToSender(String sender_id, String order_id, String values) {
    resultMessageAnswerToSender = detailOrderRepository.getSendAnswerToSender(sender_id, order_id, values);
    return  resultMessageAnswerToSender ;
  }

  public MutableLiveData<ResponseBody> getResultMessageAnswerToSender() {
    return resultMessageAnswerToSender;
  }

  public MutableLiveData<ResponseBody> sendUpdateOrderComplete(String order_id, String value) {
    resultMessageUpdateOrderComplete = detailOrderRepository.getSendUpdateOrderComplete(order_id, value);
    return resultMessageUpdateOrderComplete;
  }

  public MutableLiveData<ResponseBody> getResultMessageUpdateOrderComplete() {
    return resultMessageUpdateOrderComplete;
  }

  public MutableLiveData<ResponseBody> sendUpdateOrderRestaurant(String order_id, String value) {
    resultMessageUpdateOrderRestaurant = detailOrderRepository.getSendUpdateOrderRestaurant(order_id, value);
    return resultMessageUpdateOrderRestaurant;
  }

  public MutableLiveData<ResponseBody> getResultMessageUpdateOrderRestaurant() {
    return resultMessageUpdateOrderRestaurant;
  }

  public MutableLiveData<ResponseBody> sendRemoveOrderSender(String order_id) {
    resultMessageRemoveOrder = detailOrderRepository.getSendRemoveOrderSender(order_id);
    return resultMessageRemoveOrder;
  }

  public MutableLiveData<ResponseBody> getResultMessageRemoveOrder() {
    return resultMessageRemoveOrder;
  }

  public void setResultMessageRestaurantSuccess(MutableLiveData<ResponseBody> resultMessageRestaurantSuccess) {
    this.resultMessageRestaurantSuccess = resultMessageRestaurantSuccess;
  }

  public void fetchCheckOrderRestaurantOrderSuccess(String order_id) {
    callResponseMRS = HttpManager.getInstance().getService().checkOrderRestaurantOrderSuccess(order_id);
    callResponseMRS.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        Gson gResponseBody = new Gson();
        if (response.isSuccessful()) {
          Log.i(TAG, "onResponse: " + response.body());
          resultMessageRestaurantSuccess.setValue(response.body());
        }
        else {
          try {
            Log.i(TAG, "onResponse: " + response.errorBody().string());
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.e(TAG, "onFailure: 6" + t.toString());
      }
    });
  }

  public MutableLiveData<ResponseBody> getResultMessageRestaurantSuccess() {
    return resultMessageRestaurantSuccess;
  }

  public MutableLiveData<ResponseBody> sendUpdateStatusOrder(String order_id, String status) {
    resultMessageUpdateStatusOrder = detailOrderRepository.getUpdateStatusSender(order_id, status);
    return resultMessageUpdateStatusOrder;
  }

  public MutableLiveData<ResponseBody> getResultMessageUpdateStatusOrder() {
    return resultMessageUpdateStatusOrder;
  }

  public MutableLiveData<ResponseBody> sendSearchSender(String order_id) {
    resultMessageSearchSender = detailOrderRepository.getSearchSender(order_id);
    return resultMessageSearchSender;
  }

  public MutableLiveData<ResponseBody> getResultMessageSearchSender() {
    return resultMessageSearchSender;
  }

  public MutableLiveData<ResponseBody> sendUpdateSenderIdOrder(String order_id, String sender_id) {
    resultMessageUpdateSenderIdOrder = detailOrderRepository.getSendUpdateOrderSenderIdOrder(order_id, sender_id);
    return resultMessageUpdateSenderIdOrder;
  }

  public MutableLiveData<ResponseBody> getResultMessageUpdateSenderIdOrder() {
    return resultMessageUpdateSenderIdOrder;
  }

  public void setAnswerOrder(String answerOrder) {
    this.answerOrder.setValue(answerOrder);
  }

  public MutableLiveData<String> getAnswerOrder() {
    return answerOrder;
  }
}
