package com.example.project.ui.register;

import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.interfaces.Callbacklogin;
import com.example.project.model.json.ResponseBody;
import com.example.project.service.HttpManager;
import com.example.project.ui.login.LoginFragment;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterFragment extends Fragment {

  private RegisterViewModel mViewModel;
  private static final String TAG = "RegisterFragment";
  private Call<ResponseBody> responseBodyCall;
  private Callbacklogin callbacklogin;
  private EditText username;
  private EditText password;
  private EditText name;
  private EditText phone;
  private Button sign_up_button;
  private String strBefore ;
  private String message ;
  public static RegisterFragment newInstance() {
    return new RegisterFragment();
  }

  TextWatcher textWatcher = new TextWatcher() {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
      String value = s.toString();
      if (phone.getText().hashCode() == s.hashCode()){
        // This is just an example, your magic will be here!
        value = value.replaceAll("-","");
        phone.removeTextChangedListener(textWatcher);
        s.clear();
        s.append(regexPhone(value));

        phone.setSelection(s.length());
      }
      phone.addTextChangedListener(textWatcher);
    }
  };

  public String regexPhone(String text){
    if(text.length() == 3)
      return text;

    String regex = "(\\d{3})";
    String regex1 = "(\\d{3})(\\d{3})(\\d{4})";
    String replace = "$1-" ;
    String replace1 = "$1-$2-$3" ;
    String result = "" ;

    if(text.length() > 9)
      result = text.replaceAll(regex1,replace1);
    else
      result = text.replaceAll(regex,replace);

    if(text.length() == 6 || text.length() == 9 )
      result = result.substring(0,result.length()-1);

    return result ;
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {

    final View view = inflater.inflate(R.layout.register_fragment, container, false);
    username = view.findViewById(R.id.sign_up_username);
    password = view.findViewById(R.id.sign_up_password);
    name = view.findViewById(R.id.sign_up_name);
    phone = view.findViewById(R.id.sign_up_phone);
    StringBuilder textBuilder = new StringBuilder();
    phone.addTextChangedListener(textWatcher);
    sign_up_button = view.findViewById(R.id.sign_up_button);
    sign_up_button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if(!name.getText().toString().equals("") && !username.getText().toString().equals("") && !password.getText().toString().equals("") && !phone.getText().toString().equals("")){
          if(phone.getText().toString().length() < 9){
            Toast.makeText(getContext(),"กรุณากรอกเลขโทรศัทพ์อย่างน้อย 9 ตัว",Toast.LENGTH_SHORT).show();
            return;
          }

          if(password.getText().toString().length() < 6){
            Toast.makeText(getContext(),"กรุณากรอก password อย่างน้อย 6 ตัว",Toast.LENGTH_SHORT).show();
            return;
          }

          sendDataRegisterToServer(name.getText().toString(),username.getText().toString(),password.getText().toString(),phone.getText().toString());
        }
        else
          Toast.makeText(getContext(),"กรุณากรอกข้อมูลให้ครบ",Toast.LENGTH_SHORT).show();
      }
    });
    return view;
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    try {
      callbacklogin = (Callbacklogin) context;
    } catch (ClassCastException e) {
      throw new ClassCastException(context.toString() + " must implement Callback");
    }
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    mViewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);
    // TODO: Use the ViewModel
  }
  public void sendDataRegisterToServer(String name,String username,String password,String phone){
    responseBodyCall = HttpManager.getInstance().getService().createuser(name,username,password,phone);
    responseBodyCall.enqueue(new Callback<ResponseBody>() {
      @Override
      public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        if (response.isSuccessful()){
          Toast.makeText(getContext(),"สมัครสมาชิกสำเร็จ",Toast.LENGTH_SHORT).show();
          Handler handler = new Handler();
          Runnable r = new Runnable() {
            public void run() {
              getFragmentManager()
                  .beginTransaction()
                  .remove(RegisterFragment.this)
                  .commit();
            }
          };
          handler.postDelayed(r, 1500);
        }else {
          Gson gson = new Gson();
          try {
            message = gson.fromJson(response.errorBody().string(), ResponseBody.class).getMessage();
            Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
          } catch (IOException e) {
            e.printStackTrace();
          }
        }
      }

      @Override
      public void onFailure(Call<ResponseBody> call, Throwable t) {
        Log.i(TAG, "onFailure: " + t.toString());
      }
    });
  }

}
