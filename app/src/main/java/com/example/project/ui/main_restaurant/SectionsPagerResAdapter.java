package com.example.project.ui.main_restaurant;

import android.content.Context;
import android.util.Log;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.project.R;
import com.example.project.model.json.PurchaseOrder;
import com.example.project.model.json.User;
import com.example.project.ui.main_order_restaurant.MainOrderRestaurantFragment;

public class SectionsPagerResAdapter extends FragmentStateAdapter {
  private String TAG = "SectionsPagerResAdapter";
  private static final int[] TAB_TITLES = new int[]{R.string.tab_resNotYetReceivedTheOrder, R.string.tab_resReceivedTheOrder, R.string.tab_resOrderSuccess};
  private PurchaseOrder newPurchaseOrder;
  private User user ;

  public SectionsPagerResAdapter(@NonNull FragmentActivity fragmentActivity) {
    super(fragmentActivity);
  }

  public void setNewPurchaseOrder(PurchaseOrder newPurchaseOrder) {
    this.newPurchaseOrder = newPurchaseOrder;
  }

  public void setUser(User user) {
    this.user = user;
  }

  @NonNull
  @Override
  public Fragment createFragment(int position) {
    if (newPurchaseOrder != null)
      return MainOrderRestaurantFragment.newInstance(position,user,newPurchaseOrder);
    else
      return MainOrderRestaurantFragment.newInstance(position,user,null);
  }

  @Override
  public int getItemCount() {
    return TAB_TITLES.length;
  }
}
