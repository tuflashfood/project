package com.example.project.ui.option_menu;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.project.model.json.Menu;

import java.util.HashMap;

public class OptionMenuViewModel extends ViewModel {

  private MutableLiveData<HashMap<String,Menu>> mutableLiveData = new MutableLiveData<>();

  public void setHashMapMutableLiveData(HashMap<String,Menu> mutableLiveData) {
    this.mutableLiveData.setValue(mutableLiveData);
  }

  public MutableLiveData<HashMap<String,Menu>> getHashMapMutableLiveData() {
    return mutableLiveData;
  }

}
