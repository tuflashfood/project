package com.example.project.ui.main_restaurant;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.project.model.json.User;

public class MainRestaurantViewModel extends ViewModel {
  // TODO: Implement the ViewModel
  private MutableLiveData<User> userMutableLiveData = new MutableLiveData<>() ;

  public MutableLiveData<User> getUserMutableLiveData() {
    return userMutableLiveData;
  }

  public void setUserMutableLiveData(User userMutableLiveData) {
    this.userMutableLiveData.setValue(userMutableLiveData);
  }
}
