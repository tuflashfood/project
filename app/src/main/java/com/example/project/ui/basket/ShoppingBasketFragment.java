package com.example.project.ui.basket;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.icu.text.SimpleDateFormat;
import android.icu.util.TimeZone;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.R;
import com.example.project.adapter.ShoppingBasketAdapter;
import com.example.project.model.json.Menu;
import com.example.project.model.json.OrderBuy;
import com.example.project.model.json.ResponseBody;
import com.example.project.service.LocationService;
import com.example.project.ui.select_restaurant.SelectRestaurantViewModel;

import org.parceler.Parcels;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ShoppingBasketFragment extends Fragment implements SensorEventListener {
  private static final String SEND_ORDER_ARG_BUNDLE = "Orderbuy";
  private static String TAG = "ShoppingBasketFragment";

  private List<Menu> menu;
  private OrderBuy orderBuy;
  private SelectRestaurantViewModel selectResViewModel;
  private ShoppingBasketViewModel shoppingBasketViewModel;
  private RecyclerView recyclerView;
  private Dialog dialog;
  private TextView value_priceTotal;
  private TextView value_priceSender;
  private TextView value_billing;
  private TextView basket_amount_all;
  private TextView textView_locationSender;
  private TextView basket_totalMenuPrice;
  private TextView time_user_select;
  private LinearLayout linear_setOrder;
  private ShoppingBasketAdapter shoppingBasketAdapter;
  private String date_pic[];
  private String time_pic[];
  private NumberPicker date_picker;
  private NumberPicker time_picker;
  private LocationService locationService;
  private String finalTime;
  private String latlong;
  private String nameLocation;
  private ImageView imageView_close_basket;

  private static ShoppingBasketFragment newInstance() {
    return new ShoppingBasketFragment();
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    final View view = inflater.inflate(R.layout.shoppingbasket_fragment, container, false);
    locationService = LocationService.getLocationManager(getContext());
    latlong = locationService.getLatitude() + "," + locationService.getLongitude();
    ImageView imageViewTimeSender = view.findViewById(R.id.image_time_sender);
    imageViewTimeSender.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        createDialog();
        dialog.show();
      }
    });
    value_priceTotal = view.findViewById(R.id.value_priceTotal);
    textView_locationSender = view.findViewById(R.id.textView_locationSender);
    value_priceSender = view.findViewById(R.id.value_priceSender);
    value_billing = view.findViewById(R.id.value_billing);
    basket_amount_all = view.findViewById(R.id.basket_amount_all);
    basket_totalMenuPrice = view.findViewById(R.id.basket_totalMenuPrice);
    linear_setOrder = view.findViewById(R.id.linear_setOrder);
    time_user_select = view.findViewById(R.id.time_user_select);
    imageView_close_basket = view.findViewById(R.id.imageView_close_basket);
    imageView_close_basket.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Navigation.findNavController(getView()).popBackStack(R.id.nav_shoppingBasketFragment, true);
      }
    });
    linear_setOrder.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        createDialogBuyOrder();
      }
    });
    initDataOrderBuyFromBundle();
    setRecyclerViewShoppingBasket(view);
    return view;
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);

    this.shoppingBasketViewModel = ViewModelProviders.of(requireActivity()).get(ShoppingBasketViewModel.class);
    this.selectResViewModel = ViewModelProviders.of(requireActivity()).get(SelectRestaurantViewModel.class);
    selectResViewModel.getOrderBuy().observe(getViewLifecycleOwner(), order -> {
      orderBuy = order;
      finalTime = orderBuy.getTransportCost() + "นาที(เดี่ยวนี้)";
      time_user_select.setText(finalTime);
      setTextOrderBuyUpdate();
    });

    shoppingBasketViewModel.getFetchCurrentLocation(latlong).observe(getViewLifecycleOwner(), responseBody -> {
      Log.i(TAG, "onChanged: " + responseBody);
      if (responseBody != null) {
        nameLocation = responseBody.getMessage();
        textView_locationSender.setText(nameLocation);
      }
    });
    Log.i(TAG, "onCreateView: " + locationService.getLatitude());
    Log.i(TAG, "onCreateView: " + locationService.getLongitude());
  }

  public void createDialogBuyOrder() {
    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    LinearLayout linearLayout = new LinearLayout(getContext());
    linearLayout.setBackgroundColor(getResources().getColor(R.color.color_red));
    linearLayout.setPadding(0,10,0,0);
    linearLayout.setGravity(Gravity.CENTER);
    ImageView imageView = new ImageView(getContext());
    int dp25 = (int)pxFromDp(getContext(),25);
    imageView.setLayoutParams(new LinearLayout.LayoutParams(dp25,dp25));
    imageView.setImageResource(R.drawable.signs);
    TextView title = new TextView(getContext());
    title.setTextColor(getResources().getColor(R.color.colorWhite));
    title.setText(" โปรดตรวจสอบ");
    title.setGravity(Gravity.CENTER);
    title.setPadding(30,0,0,0);
    title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
    linearLayout.setOrientation(LinearLayout.HORIZONTAL);
    linearLayout.addView(imageView);
    linearLayout.addView(title);
    builder.setCustomTitle(linearLayout) ;
    builder.setMessage("คุณจะดำเนินการสั่งซื้อใช่หรือไม่");
    builder.setCancelable(false);
    builder.setNegativeButton("ใช่", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        if (time_picker == null || (time_picker.getValue() == 0 && date_picker.getValue() == 0)) {
          finalTime = getTimeCurrent();
        }
        selectResViewModel.sendDataOrderToServer(finalTime, latlong, nameLocation).observe(getViewLifecycleOwner(), responseBody -> {
          Log.i(TAG, "onChanged: " + responseBody.getMessage());
          if (responseBody.getMessage().equals("Insert to database sucess")) {
            Toast.makeText(getContext(), "ส่งสำเร็จแล้ว", Toast.LENGTH_SHORT).show();
            Navigation.findNavController(getView()).popBackStack(R.id.nav_shoppingBasketFragment, true);
            orderBuy.clearOrder();
            selectResViewModel.setOrderBuy(orderBuy);
          } else {
            Toast.makeText(getContext(), "กรุณากดใหม่อีกครั้ง", Toast.LENGTH_SHORT).show();
          }
        });
      }
    });
    builder.setPositiveButton("ยกเลิก", null);
    AlertDialog dialog = builder.create();
    dialog.show();
    TextView messageText = (TextView)dialog.findViewById(android.R.id.message);
    messageText.setTypeface(ResourcesCompat.getFont(getContext(), R.font.layijimahaniyom));
    messageText.setPadding(0,35,0,0);
    messageText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
    messageText.setGravity(Gravity.CENTER);
  }

  public void setTextOrderBuyUpdate() {
    int amount = orderBuy.getTotalAmount();
    if (amount == 0)
      Navigation.findNavController(getView()).popBackStack(R.id.nav_shoppingBasketFragment, true);
    value_priceTotal.setText(orderBuy.getTotalPrice() + " ฿");
    value_priceSender.setText(orderBuy.getTransportCost() + " ฿");
    value_billing.setText(calcurateBillingCost() + " ฿");
    basket_amount_all.setText(amount + "");
    basket_totalMenuPrice.setText(orderBuy.getBillingCost() + " ฿");
  }

  public String calcurateBillingCost() {
    int totalBilling = orderBuy.getTotalPrice() + Integer.parseInt(orderBuy.getTransportCost());
    orderBuy.setBillingCost(totalBilling + "");
    Log.i(TAG, "calcurateBillingCost: " + orderBuy);
    return orderBuy.getBillingCost();
  }

  public void initDataOrderBuyFromBundle() {
    //get data from selectRestaurant with bundle key is Restaurant
    if (getArguments() != null) {
      OrderBuy order = Parcels.unwrap(getArguments().getParcelable(SEND_ORDER_ARG_BUNDLE));
      if (order != null) {
        orderBuy = order;
        setTextOrderBuyUpdate();
      }
    }
  }

  public String getTimeCurrent() {
    String time = "";
    SimpleDateFormat isoFormat = new SimpleDateFormat("EEEEE dd MMMMM HH:mm", new Locale("th"));
    time = isoFormat.format(new Date());
    Log.i(TAG, "getTimeCurrent: " + time);
    return time;
  }

  public void setRecyclerViewShoppingBasket(View view) {
    recyclerView = (RecyclerView) view.findViewById(R.id.basket_recycler_view);
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    shoppingBasketAdapter = new ShoppingBasketAdapter(view.getContext(), orderBuy.getMenu());
    shoppingBasketAdapter.setOrderBuy(orderBuy);
    recyclerView.setAdapter(shoppingBasketAdapter);
  }

  public static int getWidth(Context context) {
    DisplayMetrics displayMetrics = new DisplayMetrics();
    WindowManager windowmanager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
    return displayMetrics.widthPixels;
  }

  public String[] setTimeSend(String timeStart, String timeEnd) {
    Log.i(TAG, "setTimeSend: " + timeStart + " " + timeEnd);
    int timeS = Integer.parseInt(timeStart.split(":")[0]);
    int timeSmm = Integer.parseInt(timeStart.split(":")[1]);
    int timeE = Integer.parseInt(timeEnd.split(":")[0]);
    int timeEmm = Integer.parseInt(timeEnd.split(":")[1]);
    int round = timeE - timeS + 1;
    String textResult = "";
    int textMM[] = {0, 15, 30, 45};
    for (int i = 0; i < round; i++) {
      for (int j = 0; j < textMM.length; j++) {
        if (i == 0 && textMM[j] - timeSmm < 0)
          continue;
        if (i == round - 1 && (timeS + i) * 60 + textMM[j] > (timeE * 60) + timeEmm)
          break;
        if (j == 0)
          textResult += timeS + i + ":" + textMM[j] + "0 ";
        else
          textResult += timeS + i + ":" + textMM[j] + " ";
      }
    }
    String result[] = textResult.split(" ");
    return result;
  }

  public String[] initDate(int amountDay, Date now) {
    int timeDay = 1000 * 60 * 60 * 24;
    SimpleDateFormat isoFormat = new SimpleDateFormat("EEEEE dd MMMMM", new Locale("th"));
    isoFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
    String result[] = new String[amountDay];
    result[0] = isoFormat.format(now);
    Log.i(TAG, "initDate: " + result[0]);
    for (int i = 1; i < amountDay; i++) {
      now.setTime(now.getTime() + (timeDay) * i);
      result[i] = isoFormat.format(now);
    }
    return result;
  }

  public int textTimeStringToInt(String time) {
    String splits[] = time.split(":");
    return (Integer.parseInt(splits[0]) * 60) + Integer.parseInt(splits[1]);
  }

  public void createDialog() {
    final SimpleDateFormat isoFormat = new SimpleDateFormat("HH:mm");
    final Date date = new Date();
    Log.i(TAG, "createDialog: " + date);
    date_pic = initDate(1, date);
    // fix time res close
    if (textTimeStringToInt("08:30") > textTimeStringToInt(isoFormat.format(date))) {
      time_pic = setTimeSend("08:30", "21:30");
    } else {
      time_pic = setTimeSend(isoFormat.format(date), "21:30");
      time_pic[0] = "เดี่ยวนี้";
    }
    dialog = new Dialog(getContext());
    //createLayout
    ColorDrawable back = new ColorDrawable(Color.WHITE);
    //set margin 0
    InsetDrawable inset = new InsetDrawable(back, 0);
    dialog.getWindow().setBackgroundDrawable(inset);
    dialog.setContentView(R.layout.select_time);
    dialog.getWindow().setGravity(Gravity.BOTTOM);
    dialog.getWindow().setLayout(getWidth(getContext()), LinearLayout.LayoutParams.WRAP_CONTENT);

    date_picker = (NumberPicker) dialog.findViewById(R.id.date_picker);
    time_picker = (NumberPicker) dialog.findViewById(R.id.time_picker);

    date_picker.setDisplayedValues(date_pic);
    date_picker.setWrapSelectorWheel(false);
    date_picker.setMinValue(0);
    date_picker.setMaxValue(date_pic.length - 1);
    date_picker.setOnValueChangedListener((picker, oldVal, newVal) -> {
      if (newVal == 0) {
        time_pic = setTimeSend(isoFormat.format(new Date()), "21:30");
        time_pic[0] = "เดี่ยวนี้";
      } else {
        // fix res open ;
        time_pic = setTimeSend("10:30", "21:30");
      }
      time_picker.setValue(0);
      time_picker.setMaxValue(time_pic.length - 1);
      time_picker.setDisplayedValues(time_pic);
      date_picker.setMinValue(0);
    });
    time_picker.setMinValue(0);
    time_picker.setMaxValue(time_pic.length - 1);
    time_picker.setDisplayedValues(time_pic);
    time_picker.setWrapSelectorWheel(false);

    //init variable file layout select time
    TextView cancel_dialog = (TextView) dialog.findViewById(R.id.cancel_dialog);
    cancel_dialog.setOnClickListener(v -> {
      //close dialog
      dialog.dismiss();
    });
    TextView agree_dialog = (TextView) dialog.findViewById(R.id.agree_dialog);
    agree_dialog.setOnClickListener(v -> {
      //chang data to user select
      dialog.dismiss();
      String text;
      if (date_picker.getValue() == 0 && time_picker.getValue() == 0)
        text = orderBuy.getTransportCost() + "นาที" + "(เดี่ยวนี้)";
      else
        text = date_pic[date_picker.getValue()] + " " + time_pic[time_picker.getValue()];
      Log.i(TAG, "onClick: adsf" + text);
      time_user_select.setText(text);
      finalTime = text;
    });
  }

  public static float pxFromDp(final Context context, final float dp) {
    return dp * context.getResources().getDisplayMetrics().density;
  }

  @Override
  public void onSensorChanged(SensorEvent event) {

  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) {

  }
}
