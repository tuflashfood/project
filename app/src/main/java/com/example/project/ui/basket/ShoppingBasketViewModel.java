package com.example.project.ui.basket;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.project.model.ShoppingBasketRepository;
import com.example.project.model.json.ResponseBody;

public class ShoppingBasketViewModel extends ViewModel {
  private ShoppingBasketRepository shoppingBasketRepository = new ShoppingBasketRepository() ;
  private MutableLiveData<ResponseBody> location = new MutableLiveData<>();

  public MutableLiveData<ResponseBody> getLocation() {
    return location;
  }

  public MutableLiveData<ResponseBody> getFetchCurrentLocation(String latlong) {
    location = shoppingBasketRepository.getFetchCurrentLocation(latlong) ;
    return location ;
  }
}
