package com.example.project.ui.main_restaurant;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.project.R;
import com.example.project.model.json.PurchaseOrder;
import com.example.project.model.json.User;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import org.parceler.Parcels;

public class MainRestaurantFragment extends Fragment {

  private MainRestaurantViewModel mViewModel;
  private ViewPager2 viewPager;
  private TabLayout tabLayout;
  private SectionsPagerResAdapter sectionsPagerResAdapter ;
  private static final String ARG_BUNDLE_USER = "ARG_BUNDLE_USER";
  private User user;
  private PurchaseOrder newPurchaseOrder;
  private static final String TAG = "MainRestaurantFragment";

  public static MainRestaurantFragment newInstance() {
    return new MainRestaurantFragment();
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.main_restaurant_fragment, container, false);
    viewPager = view.findViewById(R.id.res_view_pager);
    tabLayout = view.findViewById(R.id.res_tabs);
    initDataUserFromBundle();
    setViewPagerAndTab();
    return view ;
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    mViewModel = ViewModelProviders.of(requireActivity()).get(MainRestaurantViewModel.class);
    mViewModel.setUserMutableLiveData(this.user);
    // TODO: Use the ViewModel
  }

  public void setViewPagerAndTab() {
    //set getFragmentManager() in SectionsPagerAdapter bug
    int[] TAB_TITLES = new int[]{R.string.tab_resNotYetReceivedTheOrder, R.string.tab_resReceivedTheOrder, R.string.tab_resOrderSuccess};
    sectionsPagerResAdapter = new SectionsPagerResAdapter(getActivity());
    if(newPurchaseOrder != null) sectionsPagerResAdapter.setNewPurchaseOrder(newPurchaseOrder);
    sectionsPagerResAdapter.setUser(this.user);
    viewPager.setAdapter(sectionsPagerResAdapter);
    new TabLayoutMediator(tabLayout, viewPager,
        (tab, position) -> tab.setText(getResources().getString(TAB_TITLES[position]))
    ).attach();
  }

  public void initDataUserFromBundle() {
    Bundle extras = getArguments();
    if (extras != null) {
      if (extras.containsKey(ARG_BUNDLE_USER)) {
        this.user = Parcels.unwrap(extras.getParcelable(ARG_BUNDLE_USER));
//        Log.i(TAG, "onReceive: " + user);
      }
      if (extras.containsKey("data")) {
        this.newPurchaseOrder = Parcels.unwrap(extras.getParcelable("data"));
        Log.i(TAG, "initDataUserFromBundle: " + this.newPurchaseOrder);
        //fix bug background after time out
        extras.remove("data");
      }
//      if (extras.containsKey("time")) {
//        this.textTime = extras.getString("time");
//        totalTimeFromBackground = getDelayAfterUserOpenNotification(textTime);
//        Log.i(TAG, "initDataUserFromBundle: " + totalTimeFromBackground);
//        Log.i(TAG, "onReceive: " + textTime);
//      }

    }

//    for (String key : extras.keySet()){
//      Log.i(TAG, "initDataUserFromBundle: " + key);
//    }
  }
}
