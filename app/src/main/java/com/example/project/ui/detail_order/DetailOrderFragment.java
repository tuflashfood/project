package com.example.project.ui.detail_order;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.interfaces.TalkMainActivity;
import com.example.project.model.NotificationHelper;
import com.example.project.model.json.ResponseBody;
import com.example.project.model.json.FoodOrder;
import com.example.project.model.json.Location;
import com.example.project.model.json.Order;
import com.example.project.model.json.OrderStatus;
import com.example.project.model.json.PurchaseOrder;
import com.example.project.model.json.User;
import com.example.project.service.LocationService;
import com.example.project.ui.main_order_restaurant.MainOrderRestaurantViewModel;
import com.example.project.ui.main_restaurant.MainRestaurantViewModel;
import com.example.project.ui.sender.SenderViewModel;
import com.google.gson.JsonObject;

import org.parceler.Parcels;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class DetailOrderFragment extends Fragment {
  private String statusOrderSender[] = {"รอกดยืนยัน", "ยืนยันรับออเดอร์", "รับสินค้าจากทางร้าน"};
  private String senderButton[] = {"รับออเดอร์", "รับสินค้าจากทางร้านเรียบร้อย", "ผู้สั่งได้รับเรียบร้อย"};
  private String statusOrderRestaurant[] = {"รอกดยืนยันออเดอร์", "รับออเดอร์", "ทำออเดอร์สำเร็จ"};
  private String restaurantButton[] = {"ออเดอร์", "ทำออเดอร์สำเร็จ"};
  private SenderViewModel senderViewModel;
  private MainRestaurantViewModel mainRestaurantViewModel;
  private MainOrderRestaurantViewModel mainOrderRestaurantViewModel;
  private DetailOrderViewModel mViewModel;
  private LinearLayout linear_menu_order;
  private Button buttonAgree;
  private Button buttonCancel;
  private TextView texView_time_order;
  private TextView detail_order;
  private TextView textView_sender;
  private TextView textViewStatusOrder;
  private TextView order_name_res;
  private TextView textView_phone;
  private static final String ARG_BUNDLE_ISNEWORDER = "isneworder";
  private static final String ARG_BUNDLE_PHONE = "ARG_BUNDLE_PHONE";
  private static final String ARG_BUNDLE_VIEWDATA = "viewdate";
  private static final String ARG_BUNDLE_PURCHASEORDER = "ARG_BUNDLE_PURCHASEORDER";
  private static final String TAG = "DetailOrderFragment";
  private Boolean resIsNewOrder = false;
  private String isView = "";
  private String phoneUser;
  private PurchaseOrder purchaseOrder;
  private List<PurchaseOrder> agreeOrder = new ArrayList<>();
  private List<PurchaseOrder> purchaseOrderListRestaurant = new ArrayList<>();
  private LocationService locationService;
  private Locale locale;
  private SimpleDateFormat isoFormat;
  private User user;
  private HashMap<String, OrderStatus> orderStatusHashMap = null;
  private OrderStatus orderStatus;
  private View view;
  private CallBackToSenderFragment callBackToSenderFragment;

  public static DetailOrderFragment newInstance() {
    return new DetailOrderFragment();
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    view = inflater.inflate(R.layout.detail_order_fragment, container, false);
    locale = new Locale("th");
    isoFormat = new SimpleDateFormat("EEEEE dd MMMMM HH:mm", locale);
    linear_menu_order = view.findViewById(R.id.linear_menu_order);
    buttonAgree = view.findViewById(R.id.button_agree);
    buttonCancel = view.findViewById(R.id.button_cancel);
    texView_time_order = view.findViewById(R.id.texView_time_order);
    textViewStatusOrder = view.findViewById(R.id.textViewStatusOrder);
    textView_sender = view.findViewById(R.id.textView_sender);
    order_name_res = view.findViewById(R.id.order_name_res);
    detail_order = view.findViewById(R.id.detail_order);
    textView_phone = view.findViewById(R.id.textView_phone);
    initDataPurchaseOrderFromBundle();
    intiMenuFromPurchaseOrder();
    return view;
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    // TODO: Use the ViewModel
    Log.i(TAG, "onActivityCreated: " + "asjdkfl;a");
    mViewModel = ViewModelProviders.of(requireActivity()).get(DetailOrderViewModel.class);
    Date now = new Date();
    boolean bool = true;
    final String sTime = isoFormat.format(now);
//    mViewModel.setResultMessageRestaurantSuccess(null);
    mViewModel.getResultMessageRestaurantSuccess().observe(getViewLifecycleOwner(), responseBody -> {
      if (responseBody == null) return ;
      if (responseBody.getMessage().equals("true")) {
        Log.i(TAG, "onActivityCreated: " + "true");
        Log.i(TAG, "onActivityCreated: " + responseBody);
        mViewModel.sendUpdateStatusOrder(purchaseOrder.getOrder().getOrderId() + "", getStringStatusOrder("shipping"));
        orderStatusHashMap = purchaseOrder.getOrder().getOrder_status();
        orderStatus = orderStatusHashMap.get("shipping");
        orderStatus.setValue(true);
        orderStatus.setTime(sTime);
        // fix bug await server
        checkStateOrderSender(purchaseOrder.getOrder().getOrder_status(), purchaseOrder.getOrder().getSenderTypeId());
        //fix reset mutable data
        mViewModel.getResultMessageRestaurantSuccess().postValue(null);
      } else {
        Toast.makeText(getContext(), "กรุณารอรับสินค้าด้วยค่ะ", Toast.LENGTH_SHORT).show();
      }
    });
    senderViewModel = ViewModelProviders.of(requireActivity()).get(SenderViewModel.class);
    mainOrderRestaurantViewModel = ViewModelProviders.of(requireActivity()).get(MainOrderRestaurantViewModel.class);
    mainOrderRestaurantViewModel.getSetPopBackStackDetailOrderFragement().observe(getViewLifecycleOwner(), aBoolean -> {
      if (aBoolean)
        Navigation.findNavController(getView()).popBackStack(R.id.nav_restaurant_order_detail, true);
    });
    senderViewModel.getUserMutableLiveData().observe(getViewLifecycleOwner(), mUser -> {
      user = mUser;
      detailOrderSender();
    });
    mainRestaurantViewModel = ViewModelProviders.of(requireActivity()).get(MainRestaurantViewModel.class);
    mainOrderRestaurantViewModel.getAgreeOrder().observe(getViewLifecycleOwner(), purchaseOrders -> {
      agreeOrder = purchaseOrders;
    });
    mainOrderRestaurantViewModel.getNewOrder().observe(getViewLifecycleOwner(), purchaseOrder -> purchaseOrderListRestaurant = purchaseOrder);
    mainRestaurantViewModel.getUserMutableLiveData().observe(getViewLifecycleOwner(), mUser -> {
      user = mUser;
      Log.i(TAG, "onActivityCreated: isView " + isView);
      if (isView.equals("viewdata"))
        detailOrderView();
      else
        detailOrderRestaurant();
    });
    buttonAgree.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String nameButton = buttonAgree.getText().toString();
        if (user.getType().equals("sender")) {
          buttonOnClickSender(nameButton);
          checkStateOrderSender(purchaseOrder.getOrder().getOrder_status(), purchaseOrder.getOrder().getSenderTypeId());
        } else {
          buttonOnClickRestaurant(nameButton);
          checkStateOrderRestaurant(purchaseOrder.getOrder(), resIsNewOrder);
        }
      }
    });
    buttonCancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String nameButton = buttonCancel.getText().toString();
        if (user.getType().equals("sender")) {
          if (nameButton.equals("นำทางไปร้านอาหาร"))
            openGoogleMap(purchaseOrder.getLocation());
          else if (nameButton.equals("นำทางไปส่งสินค้า"))
            openGoogleMap(purchaseOrder.getOrder().getDestination_location());
          else
            buttonOnClickSender(buttonCancel.getText().toString());
        } else {
          buttonOnClickRestaurant(nameButton);
        }
      }
    });

//    detailOrderSender() ;
  }

  public String getStringStatusOrder(String nameOrder) {
    SimpleDateFormat isoFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm", locale);
    Date now = new Date();
    JsonObject main = new JsonObject();
    JsonObject orderStatusJ = new JsonObject();
    orderStatusJ.addProperty("value", "true");
    orderStatusJ.addProperty("time", isoFormat.format(now));
    main.add(nameOrder, orderStatusJ);
    return main.toString();
  }

  public int checkStatusOrder(HashMap<String, OrderStatus> stringHashMap) {
    int countStatus = 0;
    for (String s : stringHashMap.keySet()) {
      if (stringHashMap.get(s).isValue()) {
        countStatus++;
      }
    }
    return countStatus;
  }

  public void detailOrderRestaurant() {
    initDetailRestaurant();
  }

  public void buttonOnClickRestaurant(String nameButton) {
    switch (nameButton) {
      case "รับออเดอร์":
        //sender data
        if (checkTimeNotificationDelay(purchaseOrder.getOrder().getOrder_status().get("order").getTime())) {
          Log.i(TAG, "buttonOnClickRestaurant: " + "รับออเดอร์");
          NotificationHelper.scheduleRepeatingRTCNotification(getContext(), purchaseOrder.getOrder().getOrder_status().get("order").getTime(), purchaseOrder.getOrder().getOrderId() + "");
        } else {
          mViewModel.sendSearchSender(purchaseOrder.getOrder().getOrderId() + "");
          Log.i(TAG, "buttonOnClickRestaurant: " + "ไม่รับออเดอร์");
        }
        mViewModel.sendUpdateStatusOrder(purchaseOrder.getOrder().getOrderId() + "", getStringStatusOrder("prepare_products"))
            .observe(getViewLifecycleOwner(), responseBody -> Log.i(TAG, "onChanged: " + responseBody));

        removeOrder(purchaseOrderListRestaurant);
        break;
      case "ปฏิเสธออเดอร์":
        //remove order data
        removeOrder(purchaseOrderListRestaurant);
        mViewModel.sendRemoveOrderSender(purchaseOrder.getOrder().getOrderId() + "");
        Navigation.findNavController(getView()).popBackStack(R.id.nav_restaurant_order_detail, true);
        break;
      case "ทำออเดอร์สำเร็จ":
//        if (getTimeCookOrder(purchaseOrder.getOrder().getOrder_status().get("order").getTime())) {
          removeOrderAgree(agreeOrder);
          mViewModel.sendUpdateOrderRestaurant(purchaseOrder.getOrder().getOrderId() + "", "true");
          Navigation.findNavController(getView()).popBackStack(R.id.nav_restaurant_order_detail, true);
//        } else {
//          Toast.makeText(getContext(), "ยังไม่ถึงเวลา", Toast.LENGTH_SHORT).show();
//        }
        break;
    }
  }

  public boolean checkTimeNotificationDelay(String timeStartOrder) {
    SimpleDateFormat isoFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm", locale);
    Date dateStartOrder = null, timeCurrent = null;
    timeCurrent = new Date();
    dateStartOrder = changeStringDateToDate(timeStartOrder);
    long timeLeft = dateStartOrder.getTime() - timeCurrent.getTime();
    return timeLeft > 0;
  }

  public void removeOrderAgree(List<PurchaseOrder> purchase) {
    for (int i = purchase.size() - 1; i >= 0; i--) {
      if (purchase.get(i).getOrder().getOrderId() == purchaseOrder.getOrder().getOrderId()) {
        purchase.remove(i);
        break;
      }
    }
    mainOrderRestaurantViewModel.setAgreeOrder(purchase);
  }


  public void removeOrder(List<PurchaseOrder> purchase) {
    for (int i = purchase.size() - 1; i >= 0; i--) {
      if (purchase.get(i).getOrder().getOrderId() == purchaseOrder.getOrder().getOrderId()) {
        purchase.remove(i);
        break;
      }
    }
    mainOrderRestaurantViewModel.setNewOrder(purchase);
  }

  public Date changeStringDateToDate(String stringDate) {
    SimpleDateFormat isoFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm", locale);
    Date date = null;
    try {
      date = isoFormat.parse(stringDate);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return date;
  }

  public boolean getTimeCookOrder(String time) {
    Date timeCurrent = null, timeCook = null;
    timeCurrent = new Date();
    timeCook = changeStringDateToDate(time);

    Log.i(TAG, "getTimeCookOrder: " + timeCurrent);
    Log.i(TAG, "getTimeCookOrder: " + timeCook);
    return timeCurrent.getTime() > (timeCook.getTime() - 15 * 60 * 1000);
  }

  public void checkStateOrderRestaurant(Order order, boolean isNewOrder) {
    if (order.isOrder_restaurant()) {
      textViewStatusOrder.setText(statusOrderRestaurant[2]);
      buttonAgree.setVisibility(View.GONE);
      buttonCancel.setVisibility(View.GONE);
    } else {
      Log.i(TAG, "checkStateOrderRestaurant: " + isNewOrder);
      if (isNewOrder) {
        textViewStatusOrder.setText(statusOrderRestaurant[0]);
        buttonAgree.setText("รับ" + restaurantButton[0]);
        buttonCancel.setText("ปฏิเสธ" + restaurantButton[0]);
        resIsNewOrder = false;
      } else {
        textViewStatusOrder.setText(statusOrderRestaurant[1]);
        buttonAgree.setText(restaurantButton[1]);
        buttonCancel.setVisibility(View.GONE);
      }
    }
  }

  public void initDetailRestaurant() {
    initDetailOrder();
    checkStateOrderRestaurant(purchaseOrder.getOrder(), resIsNewOrder);
//    setValueButton()
  }

  public void initDetailOrder() {
    detail_order.setText("เลขใบสั่งซื้อ: #" + String.format("%05d", purchaseOrder.getOrder().getOrderId()));
    order_name_res.setText("● รับอาหารที่ร้าน: " + purchaseOrder.getRes_name());
    textView_sender.setText("● " + purchaseOrder.getOrder().getDestinationLocationName());
    String timeParse = purchaseOrder.getOrder().getOrder_status().get("order").getTime();
    texView_time_order.setText("● " + changeDateToString(timeParse));
  }

  public void detailOrderView() {
    initDetailOrder();
    buttonCancel.setVisibility(View.GONE);
    buttonAgree.setVisibility(View.GONE);
    Log.i(TAG, "detailOrderView: " + purchaseOrder.getOrder().isOrder_restaurant());
    if (purchaseOrder.getOrder().isOrder_complete()) {
      textViewStatusOrder.setText("ผู้สั่งได้รับเรียบร้อย");
    }else if(purchaseOrder.getOrder().isOrder_restaurant()){
      textViewStatusOrder.setText("ทำออเดอร์สำเร็จ");
    }
  }

  public void detailOrderSender() {
    senderViewModel.getTimeOutOrder().observe(getViewLifecycleOwner(), aBoolean -> {
      if (aBoolean) {
        Navigation.findNavController(view).popBackStack();
      }
    });
    SimpleDateFormat isoFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm", locale);

    textView_phone.setVisibility(View.VISIBLE);
    textView_phone.setText("● เบอร์คนสั่งสินค้า " + purchaseOrder.getPhone());
    initDetailSender();

  }

  public void checkStateOrderSender(HashMap<String, OrderStatus> stringHashMap, int senderId) {
    if (senderId == 0) {
      setValueButton("ยืนยัน" + senderButton[0], "ปฏิเสธ" + senderButton[0]);
      textViewStatusOrder.setText(statusOrderSender[0]);
    } else {
      buttonCancel.setBackgroundTintList(getResources().getColorStateList(R.color.colorYellow));
      Log.i(TAG, "checkStateOrderSender: " + stringHashMap.get("shipping"));
      Log.i(TAG, "checkStateOrderSender: " + stringHashMap.get("delivered"));
      if (!stringHashMap.get("shipping").isValue()) {
        buttonCancel.setText("นำทางไปร้านอาหาร");
        buttonAgree.setText(senderButton[1]);
        textViewStatusOrder.setText(statusOrderSender[1]);
      } else if (!stringHashMap.get("delivered").isValue()) {
        buttonCancel.setText("นำทางไปส่งสินค้า");
        buttonAgree.setText(senderButton[2]);
        textViewStatusOrder.setText(statusOrderSender[2]);
      }
    }
  }

  public boolean buttonOnClickSender(String stringButton) {
    SimpleDateFormat isoFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm", locale);
    Date now = new Date();
    boolean bool = true;
    final String sTime = isoFormat.format(now);

    switch (stringButton) {
      case "ยืนยันรับออเดอร์":
        purchaseOrder.getOrder().setSenderTypeId(Integer.parseInt(user.getUser_id()));
        mViewModel.setAnswerOrder("ยืนยันรับออเดอร์");
        mViewModel.sendAnswerToSender(user.getUser_id(), purchaseOrder.getOrder().getOrderId() + "", "accept");
        senderViewModel.setSetSwitchCompat(false);
//        mViewModel.getResultMessageUpdateSenderIdOrder().observe(getViewLifecycleOwner(),responseBody -> {
//          Log.i(TAG, "buttonOnClickSender: " + responseBody);
//          if (responseBody.getMessage().equals("send answer success")) {
//          } else {
//            Log.i(TAG, "buttonOnClickSender: " + "sendAnswerToSender fail");
//          }
//        });
        break;
      case "ปฏิเสธรับออเดอร์":
        mViewModel.setAnswerOrder("ปฏิเสธรับออเดอร์");
        mViewModel.sendAnswerToSender(user.getUser_id(), purchaseOrder.getOrder().getOrderId() + "", "unaccept");
        Navigation.findNavController(getView()).popBackStack(R.id.nav_sender_order_detail, true);
        break;
      case "รับสินค้าจากทางร้านเรียบร้อย":
        //check order restaurant success
        mViewModel.fetchCheckOrderRestaurantOrderSuccess(purchaseOrder.getOrder().getOrderId() + "");
       break;
      case "ผู้สั่งได้รับเรียบร้อย":
        mViewModel.sendUpdateStatusOrder(purchaseOrder.getOrder().getOrderId() + "", getStringStatusOrder("delivered"));
        orderStatusHashMap = purchaseOrder.getOrder().getOrder_status();
        orderStatus = orderStatusHashMap.get("delivered");
        orderStatus.setValue(true);
        orderStatus.setTime(sTime);
        mViewModel.sendUpdateOrderComplete(purchaseOrder.getOrder().getOrderId() + "", "true");
        Navigation.findNavController(getView()).popBackStack(R.id.nav_sender_order_detail, true);
        break;
    }
    if (orderStatusHashMap != null) {
      purchaseOrder.getOrder().setOrder_status(orderStatusHashMap);
    }
    return bool;
  }

  private void openGoogleMap(Location location) {
//    String url = "http://maps.google.com/maps?saddr=" + "Your%20Location" + "&daddr=" + location.getX() + "," + location.getY() + "&mode=driving";
    String url = "http://maps.google.com/maps?saddr=" + "14.073313,100.601188" + "&daddr=" + location.getX() + "," + location.getY() + "&mode=driving";
    Uri gmmIntentUri = Uri.parse(url);
    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
    mapIntent.setPackage("com.google.android.apps.maps");
    startActivity(mapIntent);
  }

  public void initDetailSender() {
    initDetailOrder();
    checkStateOrderSender(purchaseOrder.getOrder().getOrder_status(), purchaseOrder.getOrder().getSenderTypeId());
  }

  public String changeDateToString(String timeParse) {
    Date date = null;
    try {
      date = new SimpleDateFormat("dd/MM/yyyy - HH:mm", locale).parse(timeParse);
      String sDate = new SimpleDateFormat("dd MMMM yyyy", locale).format(date);
      String sTime = new SimpleDateFormat("HH:mm", locale).format(date);
      String timeOrder = "รับออเดอร์: " + sDate + ", " + "เวลา " + sTime + " น.";
//      Log.i(TAG, "onActivityCreated: " + timeOrder);
      if (!timeOrder.equals("")) return timeOrder;
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return null;
  }

  public void initDataPurchaseOrderFromBundle() {
    Bundle extras = getArguments();
    if (extras != null) {
      if (extras.containsKey(ARG_BUNDLE_PURCHASEORDER)) {
        this.purchaseOrder = Parcels.unwrap(extras.getParcelable(ARG_BUNDLE_PURCHASEORDER));
//        Log.i(TAG, "onReceive: " + this.purchaseOrder);
      }
      if (extras.containsKey(ARG_BUNDLE_ISNEWORDER)) {
        this.resIsNewOrder = extras.getBoolean(ARG_BUNDLE_ISNEWORDER);
      }
      if (extras.containsKey(ARG_BUNDLE_VIEWDATA)) {
        this.isView = extras.getString(ARG_BUNDLE_VIEWDATA);
        extras.remove(ARG_BUNDLE_VIEWDATA);
        Log.i(TAG, "initDataPurchaseOrderFromBundle: " + isView);
      }
      if (extras.containsKey(ARG_BUNDLE_PHONE)) {
        this.phoneUser = extras.getString(ARG_BUNDLE_PHONE);
      }
    }
  }

  public void intiMenuFromPurchaseOrder() {
    Order order = this.purchaseOrder.getOrder();
    for (FoodOrder foodorder : order.getFood_order()) {
      if (!foodorder.getProduct_notes().equals(""))
        createLinearMenuOrder(foodorder.getKey() + " " + foodorder.getProduct_notes(), foodorder.getAmount(), foodorder.getTotal_price());
      else
        createLinearMenuOrder(foodorder.getKey(), foodorder.getAmount(), foodorder.getTotal_price());
    }
    createTotalMenuOrder(this.purchaseOrder.getDelivery_cost() + "", "" + order.getBillingCost());
  }

  public void setValueButton(String agree, String cancel) {
    buttonAgree.setText(agree);
    buttonCancel.setText(cancel);
  }

  public void createTotalMenuOrder(String deliveryCost, String billingCost) {
    LinearLayout.LayoutParams paraWrap = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    LinearLayout.LayoutParams paraMax = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    LinearLayout.LayoutParams paraMaxTop = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    LinearLayout mainDelivery = new LinearLayout(getContext());
    LinearLayout mainBilling = new LinearLayout(getContext());
    mainDelivery.setLayoutParams(paraMaxTop);
    mainBilling.setLayoutParams(paraMaxTop);
    paraMaxTop.setMargins(0, (int) convertDpToPixel(10), 0, 0);

    TextView textDeliveryCost = new TextView(getContext());
    textDeliveryCost.setText("ค่าส่ง");
    TextView valueDeliveryCost = new TextView(getContext());
    valueDeliveryCost.setLayoutParams(paraMax);
    valueDeliveryCost.setGravity(Gravity.RIGHT);
    valueDeliveryCost.setText(deliveryCost + " ฿");

    TextView textTotalPrice = new TextView(getContext());
    textTotalPrice.setText("ยอดรวม");
    TextView valueTotalPrice = new TextView(getContext());
    valueTotalPrice.setLayoutParams(paraMax);
    valueTotalPrice.setGravity(Gravity.RIGHT);
    valueTotalPrice.setText(billingCost + " ฿");
    mainDelivery.addView(textDeliveryCost);
    mainDelivery.addView(valueDeliveryCost);
    mainBilling.addView(textTotalPrice);
    mainBilling.addView(valueTotalPrice);
    linear_menu_order.addView(mainDelivery);
    linear_menu_order.addView(mainBilling);
  }

  public void createLinearMenuOrder(String name, String amount, String price) {
    LinearLayout main = new LinearLayout(getContext());
    int pd200ToPx = (int) convertDpToPixel(200);
    int pd20ToPx = (int) convertDpToPixel(20);
    LinearLayout.LayoutParams paraName = new LinearLayout.LayoutParams(pd200ToPx, LinearLayout.LayoutParams.WRAP_CONTENT);
    LinearLayout.LayoutParams paraAmount = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    paraAmount.setMarginStart(pd20ToPx);
    LinearLayout.LayoutParams paraPrice = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    TextView name_menu = new TextView(getContext());
    name_menu.setLayoutParams(paraName);
    TextView amount_menu = new TextView(getContext());
    amount_menu.setLayoutParams(paraAmount);
    TextView price_menu = new TextView(getContext());
    price_menu.setGravity(Gravity.RIGHT);
    price_menu.setLayoutParams(paraPrice);
    name_menu.setText(name);
    amount_menu.setText("x" + amount);
    price_menu.setText(price);
    main.addView(name_menu);
    main.addView(amount_menu);
    main.addView(price_menu);
    linear_menu_order.addView(main);
  }

  public float convertDpToPixel(float dp) {
    return dp * ((float) getContext().getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
  }

  public interface CallBackToSenderFragment {
    public void sendMessageToTimer(String message);
  }

}
