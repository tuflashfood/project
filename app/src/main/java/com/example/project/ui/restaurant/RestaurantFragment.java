package com.example.project.ui.restaurant;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.viewpager.widget.ViewPager;

import com.example.project.model.json.OrderBuy;
import com.example.project.R;
import com.example.project.interfaces.TalkMainActivity;
import com.example.project.model.json.Restaurant;
import com.example.project.ui.menu_retaurant.SectionsPagerAdapter;
import com.example.project.ui.option_menu.OptionMenuViewModel;
import com.example.project.ui.select_restaurant.SelectRestaurantViewModel;
import com.google.android.material.tabs.TabLayout;

import org.parceler.Parcels;


public class RestaurantFragment extends Fragment {
  private static final String SEND_ORDER_ARG_BUNDLE = "Orderbuy";
  private static final String SELECT_RESTAURANT_ARG_BUNDLE = "Restaurant";
  private String TAG = "RestaurantFragment";
  private TalkMainActivity talkMainActivity;
  private SectionsPagerAdapter sectionsPagerAdapter;
  private ViewPager viewPager;
  private TabLayout tabLayout;
  private LinearLayout mainResLinearShoppingฺBasket;
  private TextView text_amount_all;
  private TextView totalMenuPrice;
  private TextView textView_name_res ;
  private RestaurantViewModel resViewModel;
  private OptionMenuViewModel optMenuViewModel;
  private SelectRestaurantViewModel selectResViewModel;
  private ImageView image_close_restaurant ;
  private ImageView image_res_bar ;
  private OrderBuy orderBuy;
  private Restaurant restaurant;

  public static RestaurantFragment newInstance() {
    return new RestaurantFragment();
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.restaurant_fragment, container, false);
    image_close_restaurant = view.findViewById(R.id.image_close_restaurant);
    image_res_bar = view.findViewById(R.id.image_res_bar);
    textView_name_res = view.findViewById(R.id.textView_name_res) ;
    viewPager = view.findViewById(R.id.menu_res_view_pager);
    tabLayout = view.findViewById(R.id.menu_res_tabs);
    if(restaurant != null) {
      textView_name_res.setText(restaurant.getResName());
      image_res_bar.setImageURI(Uri.parse("android.resource://"+getContext().getPackageName()+"/drawable/"+restaurant.getMenus().getImage()+"1"));
    }
    image_close_restaurant.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Navigation.findNavController(getView()).popBackStack(R.id.nav_restaurant, true);
      }
    });
    mainResLinearShoppingฺBasket = view.findViewById(R.id.mainResLinearShoppingฺBasket);
    mainResLinearShoppingฺBasket.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //calcurate BillingCost
        Bundle bundle = new Bundle();
        bundle.putParcelable(SEND_ORDER_ARG_BUNDLE, Parcels.wrap(orderBuy));
        Navigation.findNavController(v).navigate(R.id.nav_shoppingBasketFragment,bundle);
      }
    });
    text_amount_all = view.findViewById(R.id.text_amount_all);
    totalMenuPrice = view.findViewById(R.id.totalMenuPrice);
    //disable toolbar android drawer layout
    talkMainActivity.setDisplayToolBar(false);
    talkMainActivity.setDisplayDrawerLayout(false);
    return view;
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    // TODO: Use the ViewModel
    resViewModel = ViewModelProviders.of(requireActivity()).get(RestaurantViewModel.class);
    selectResViewModel = ViewModelProviders.of(requireActivity()).get(SelectRestaurantViewModel.class);
    if (restaurant == null) {
      //assign value restaurant
      initDataRestaurantFromBundle();
    }
    selectResViewModel.getOrderBuy().observe(getViewLifecycleOwner(), new Observer<OrderBuy>() {
      @Override
      public void onChanged(OrderBuy order) {
        orderBuy = order;
        if (orderBuy != null) {
          text_amount_all.setText(orderBuy.getTotalAmount()+"");
          totalMenuPrice.setText(orderBuy.getTotalPrice()+" ฿");
          mainResLinearShoppingฺBasket.setVisibility(View.VISIBLE);
        }
        if(orderBuy.getMenu().size() == 0){
          orderBuy.setRes_id(null);
          orderBuy.setTransportCost("0");
          mainResLinearShoppingฺBasket.setVisibility(View.GONE);
        }
      }
    });
    //set ViewPagerAndTab
    setViewPagerAndTab();
  }

  @Override
  public void onResume() {
    super.onResume();
  }

  public void initDataRestaurantFromBundle() {
    //get data from selectRestaurant with bundle key is Restaurant
    if (getArguments() != null) {
      Restaurant res = Parcels.unwrap(getArguments().getParcelable(SELECT_RESTAURANT_ARG_BUNDLE));
      if (res != null) {
        restaurant = res;
        textView_name_res.setText(restaurant.getResName());
        image_res_bar.setImageURI(Uri.parse("android.resource://"+getContext().getPackageName()+"/drawable/"+restaurant.getMenus().getImage()+"1"));
        resViewModel.setRestaurant(restaurant);
      }
    }
  }

  public void setViewPagerAndTab() {
    //set getFragmentManager() in SectionsPagerAdapter bug
    sectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
    sectionsPagerAdapter.setRestaurant(restaurant);
    sectionsPagerAdapter.setTabTitles(restaurant.getMenus().getCategory());
    viewPager.setAdapter(sectionsPagerAdapter);
    tabLayout.setupWithViewPager(viewPager);
  }


  @Override
  public void onAttach(@NonNull Context context) {
    super.onAttach(context);
    if (context instanceof TalkMainActivity) {
      talkMainActivity = (TalkMainActivity) context;
    } else {
      throw new ClassCastException(context.toString()
          + " must implement MyListFragment.OnItemSelectedListener");
    }

  }

  @Override
  public void onDetach() {
    super.onDetach();
    talkMainActivity = null;
  }

}
