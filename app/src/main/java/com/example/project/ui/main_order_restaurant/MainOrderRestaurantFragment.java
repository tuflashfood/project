package com.example.project.ui.main_order_restaurant;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Parcel;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.project.R;
import com.example.project.adapter.OrderRestaurantMainAdapter;
import com.example.project.model.json.PurchaseOrder;
import com.example.project.model.json.Restaurant;
import com.example.project.model.json.User;
import com.google.gson.Gson;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainOrderRestaurantFragment extends Fragment {

  private static final String PURCHASE_ORDER_ARG_BUNDLE = "purchaseOrderNumber";
  private static final String ARG_BUNDLE_ISNEWORDER = "isneworder";
  private static final String ARG_BUNDLE_VIEWDATA = "viewdate";
  private static final String ARG_BUNDLE_USER = "ARG_BUNDLE_USER";
  private static final String TAG = "MainOrderRestaurantFragment";
  private static final String ARG_BUNDLE_PURCHASEORDER = "ARG_BUNDLE_PURCHASEORDER";
  private static final String ARG_BUNDLE_NEWPURCHASEORDER = "ARG_BUNDLE_NEWPURCHASEORDER";
  private Boolean addAgreeOrder = true;
  private BroadcastReceiver mHandler;
  private User user;
  private MainOrderRestaurantViewModel mViewModel;
  private RecyclerView recyclerView;
  private OrderRestaurantMainAdapter orderRestaurantMainAdapter;
  private List<PurchaseOrder> purchaseOrderLists = new ArrayList<>();
  private Restaurant restaurant = new Restaurant();
  private int num = -1;
  private PurchaseOrder newPurchaseOrder = new PurchaseOrder();

  //  public static MainOrderRestaurantFragment newInstance(List<PurchaseOrder> purchaseOrderList) {
  public static MainOrderRestaurantFragment newInstance(int number, User user, PurchaseOrder purchaseOrders) {
    MainOrderRestaurantFragment fragment = new MainOrderRestaurantFragment();
    Bundle bundle = new Bundle();
    if (purchaseOrders != null)
      bundle.putParcelable(ARG_BUNDLE_NEWPURCHASEORDER, Parcels.wrap(purchaseOrders));
    bundle.putInt(PURCHASE_ORDER_ARG_BUNDLE, number);
    bundle.putParcelable(ARG_BUNDLE_USER, Parcels.wrap(user));
    fragment.setArguments(bundle);
    return fragment;
//    return new MainOrderRestaurantFragment();
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
//    setRetainInstance(true);
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.main_order_restaurant_fragment, container, false);
    recyclerView = view.findViewById(R.id.order_recycler_view);
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    recyclerView.setHasFixedSize(true);
    mViewModel = ViewModelProviders.of(requireActivity()).get(MainOrderRestaurantViewModel.class);
    initDataFromBundle();
    if (this.user != null) {
      mViewModel.getFetchDataRestaurant(user.getUser_id()).observe(getViewLifecycleOwner(), new Observer<Restaurant>() {
        @Override
        public void onChanged(Restaurant res) {
          if (res != null) {
            restaurant = res;
          }
        }
      });
    }
    Log.i(TAG, "onCreateView: " + num);
    return view;
  }

  public void initBroadcastReceiver() {
    //from foreground
    if (mHandler != null) return;
    mHandler = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
          if (extras.containsKey("dataFromForeGround")) {
            Gson gson = new Gson();
            String sPurchaseOrder = extras.getString("dataFromForeGround");
            PurchaseOrder newPurchaseOrder = gson.fromJson(sPurchaseOrder, PurchaseOrder.class);
            purchaseOrderLists.add(newPurchaseOrder);
            mViewModel.setNewOrder(purchaseOrderLists);
            initOrderRestaurantMainAdapter();
            extras.remove("dataFromForeGround");
          } else if (extras.containsKey("messageResCancel")) {
            Toast.makeText(getContext(), "ออเดอร์ถูกยกเลิกเนื่องจากไม่มีคนส่งสินค้า", Toast.LENGTH_LONG).show();
            new Handler().postDelayed(new Runnable() {
              public void run() {
                mViewModel.setSetPopBackStackDetailOrderFragement(true);
                mViewModel.setSetPopBackStackDetailOrderFragement(false);
              }
            }, 1000);
            extras.remove("messageResCancel");
          }
        }
      }
    };
    LocalBroadcastManager.getInstance(getContext()).registerReceiver(mHandler, new IntentFilter("messageFormFCM"));
  }

  @Override
  public void onResume() {
    super.onResume();
    Log.i(TAG, "onResume: " + num);
    dataPurchaseOrderFormNumber(num);
  }

  @Override
  public void onDetach() {
    super.onDetach();
    Log.i(TAG, "onDetach: ASD");
    if (num == 0)
      LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(mHandler);
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    // TODO: Use the ViewModel
//    initDataFromBundle();
//    dataPurchaseOrderFormNumber(this.num) ;
//    orderRestaurantMainAdapter.setPurchaseOrder(this.purchaseOrderLists);
  }

  private void dataPurchaseOrderFormNumber(int num) {
    if (user == null) return;
    switch (num) {
      case 0:
        initBroadcastReceiver();
        mViewModel.getNewOrder().observe(getViewLifecycleOwner(), purchaseOrders -> {
          setPurchase(purchaseOrders);
        });
        break;
      case 1:
        mViewModel.getDataOrderNotSuccess(restaurant.getResId() + "").observe(getViewLifecycleOwner(), purchaseOrderList -> {
          if (purchaseOrderList != null) {
            Collections.sort(purchaseOrderList);
            setPurchaseOrderAgreeOrSuccess(purchaseOrderList, false);
            addAgreeOrder = false;
            mViewModel.setAgreeOrder(purchaseOrderLists);
          }else {
            orderRestaurantMainAdapter = new OrderRestaurantMainAdapter();
            orderRestaurantMainAdapter.setPurchaseOrder(purchaseOrderLists);
            orderRestaurantMainAdapter.notifyDataSetChanged();
            recyclerView.setAdapter(orderRestaurantMainAdapter);
            purchaseOrderLists.clear();
          }
        });
        mViewModel.getAgreeOrder().observe(getViewLifecycleOwner(), purchaseOrders -> {
          if (addAgreeOrder) {
            setPurchase(purchaseOrders);
          }
          addAgreeOrder = true;
        });
        break;
      case 2:
        mViewModel.getDataOrderNotSuccess(restaurant.getResId() + "").observe(getViewLifecycleOwner(), new Observer<List<PurchaseOrder>>() {
          @Override
          public void onChanged(List<PurchaseOrder> purchaseOrders) {
            if (purchaseOrders != null) {
              setPurchaseOrderAgreeOrSuccess(purchaseOrders, true);
            } else {
              orderRestaurantMainAdapter = new OrderRestaurantMainAdapter();
              orderRestaurantMainAdapter.setPurchaseOrder(purchaseOrderLists);
              orderRestaurantMainAdapter.notifyDataSetChanged();
              recyclerView.setAdapter(orderRestaurantMainAdapter);
              purchaseOrderLists.clear();
            }
          }
        });
        break;
    }
  }

  public void setPurchaseOrderAgreeOrSuccess(List<PurchaseOrder> purchase, Boolean boo) {
    List<PurchaseOrder> purchaseOrders = new ArrayList<>();
    for (PurchaseOrder p : purchase) {
      if (p.getOrder().isOrder_restaurant() == boo && p.getOrder().getOrder_status().get("prepare_products").isValue())
        purchaseOrders.add(p);
    }
    this.purchaseOrderLists = purchaseOrders;
    initOrderRestaurantMainAdapter();
  }

  public void initOrderRestaurantMainAdapter() {
    Log.i(TAG, "initOrderRestaurantMainAdapter: " + num);
    orderRestaurantMainAdapter = new OrderRestaurantMainAdapter();
    if (purchaseOrderLists.size() != 0) {
      orderRestaurantMainAdapter.setOnItemClickListener(new OrderRestaurantMainAdapter.MyClickListener() {
        @Override
        public void onItemClick(View v, int position) {
          Bundle bundle = new Bundle();
          if (num == 0)
            bundle.putBoolean(ARG_BUNDLE_ISNEWORDER, true);
          else if (num == 2) {
            Log.i(TAG, "onItemClick: " + "bug is View");
            bundle.putString(ARG_BUNDLE_VIEWDATA, "viewdata");
          }
          bundle.putParcelable(ARG_BUNDLE_PURCHASEORDER, Parcels.wrap(purchaseOrderLists.get(position)));
          Navigation.findNavController(v).navigate(R.id.nav_restaurant_order_detail, bundle);
        }
      });
    }
    Log.i(TAG, "initOrderRestaurantMainAdapter: " + purchaseOrderLists);
    orderRestaurantMainAdapter.setPurchaseOrder(purchaseOrderLists);
    orderRestaurantMainAdapter.notifyDataSetChanged();
    recyclerView.setAdapter(orderRestaurantMainAdapter);
  }

  public void setPurchase(List<PurchaseOrder> purchase) {
    this.purchaseOrderLists = purchase;
    initOrderRestaurantMainAdapter();
  }

  public void initDataFromBundle() {
    Bundle extras = getArguments();
    if (extras != null) {
      if (extras.containsKey(PURCHASE_ORDER_ARG_BUNDLE)) {
        this.num = extras.getInt(PURCHASE_ORDER_ARG_BUNDLE);
      }
      if (extras.containsKey(ARG_BUNDLE_USER)) {
        this.user = Parcels.unwrap(extras.getParcelable(ARG_BUNDLE_USER));
      }
      //fix bug read all num == 0
      if (extras.containsKey(ARG_BUNDLE_NEWPURCHASEORDER) && num == 0) {
        this.newPurchaseOrder = Parcels.unwrap(extras.getParcelable(ARG_BUNDLE_NEWPURCHASEORDER));
        purchaseOrderLists.add(newPurchaseOrder);
        initOrderRestaurantMainAdapter();
        extras.remove(ARG_BUNDLE_NEWPURCHASEORDER);
        Log.i(TAG, "initDataFromBundle: " + extras.containsKey(ARG_BUNDLE_NEWPURCHASEORDER));
      }
    }
  }

}
