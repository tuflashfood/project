package com.example.project.ui.menu_retaurant;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.project.model.json.Menu;
import com.example.project.model.json.Restaurant;
import com.example.project.ui.menu_retaurant.MenuRestaurantFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SectionsPagerAdapter extends FragmentStatePagerAdapter {
  private List<String> TAB_TITLES = new ArrayList<>();
  private Restaurant restaurant ;

  public SectionsPagerAdapter(@NonNull FragmentManager fm) {
    super(fm);
  }

  public void setTabTitles(List<String> tabTitles){
    this.TAB_TITLES = tabTitles ;
  }

  public void setRestaurant(Restaurant restaurant) {
    this.restaurant = restaurant;
  }

  @Nullable
  @Override
  public CharSequence getPageTitle(int position) {
    return TAB_TITLES.get(position);
  }

  @NonNull
  @Override
  public Fragment getItem(int position) {
    HashMap<String,List<Menu>> selectMenu = restaurant.getMenus().getMenu();
    List<Menu> selectFromCategory = selectMenu.get(restaurant.getMenus().getCategory().get(position));
    return MenuRestaurantFragment.newInstance(selectFromCategory,restaurant);
  }

  @Override
  public int getCount() {
    return TAB_TITLES.size();
  }
}
