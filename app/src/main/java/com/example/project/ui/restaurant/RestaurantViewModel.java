package com.example.project.ui.restaurant;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.project.model.RestaurantRepository;
import com.example.project.model.json.Restaurant;

public class RestaurantViewModel extends ViewModel {
  // TODO: Implement the ViewModel
  private MutableLiveData<Restaurant> restaurant = new MutableLiveData<>();
  private RestaurantRepository restaurantRepository = new RestaurantRepository();

  public MutableLiveData<Restaurant> getRestaurant() {
    return restaurant;
  }

  public void setRestaurant(Restaurant restaurant) {
    this.restaurant.setValue(restaurant);
  }
}
