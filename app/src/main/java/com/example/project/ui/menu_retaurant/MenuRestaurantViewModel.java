package com.example.project.ui.menu_retaurant;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.project.model.json.Menu;

import java.util.List;

public class MenuRestaurantViewModel extends ViewModel {
  private MutableLiveData<List<Menu>> menuList = new MutableLiveData<>();

  public void setMenuList(List<Menu> menuList) {
    this.menuList.setValue(menuList);
  }

  public MutableLiveData<List<Menu>> getMenuList() {
    return menuList;
  }



}
