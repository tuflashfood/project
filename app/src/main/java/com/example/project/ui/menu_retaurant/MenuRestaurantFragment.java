package com.example.project.ui.menu_retaurant;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.project.model.json.OrderBuy;
import com.example.project.R;
import com.example.project.adapter.MenuRestaurantAdapter;
import com.example.project.model.json.Menu;
import com.example.project.model.json.Restaurant;
import com.example.project.ui.restaurant.RestaurantViewModel;
import com.example.project.ui.select_restaurant.SelectRestaurantViewModel;

import org.parceler.Parcels;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;

public class MenuRestaurantFragment extends Fragment {

  private static final String SEND_RESIDANDCOST_ARG_BUNDLE = "residandcost";
  private static final String SEND_MENU_ARG_BUNDLE = "menu";
  private static final String MENU_ARG_BUNDLE = "section_menu";
  private static final String RESTAURANT_ARG_BUNDLE = "restaurant";
  private static String TAG = "MenuRestaurantFragment";

  private OrderBuy orderBuy;
  private Restaurant restaurant;
  private List<Menu> menuList;
  private RecyclerView recyclerView;
  private MenuRestaurantAdapter menuRestaurantAdapter;
  private MenuRestaurantViewModel mViewModel;
  private RestaurantViewModel resViewModel;
  private SelectRestaurantViewModel selectResViewModel;

  public static MenuRestaurantFragment newInstance(List<Menu> menuList, Restaurant res) {
    MenuRestaurantFragment fragment = new MenuRestaurantFragment();
    Bundle bundle = new Bundle();
    bundle.putParcelable(MENU_ARG_BUNDLE, Parcels.wrap(menuList));
    bundle.putParcelable(RESTAURANT_ARG_BUNDLE, Parcels.wrap(res));
    fragment.setArguments(bundle);
    return fragment;
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    final View view = inflater.inflate(R.layout.recycler_menu_restaurant_fragment, container, false);
    recyclerView = (RecyclerView) view.findViewById(R.id.menu_recycler_view);
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    recyclerView.setHasFixedSize(true);
//    resViewModel.getRestaurant().observe();
    //user select menu success
    return view;
  }


  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
//    if (savedInstanceState.getParcelable("dataGotFromServer") != null) menuList = savedInstanceState.getParcelable("dataGotFromServer");
    mViewModel = ViewModelProviders.of(requireActivity()).get(MenuRestaurantViewModel.class);
    resViewModel = ViewModelProviders.of(requireActivity()).get(RestaurantViewModel.class);
    selectResViewModel = ViewModelProviders.of(requireActivity()).get(SelectRestaurantViewModel.class);
    //assign value restaurant
    initDataMenuFromBundle();
    setSelectRestaurant(this.menuList);

    selectResViewModel.getOrderBuy().observe(getViewLifecycleOwner(), new Observer<OrderBuy>() {
      @Override
      public void onChanged(OrderBuy order) {
        orderBuy = order;
        menuRestaurantAdapter.setOrder(orderBuy);
      }
    });

    menuRestaurantAdapter.setOnItemClickListener(new MenuRestaurantAdapter.MyClickListener() {
      @Override
      public void onItemClick(Menu menus, View v, int position) {
        //check menu is option
        if (menus.getOption() != null) {
          Bundle bundle = new Bundle();
          bundle.putParcelable(SEND_RESIDANDCOST_ARG_BUNDLE, Parcels.wrap(restaurant.getResId() + " " + restaurant.getDeliveryCost()));
          bundle.putParcelable(SEND_MENU_ARG_BUNDLE, Parcels.wrap(menus));
          Navigation.findNavController(v).navigate(R.id.nav_menu_option, bundle);
        } else {
          if (orderBuy == null) {
            orderBuy = new OrderBuy();
          }
          // TODO: fix user buy order
          boolean agreeChangeMenu = true;

          if (orderBuy.getRes_id() == null) {
            orderBuy.setRes_id(restaurant.getResId() + "");
            orderBuy.setTransportCost(restaurant.getDeliveryCost() + "");
          }

          if (Integer.parseInt(orderBuy.getRes_id()) != restaurant.getResId()) {
            alertNewRestaurant(menus);
          } else {
            orderBuy.addMenu(createMenuOption(menus), "menu");
            selectResViewModel.setOrderBuy(orderBuy);
            menuRestaurantAdapter.notifyDataSetChanged();
          }
        }
      }
    });

//    mViewModel.getMenuList().observe(getViewLifecycleOwner(), new Observer<List<Menu>>() {
//      @Override
//      public void onChanged(List<Menu> menuList) {
//        Log.i(TAG, "onChanged: --- " + menuList);
//      }
//    });
  }

  @Override
  public void onPause() {
    super.onPause();
  }

  public HashMap<String, String> createMenuOption(Menu menus) {
    HashMap<String, String> menuOption = new HashMap<>();
    menuOption.put("key", menus.getName());
    menuOption.put("menu_name", menus.getName());
    menuOption.put("menu_description", menus.getDescription());
    menuOption.put("price", menus.getPrice());
    menuOption.put("total_price", menus.getPrice());
    menuOption.put("amount", "1");
    menuOption.put("product_notes", "");
    return menuOption;
  }

  public void setSelectRestaurant(List<Menu> menuList) {
    menuRestaurantAdapter = new MenuRestaurantAdapter(menuList);
    if (orderBuy != null) {
      menuRestaurantAdapter.setOrder(orderBuy);
    }
    recyclerView.setAdapter(menuRestaurantAdapter);
  }

  public void initDataMenuFromBundle() {
    if (getArguments() != null) {
      List<Menu> menuList = Parcels.unwrap(getArguments().getParcelable(MENU_ARG_BUNDLE));
      Restaurant res = Parcels.unwrap(getArguments().getParcelable(RESTAURANT_ARG_BUNDLE));
      if (res != null) {
        restaurant = res;
      }
      if (menuList != null) {
        this.menuList = menuList;
        mViewModel.setMenuList(menuList);
      }
    }
  }

  public void alertNewRestaurant(final Menu menus) {
    final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    LinearLayout linearLayout = new LinearLayout(getContext());
    linearLayout.setBackgroundColor(getResources().getColor(R.color.color_red));
    linearLayout.setPadding(0, 10, 0, 0);
    linearLayout.setGravity(Gravity.CENTER);
    ImageView imageView = new ImageView(getContext());
    int dp25 = (int) pxFromDp(getContext(), 25);
    imageView.setLayoutParams(new LinearLayout.LayoutParams(dp25, dp25));
    imageView.setImageResource(R.drawable.signs);
    TextView title = new TextView(getContext());
    title.setTextColor(getResources().getColor(R.color.colorWhite));
    title.setText(" โปรดตรวจสอบ");
    title.setGravity(Gravity.CENTER);
    title.setPadding(30, 0, 0, 0);
    title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);
    linearLayout.setOrientation(LinearLayout.HORIZONTAL);
    linearLayout.addView(imageView);
    linearLayout.addView(title);
    builder.setCustomTitle(linearLayout);
    builder.setMessage("คุณเลือกร้านอื่น หากดำเนินการต่อรายการในตะกร้าของคุณจะถูกลบ");
    builder.setCancelable(false);
    builder.setNegativeButton("ใช่", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        orderBuy.clearOrder();
        orderBuy.setRes_id(restaurant.getResId() + "");
        orderBuy.setTransportCost(restaurant.getDeliveryCost() + "");
        orderBuy.addMenu(createMenuOption(menus), "menu");
        selectResViewModel.setOrderBuy(orderBuy);
        menuRestaurantAdapter.notifyDataSetChanged();
      }
    });
    builder.setPositiveButton("ยกเลิก", null);
    AlertDialog dialog = builder.create();
    dialog.show();
    TextView messageText = (TextView) dialog.findViewById(android.R.id.message);
    messageText.setTypeface(ResourcesCompat.getFont(getContext(), R.font.layijimahaniyom));
    messageText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
    messageText.setGravity(Gravity.CENTER);
  }

  public static float pxFromDp(final Context context, final float dp) {
    return dp * context.getResources().getDisplayMetrics().density;
  }

}
