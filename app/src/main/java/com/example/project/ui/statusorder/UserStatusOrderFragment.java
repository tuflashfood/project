package com.example.project.ui.statusorder;

import androidx.lifecycle.ViewModelProviders;

import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.project.R;
import com.example.project.model.json.OrderStatus;
import com.example.project.model.json.PurchaseOrder;
import com.example.project.model.json.Restaurant;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UserStatusOrderFragment extends Fragment {
  private static final String ARG_BUNDLE_ORDERSTATUS = "order_status";
  private static final String ARG_BUNDLE_ORDERID = "order_id";
  private static final String TAG = "UserStatusOrderFragment";
  private UserStatusOrderViewModel mViewModel;
  private HashMap<String, OrderStatus> orderStatusHashMap = new HashMap<String, OrderStatus>();
  private TextView textView_acceptOrder;
  private TextView textView_cookingOrder;
  private TextView textView_deliveryOrder;
  private TextView textView_successOrder;
  private ImageView imageView_acceptOrder;
  private ImageView imageView_cookingOrder;
  private ImageView imageView_deliveryOrder;
  private ImageView imageView_successOrder;
  private TextView textView_orderId ;
  private ImageView imageView_close_statusOrder1 ;

  @Nullable
  @Override
  public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
    return super.onCreateAnimation(transit, enter, nextAnim);
  }

  public static UserStatusOrderFragment newInstance() {
    return new UserStatusOrderFragment();
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.user_status_order_fragment, container, false);
    textView_acceptOrder = view.findViewById(R.id.textView_acceptOrder) ;
    textView_cookingOrder = view.findViewById(R.id.textView_cookingOrder) ;
    textView_deliveryOrder = view.findViewById(R.id.textView_deliveryOrder) ;
    textView_successOrder = view.findViewById(R.id.textView_successOrder) ;
    textView_orderId = view.findViewById(R.id.textView_orderId) ;
    imageView_acceptOrder = view.findViewById(R.id.imageView_acceptOrder) ;
    imageView_cookingOrder = view.findViewById(R.id.imageView_cookingOrder) ;
    imageView_deliveryOrder = view.findViewById(R.id.imageView_deliveryOrder) ;
    imageView_successOrder = view.findViewById(R.id.imageView_successOrder) ;
    imageView_close_statusOrder1 = view.findViewById(R.id.imageView_close_statusOrder1);
    imageView_close_statusOrder1.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Navigation.findNavController(getView()).popBackStack(R.id.nav_userStatusOrderFragment,true);
      }
    });
    initDataStatusOrderFromBundle();
    intiStatusOrder();
    return view;
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    mViewModel = ViewModelProviders.of(this).get(UserStatusOrderViewModel.class);
    // TODO: Use the ViewModel
  }

  public void initDataStatusOrderFromBundle() {
    //get data from selectRestaurant with bundle key is Restaurant
    if (getArguments() != null) {
      Bundle extras = getArguments();
      if (extras.containsKey(ARG_BUNDLE_ORDERSTATUS)) {
        orderStatusHashMap = Parcels.unwrap(getArguments().getParcelable(ARG_BUNDLE_ORDERSTATUS));
        for (String key : orderStatusHashMap.keySet()) {
          Log.i(TAG, "initDataStatusOrderFromBundle: " + key);
          Log.i(TAG, "initDataStatusOrderFromBundle: " + orderStatusHashMap.get(key));
        }
      }
      if(extras.containsKey(ARG_BUNDLE_ORDERID)){
        String order_id = extras.getString(ARG_BUNDLE_ORDERID) ;
        textView_orderId.setText(" ใบสั่งซื้อ #"+String.format("%05d", Integer.parseInt(order_id)));
      }
    }
  }

  public void intiStatusOrder(){
    for (String key : orderStatusHashMap.keySet()){
      if(orderStatusHashMap.get(key).isValue()){
        switch (key){
          case "order" :
            setDataStatusOrder(textView_acceptOrder,imageView_acceptOrder,orderStatusHashMap.get(key).getTime(),R.drawable.accept_order_c);
            break;
          case "prepare_products" :
            setDataStatusOrder(textView_cookingOrder,imageView_cookingOrder,orderStatusHashMap.get(key).getTime(),R.drawable.cooking_order_c);
            break;
          case "shipping" :
            setDataStatusOrder(textView_deliveryOrder,imageView_deliveryOrder,orderStatusHashMap.get(key).getTime(),R.drawable.delivery_order_c);
            break;
          case "delivered" :
            setDataStatusOrder(textView_successOrder,imageView_successOrder,orderStatusHashMap.get(key).getTime(),R.drawable.success_order_c);
            break;
        }
      }
    }
  }

  public void setDataStatusOrder(TextView textView,ImageView imageView,String time,int picture){
    textView.setText(time);
    imageView.setImageResource(picture);
  }

}
