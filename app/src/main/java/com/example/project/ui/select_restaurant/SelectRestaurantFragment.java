package com.example.project.ui.select_restaurant;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.project.R;
import com.example.project.adapter.SelectRestaurantAdapter;
import com.example.project.interfaces.TalkMainActivity;
import com.example.project.model.json.OrderBuy;
import com.example.project.model.json.Restaurant;
import com.example.project.model.json.User;
import com.google.gson.Gson;

import org.parceler.Parcels;

import java.util.List;

public class SelectRestaurantFragment extends Fragment {
  private String TAG = "Select_Restaurant";
  private static final String ARG_BUNDLE_USER = "ARG_BUNDLE_USER";
  private static final String ARG_BUNDLE = "Restaurant";
  private TalkMainActivity talkMainActivity;
  private SelectRestaurantAdapter selectRestaurantAdapter;
  private RecyclerView recyclerView;
  private List<Restaurant> restaurant;
  private OrderBuy orderBuy;
  private User user;
  private SelectRestaurantViewModel selectResViewModel;

  public static SelectRestaurantFragment newInstance() {
    return new SelectRestaurantFragment();
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable final Bundle savedInstanceState) {
    //enable toolbar and drawer layout
    initDataRestaurantFromBundle();
    talkMainActivity.setDisplayToolBar(true);
    talkMainActivity.setDisplayDrawerLayout(true);
    View view = inflater.inflate(R.layout.recycler_select_restaurant_fragment, container, false);
    setRecyclerViewSelectRestaurant(view);
    return view;
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    // TODO: Use the ViewModel

    // use view model because await data form server
    if (orderBuy == null) orderBuy = new OrderBuy();
    if (user != null)
      orderBuy.setBuy_type_user_id(user.getUser_id());
    else
      getStringFormFile();
    orderBuy.setBuy_type_user_id(user.getUser_id());
    selectResViewModel = ViewModelProviders.of(requireActivity()).get(SelectRestaurantViewModel.class);
    selectResViewModel.setOrderBuy(orderBuy);
    selectResViewModel.setUserMutableLiveData(user);
    selectResViewModel.getRestaurantLiveData().observe(getViewLifecycleOwner(), restaurants -> {
      selectRestaurantAdapter = new SelectRestaurantAdapter(restaurants);
      restaurant = restaurants;
      selectRestaurantAdapter.setOnItemClickListener((position, v) -> selectResViewModel.getIndexRestaurantLiveData(position).observe(getViewLifecycleOwner(), res -> {
        Bundle bundle = new Bundle();
        bundle.putParcelable(ARG_BUNDLE, Parcels.wrap(res));
        Navigation.findNavController(v).navigate(R.id.nav_restaurant, bundle);
      }));
      recyclerView.setAdapter(selectRestaurantAdapter);
    });
  }

  @Override
  public void onAttach(@NonNull Context context) {
    super.onAttach(context);
    if (context instanceof TalkMainActivity) {
      talkMainActivity = (TalkMainActivity) context;
    } else {
      throw new ClassCastException(context.toString()
          + " must implement MyListFragment.OnItemSelectedListener");
    }
  }

  @Override
  public void onDetach() {
    super.onDetach();
    Log.i(TAG, "onDetach: ");
    talkMainActivity = null;
  }

  @Override
  public void onPause() {
    super.onPause();
    Log.i(TAG, "onDetach: ");
  }

  public void initDataRestaurantFromBundle() {
    //get data from selectRestaurant with bundle key is Restaurant
    if (getArguments() != null) {
      User u = Parcels.unwrap(getArguments().getParcelable(ARG_BUNDLE_USER));
      if (u != null) {
        user = u;
      }
    }
  }

  public void setRecyclerViewSelectRestaurant(View view) {
    recyclerView = (RecyclerView) view.findViewById(R.id.select_recycler_view);
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    recyclerView.setHasFixedSize(true);
  }

  @Override
  public void onSaveInstanceState(@NonNull Bundle outState) {
    super.onSaveInstanceState(outState);
//    outState.putParcelable("res", (Parcelable) restaurant);
  }

  public void getStringFormFile() {
    SharedPreferences shared_pref = getContext().getSharedPreferences("MyPrefsFile", Context.MODE_PRIVATE);
    String textUser = shared_pref.getString("user", "No Pref");
    if (!textUser.equals("No Pref")) {
      Gson gson = new Gson();
      user = gson.fromJson(textUser, User.class);
    }
  }
}
