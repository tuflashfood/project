package com.example.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.project.interfaces.TalkMainActivity;
import com.example.project.model.json.OrderBuy;
import com.example.project.model.json.User;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;

import org.parceler.Parcels;

public class

MainActivity extends AppCompatActivity implements TalkMainActivity , NavigationView.OnNavigationItemSelectedListener{
  private static final String ARG_BUNDLE_USER = "ARG_BUNDLE_USER" ;
  private String TAG = "activityMain";
  private AppBarConfiguration mAppBarConfiguration;
  private Toolbar toolbar ;
  private OrderBuy orderBuy ;
  private DrawerLayout drawer ;
  private TextView nav_header_title ;
  private User user ;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    String s = FirebaseInstanceId.getInstance().getToken();
    Log.i(TAG, "onCreate: " + s);
    getExtraFromLogin();
    LinearLayout linear_logout = (LinearLayout) findViewById(R.id.linear_logout);
    linear_logout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        SharedPreferences shared_pref = getSharedPreferences("MyPrefsFile", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared_pref.edit();
        editor.remove("user");
        editor.commit();
        Intent intent = new Intent(MainActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();
      }
    });

    drawer = findViewById(R.id.drawer_layout);
    NavigationView navigationView = findViewById(R.id.nav_view);
    View hView =  navigationView.getHeaderView(0);
    nav_header_title = (TextView)hView.findViewById(R.id.nav_header_title);
    if(user != null) nav_header_title.setText("คุณ " + user.getName());
    //clear menu
    navigationView.getMenu().clear();
    navigationView.setItemIconTintList(null);
    navigationView.inflateMenu(R.menu.activity_main_drawer_user);
//     Passing each menu ID as a set of Ids because each
//     menu should be considered as top level destinations.
    //fragment show
    mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.nav_home,R.id.nav_userStatusOrderFragment)
        .setDrawerLayout(drawer)
        .build();

    NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
    Bundle bundle = new Bundle();
    bundle.putParcelable(ARG_BUNDLE_USER, Parcels.wrap(user));
    navController.setGraph(R.navigation.user_navigation,bundle);
    NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
    NavigationUI.setupWithNavController(navigationView, navController);
  }

  @Override
  protected void onResume() {
    super.onResume();
  }

  @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
    switch (menuItem.getItemId()) {
      case R.id.linear_logout :
        Log.i(TAG, "onNavigationItemSelected: " + R.id.linear_logout);
        break;
    }
    drawer.closeDrawers();
    return true;
  }

  public void getExtraFromLogin(){
    Bundle extras = getIntent().getExtras();
    if(extras != null){
      user = Parcels.unwrap(getIntent().getParcelableExtra("user"));
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override
  public boolean onSupportNavigateUp() {
    NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
    return NavigationUI.navigateUp(navController, mAppBarConfiguration)
        || super.onSupportNavigateUp();
  }


  @Override
  public void setDisplayToolBar(Boolean bool) {
    int display = bool ? View.VISIBLE:View.GONE;
    toolbar.setVisibility(display);
  }

  public void setDisplayDrawerLayout(Boolean bool){
    int display = bool ? DrawerLayout.LOCK_MODE_UNLOCKED:DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
    drawer.setDrawerLockMode(display);
  }

}
